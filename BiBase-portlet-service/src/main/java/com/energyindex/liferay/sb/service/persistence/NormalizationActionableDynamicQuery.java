package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.model.Normalization;
import com.energyindex.liferay.sb.service.NormalizationLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author InfinityFrame : Jason Gonin
 * @generated
 */
public abstract class NormalizationActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public NormalizationActionableDynamicQuery() throws SystemException {
        setBaseLocalService(NormalizationLocalServiceUtil.getService());
        setClass(Normalization.class);

        setClassLoader(com.energyindex.liferay.sb.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("normId");
    }
}
