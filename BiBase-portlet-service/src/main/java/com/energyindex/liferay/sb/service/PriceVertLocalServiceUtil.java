package com.energyindex.liferay.sb.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for PriceVert. This utility wraps
 * {@link com.energyindex.liferay.sb.service.impl.PriceVertLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVertLocalService
 * @see com.energyindex.liferay.sb.service.base.PriceVertLocalServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.impl.PriceVertLocalServiceImpl
 * @generated
 */
public class PriceVertLocalServiceUtil {
    private static PriceVertLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.energyindex.liferay.sb.service.impl.PriceVertLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the price vert to the database. Also notifies the appropriate model listeners.
    *
    * @param priceVert the price vert
    * @return the price vert that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert addPriceVert(
        com.energyindex.liferay.sb.model.PriceVert priceVert)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addPriceVert(priceVert);
    }

    /**
    * Creates a new price vert with the primary key. Does not add the price vert to the database.
    *
    * @param price_id the primary key for the new price vert
    * @return the new price vert
    */
    public static com.energyindex.liferay.sb.model.PriceVert createPriceVert(
        long price_id) {
        return getService().createPriceVert(price_id);
    }

    /**
    * Deletes the price vert with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param price_id the primary key of the price vert
    * @return the price vert that was removed
    * @throws PortalException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert deletePriceVert(
        long price_id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePriceVert(price_id);
    }

    /**
    * Deletes the price vert from the database. Also notifies the appropriate model listeners.
    *
    * @param priceVert the price vert
    * @return the price vert that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert deletePriceVert(
        com.energyindex.liferay.sb.model.PriceVert priceVert)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePriceVert(priceVert);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static com.energyindex.liferay.sb.model.PriceVert fetchPriceVert(
        long price_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchPriceVert(price_id);
    }

    /**
    * Returns the price vert with the primary key.
    *
    * @param price_id the primary key of the price vert
    * @return the price vert
    * @throws PortalException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert getPriceVert(
        long price_id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceVert(price_id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the price verts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @return the range of price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> getPriceVerts(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceVerts(start, end);
    }

    /**
    * Returns the number of price verts.
    *
    * @return the number of price verts
    * @throws SystemException if a system exception occurred
    */
    public static int getPriceVertsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceVertsCount();
    }

    /**
    * Updates the price vert in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param priceVert the price vert
    * @return the price vert that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert updatePriceVert(
        com.energyindex.liferay.sb.model.PriceVert priceVert)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePriceVert(priceVert);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.energyindex.liferay.sb.model.PriceVert addPriceVert(
        long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addPriceVert(userId, groupId, price_close, port_id,
            product_id, month, day, year, portId, serviceContext);
    }

    public static void addPriceVertResources(
        com.energyindex.liferay.sb.model.PriceVert price,
        java.lang.String[] groupPermissions, java.lang.String[] guestPermissions)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService()
            .addPriceVertResources(price, groupPermissions, guestPermissions);
    }

    public static void addPriceVertResources(
        com.energyindex.liferay.sb.model.PriceVert price,
        boolean addGroupPermissions, boolean addGuestPermissions)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService()
            .addPriceVertResources(price, addGroupPermissions,
            addGuestPermissions);
    }

    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> getPriceVertsByPortId(
        int portId) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceVertsByPortId(portId);
    }

    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> getPriceVertsByPortId(
        int portId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceVertsByPortId(portId, start, end);
    }

    public static int getPriceVertsCountByPortId(int port_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceVertsCountByPortId(port_id);
    }

    public static com.energyindex.liferay.sb.model.PriceVert updatePriceVert(
        int priceId, long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updatePriceVert(priceId, userId, groupId, price_close,
            port_id, product_id, month, day, year, portId, serviceContext);
    }

    public static void clearService() {
        _service = null;
    }

    public static PriceVertLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    PriceVertLocalService.class.getName());

            if (invokableLocalService instanceof PriceVertLocalService) {
                _service = (PriceVertLocalService) invokableLocalService;
            } else {
                _service = new PriceVertLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(PriceVertLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(PriceVertLocalService service) {
    }
}
