package com.energyindex.liferay.sb.service.messaging;

import com.energyindex.liferay.sb.service.ClpSerializer;
import com.energyindex.liferay.sb.service.InternalindexesLocalServiceUtil;
import com.energyindex.liferay.sb.service.InternalindexesServiceUtil;
import com.energyindex.liferay.sb.service.NormalizationLocalServiceUtil;
import com.energyindex.liferay.sb.service.NormalizationServiceUtil;
import com.energyindex.liferay.sb.service.PriceHorizLocalServiceUtil;
import com.energyindex.liferay.sb.service.PriceHorizServiceUtil;
import com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil;
import com.energyindex.liferay.sb.service.PriceVertServiceUtil;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            InternalindexesLocalServiceUtil.clearService();

            InternalindexesServiceUtil.clearService();
            NormalizationLocalServiceUtil.clearService();

            NormalizationServiceUtil.clearService();
            PriceHorizLocalServiceUtil.clearService();

            PriceHorizServiceUtil.clearService();
            PriceVertLocalServiceUtil.clearService();

            PriceVertServiceUtil.clearService();
        }
    }
}
