package com.energyindex.liferay.sb.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Internalindexes}.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see Internalindexes
 * @generated
 */
public class InternalindexesWrapper implements Internalindexes,
    ModelWrapper<Internalindexes> {
    private Internalindexes _internalindexes;

    public InternalindexesWrapper(Internalindexes internalindexes) {
        _internalindexes = internalindexes;
    }

    @Override
    public Class<?> getModelClass() {
        return Internalindexes.class;
    }

    @Override
    public String getModelClassName() {
        return Internalindexes.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("price_id", getPrice_id());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("userName", getUserName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("product_id", getProduct_id());
        attributes.put("price_date", getPrice_date());
        attributes.put("month_id", getMonth_id());
        attributes.put("price_contractyear", getPrice_contractyear());
        attributes.put("price_low", getPrice_low());
        attributes.put("price_current", getPrice_current());
        attributes.put("price_high", getPrice_high());
        attributes.put("price_close", getPrice_close());
        attributes.put("price_unitquote", getPrice_unitquote());
        attributes.put("currency_id", getCurrency_id());
        attributes.put("unit_id", getUnit_id());
        attributes.put("price_quantity", getPrice_quantity());
        attributes.put("delivery_id", getDelivery_id());
        attributes.put("priceindex_id", getPriceindex_id());
        attributes.put("company_id", getCompany_id());
        attributes.put("contracttype_id", getContracttype_id());
        attributes.put("city_id", getCity_id());
        attributes.put("port_id", getPort_id());
        attributes.put("airport_id", getAirport_id());
        attributes.put("state_id", getState_id());
        attributes.put("subregion_id", getSubregion_id());
        attributes.put("country_id", getCountry_id());
        attributes.put("publish_id", getPublish_id());
        attributes.put("current_id", getCurrent_id());
        attributes.put("price_highlight", getPrice_highlight());
        attributes.put("price_mainhighlight", getPrice_mainhighlight());
        attributes.put("price_unqouted", getPrice_unqouted());
        attributes.put("price_lastedit", getPrice_lastedit());
        attributes.put("price_dateadded", getPrice_dateadded());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer price_id = (Integer) attributes.get("price_id");

        if (price_id != null) {
            setPrice_id(price_id);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String userName = (String) attributes.get("userName");

        if (userName != null) {
            setUserName(userName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Integer product_id = (Integer) attributes.get("product_id");

        if (product_id != null) {
            setProduct_id(product_id);
        }

        Date price_date = (Date) attributes.get("price_date");

        if (price_date != null) {
            setPrice_date(price_date);
        }

        Integer month_id = (Integer) attributes.get("month_id");

        if (month_id != null) {
            setMonth_id(month_id);
        }

        Integer price_contractyear = (Integer) attributes.get(
                "price_contractyear");

        if (price_contractyear != null) {
            setPrice_contractyear(price_contractyear);
        }

        Double price_low = (Double) attributes.get("price_low");

        if (price_low != null) {
            setPrice_low(price_low);
        }

        Double price_current = (Double) attributes.get("price_current");

        if (price_current != null) {
            setPrice_current(price_current);
        }

        Double price_high = (Double) attributes.get("price_high");

        if (price_high != null) {
            setPrice_high(price_high);
        }

        Double price_close = (Double) attributes.get("price_close");

        if (price_close != null) {
            setPrice_close(price_close);
        }

        Integer price_unitquote = (Integer) attributes.get("price_unitquote");

        if (price_unitquote != null) {
            setPrice_unitquote(price_unitquote);
        }

        Integer currency_id = (Integer) attributes.get("currency_id");

        if (currency_id != null) {
            setCurrency_id(currency_id);
        }

        Integer unit_id = (Integer) attributes.get("unit_id");

        if (unit_id != null) {
            setUnit_id(unit_id);
        }

        Integer price_quantity = (Integer) attributes.get("price_quantity");

        if (price_quantity != null) {
            setPrice_quantity(price_quantity);
        }

        Integer delivery_id = (Integer) attributes.get("delivery_id");

        if (delivery_id != null) {
            setDelivery_id(delivery_id);
        }

        Integer priceindex_id = (Integer) attributes.get("priceindex_id");

        if (priceindex_id != null) {
            setPriceindex_id(priceindex_id);
        }

        Integer company_id = (Integer) attributes.get("company_id");

        if (company_id != null) {
            setCompany_id(company_id);
        }

        Integer contracttype_id = (Integer) attributes.get("contracttype_id");

        if (contracttype_id != null) {
            setContracttype_id(contracttype_id);
        }

        Integer city_id = (Integer) attributes.get("city_id");

        if (city_id != null) {
            setCity_id(city_id);
        }

        Integer port_id = (Integer) attributes.get("port_id");

        if (port_id != null) {
            setPort_id(port_id);
        }

        Integer airport_id = (Integer) attributes.get("airport_id");

        if (airport_id != null) {
            setAirport_id(airport_id);
        }

        Integer state_id = (Integer) attributes.get("state_id");

        if (state_id != null) {
            setState_id(state_id);
        }

        Integer subregion_id = (Integer) attributes.get("subregion_id");

        if (subregion_id != null) {
            setSubregion_id(subregion_id);
        }

        Integer country_id = (Integer) attributes.get("country_id");

        if (country_id != null) {
            setCountry_id(country_id);
        }

        Integer publish_id = (Integer) attributes.get("publish_id");

        if (publish_id != null) {
            setPublish_id(publish_id);
        }

        Integer current_id = (Integer) attributes.get("current_id");

        if (current_id != null) {
            setCurrent_id(current_id);
        }

        Integer price_highlight = (Integer) attributes.get("price_highlight");

        if (price_highlight != null) {
            setPrice_highlight(price_highlight);
        }

        Integer price_mainhighlight = (Integer) attributes.get(
                "price_mainhighlight");

        if (price_mainhighlight != null) {
            setPrice_mainhighlight(price_mainhighlight);
        }

        Integer price_unqouted = (Integer) attributes.get("price_unqouted");

        if (price_unqouted != null) {
            setPrice_unqouted(price_unqouted);
        }

        Date price_lastedit = (Date) attributes.get("price_lastedit");

        if (price_lastedit != null) {
            setPrice_lastedit(price_lastedit);
        }

        Date price_dateadded = (Date) attributes.get("price_dateadded");

        if (price_dateadded != null) {
            setPrice_dateadded(price_dateadded);
        }
    }

    /**
    * Returns the primary key of this internalindexes.
    *
    * @return the primary key of this internalindexes
    */
    @Override
    public int getPrimaryKey() {
        return _internalindexes.getPrimaryKey();
    }

    /**
    * Sets the primary key of this internalindexes.
    *
    * @param primaryKey the primary key of this internalindexes
    */
    @Override
    public void setPrimaryKey(int primaryKey) {
        _internalindexes.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the price_id of this internalindexes.
    *
    * @return the price_id of this internalindexes
    */
    @Override
    public int getPrice_id() {
        return _internalindexes.getPrice_id();
    }

    /**
    * Sets the price_id of this internalindexes.
    *
    * @param price_id the price_id of this internalindexes
    */
    @Override
    public void setPrice_id(int price_id) {
        _internalindexes.setPrice_id(price_id);
    }

    /**
    * Returns the group ID of this internalindexes.
    *
    * @return the group ID of this internalindexes
    */
    @Override
    public long getGroupId() {
        return _internalindexes.getGroupId();
    }

    /**
    * Sets the group ID of this internalindexes.
    *
    * @param groupId the group ID of this internalindexes
    */
    @Override
    public void setGroupId(long groupId) {
        _internalindexes.setGroupId(groupId);
    }

    /**
    * Returns the company ID of this internalindexes.
    *
    * @return the company ID of this internalindexes
    */
    @Override
    public long getCompanyId() {
        return _internalindexes.getCompanyId();
    }

    /**
    * Sets the company ID of this internalindexes.
    *
    * @param companyId the company ID of this internalindexes
    */
    @Override
    public void setCompanyId(long companyId) {
        _internalindexes.setCompanyId(companyId);
    }

    /**
    * Returns the user ID of this internalindexes.
    *
    * @return the user ID of this internalindexes
    */
    @Override
    public long getUserId() {
        return _internalindexes.getUserId();
    }

    /**
    * Sets the user ID of this internalindexes.
    *
    * @param userId the user ID of this internalindexes
    */
    @Override
    public void setUserId(long userId) {
        _internalindexes.setUserId(userId);
    }

    /**
    * Returns the user uuid of this internalindexes.
    *
    * @return the user uuid of this internalindexes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexes.getUserUuid();
    }

    /**
    * Sets the user uuid of this internalindexes.
    *
    * @param userUuid the user uuid of this internalindexes
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _internalindexes.setUserUuid(userUuid);
    }

    /**
    * Returns the user name of this internalindexes.
    *
    * @return the user name of this internalindexes
    */
    @Override
    public java.lang.String getUserName() {
        return _internalindexes.getUserName();
    }

    /**
    * Sets the user name of this internalindexes.
    *
    * @param userName the user name of this internalindexes
    */
    @Override
    public void setUserName(java.lang.String userName) {
        _internalindexes.setUserName(userName);
    }

    /**
    * Returns the create date of this internalindexes.
    *
    * @return the create date of this internalindexes
    */
    @Override
    public java.util.Date getCreateDate() {
        return _internalindexes.getCreateDate();
    }

    /**
    * Sets the create date of this internalindexes.
    *
    * @param createDate the create date of this internalindexes
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _internalindexes.setCreateDate(createDate);
    }

    /**
    * Returns the modified date of this internalindexes.
    *
    * @return the modified date of this internalindexes
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _internalindexes.getModifiedDate();
    }

    /**
    * Sets the modified date of this internalindexes.
    *
    * @param modifiedDate the modified date of this internalindexes
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _internalindexes.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the product_id of this internalindexes.
    *
    * @return the product_id of this internalindexes
    */
    @Override
    public int getProduct_id() {
        return _internalindexes.getProduct_id();
    }

    /**
    * Sets the product_id of this internalindexes.
    *
    * @param product_id the product_id of this internalindexes
    */
    @Override
    public void setProduct_id(int product_id) {
        _internalindexes.setProduct_id(product_id);
    }

    /**
    * Returns the price_date of this internalindexes.
    *
    * @return the price_date of this internalindexes
    */
    @Override
    public java.util.Date getPrice_date() {
        return _internalindexes.getPrice_date();
    }

    /**
    * Sets the price_date of this internalindexes.
    *
    * @param price_date the price_date of this internalindexes
    */
    @Override
    public void setPrice_date(java.util.Date price_date) {
        _internalindexes.setPrice_date(price_date);
    }

    /**
    * Returns the month_id of this internalindexes.
    *
    * @return the month_id of this internalindexes
    */
    @Override
    public int getMonth_id() {
        return _internalindexes.getMonth_id();
    }

    /**
    * Sets the month_id of this internalindexes.
    *
    * @param month_id the month_id of this internalindexes
    */
    @Override
    public void setMonth_id(int month_id) {
        _internalindexes.setMonth_id(month_id);
    }

    /**
    * Returns the price_contractyear of this internalindexes.
    *
    * @return the price_contractyear of this internalindexes
    */
    @Override
    public int getPrice_contractyear() {
        return _internalindexes.getPrice_contractyear();
    }

    /**
    * Sets the price_contractyear of this internalindexes.
    *
    * @param price_contractyear the price_contractyear of this internalindexes
    */
    @Override
    public void setPrice_contractyear(int price_contractyear) {
        _internalindexes.setPrice_contractyear(price_contractyear);
    }

    /**
    * Returns the price_low of this internalindexes.
    *
    * @return the price_low of this internalindexes
    */
    @Override
    public double getPrice_low() {
        return _internalindexes.getPrice_low();
    }

    /**
    * Sets the price_low of this internalindexes.
    *
    * @param price_low the price_low of this internalindexes
    */
    @Override
    public void setPrice_low(double price_low) {
        _internalindexes.setPrice_low(price_low);
    }

    /**
    * Returns the price_current of this internalindexes.
    *
    * @return the price_current of this internalindexes
    */
    @Override
    public double getPrice_current() {
        return _internalindexes.getPrice_current();
    }

    /**
    * Sets the price_current of this internalindexes.
    *
    * @param price_current the price_current of this internalindexes
    */
    @Override
    public void setPrice_current(double price_current) {
        _internalindexes.setPrice_current(price_current);
    }

    /**
    * Returns the price_high of this internalindexes.
    *
    * @return the price_high of this internalindexes
    */
    @Override
    public double getPrice_high() {
        return _internalindexes.getPrice_high();
    }

    /**
    * Sets the price_high of this internalindexes.
    *
    * @param price_high the price_high of this internalindexes
    */
    @Override
    public void setPrice_high(double price_high) {
        _internalindexes.setPrice_high(price_high);
    }

    /**
    * Returns the price_close of this internalindexes.
    *
    * @return the price_close of this internalindexes
    */
    @Override
    public double getPrice_close() {
        return _internalindexes.getPrice_close();
    }

    /**
    * Sets the price_close of this internalindexes.
    *
    * @param price_close the price_close of this internalindexes
    */
    @Override
    public void setPrice_close(double price_close) {
        _internalindexes.setPrice_close(price_close);
    }

    /**
    * Returns the price_unitquote of this internalindexes.
    *
    * @return the price_unitquote of this internalindexes
    */
    @Override
    public int getPrice_unitquote() {
        return _internalindexes.getPrice_unitquote();
    }

    /**
    * Sets the price_unitquote of this internalindexes.
    *
    * @param price_unitquote the price_unitquote of this internalindexes
    */
    @Override
    public void setPrice_unitquote(int price_unitquote) {
        _internalindexes.setPrice_unitquote(price_unitquote);
    }

    /**
    * Returns the currency_id of this internalindexes.
    *
    * @return the currency_id of this internalindexes
    */
    @Override
    public int getCurrency_id() {
        return _internalindexes.getCurrency_id();
    }

    /**
    * Sets the currency_id of this internalindexes.
    *
    * @param currency_id the currency_id of this internalindexes
    */
    @Override
    public void setCurrency_id(int currency_id) {
        _internalindexes.setCurrency_id(currency_id);
    }

    /**
    * Returns the unit_id of this internalindexes.
    *
    * @return the unit_id of this internalindexes
    */
    @Override
    public int getUnit_id() {
        return _internalindexes.getUnit_id();
    }

    /**
    * Sets the unit_id of this internalindexes.
    *
    * @param unit_id the unit_id of this internalindexes
    */
    @Override
    public void setUnit_id(int unit_id) {
        _internalindexes.setUnit_id(unit_id);
    }

    /**
    * Returns the price_quantity of this internalindexes.
    *
    * @return the price_quantity of this internalindexes
    */
    @Override
    public int getPrice_quantity() {
        return _internalindexes.getPrice_quantity();
    }

    /**
    * Sets the price_quantity of this internalindexes.
    *
    * @param price_quantity the price_quantity of this internalindexes
    */
    @Override
    public void setPrice_quantity(int price_quantity) {
        _internalindexes.setPrice_quantity(price_quantity);
    }

    /**
    * Returns the delivery_id of this internalindexes.
    *
    * @return the delivery_id of this internalindexes
    */
    @Override
    public int getDelivery_id() {
        return _internalindexes.getDelivery_id();
    }

    /**
    * Sets the delivery_id of this internalindexes.
    *
    * @param delivery_id the delivery_id of this internalindexes
    */
    @Override
    public void setDelivery_id(int delivery_id) {
        _internalindexes.setDelivery_id(delivery_id);
    }

    /**
    * Returns the priceindex_id of this internalindexes.
    *
    * @return the priceindex_id of this internalindexes
    */
    @Override
    public int getPriceindex_id() {
        return _internalindexes.getPriceindex_id();
    }

    /**
    * Sets the priceindex_id of this internalindexes.
    *
    * @param priceindex_id the priceindex_id of this internalindexes
    */
    @Override
    public void setPriceindex_id(int priceindex_id) {
        _internalindexes.setPriceindex_id(priceindex_id);
    }

    /**
    * Returns the company_id of this internalindexes.
    *
    * @return the company_id of this internalindexes
    */
    @Override
    public int getCompany_id() {
        return _internalindexes.getCompany_id();
    }

    /**
    * Sets the company_id of this internalindexes.
    *
    * @param company_id the company_id of this internalindexes
    */
    @Override
    public void setCompany_id(int company_id) {
        _internalindexes.setCompany_id(company_id);
    }

    /**
    * Returns the contracttype_id of this internalindexes.
    *
    * @return the contracttype_id of this internalindexes
    */
    @Override
    public int getContracttype_id() {
        return _internalindexes.getContracttype_id();
    }

    /**
    * Sets the contracttype_id of this internalindexes.
    *
    * @param contracttype_id the contracttype_id of this internalindexes
    */
    @Override
    public void setContracttype_id(int contracttype_id) {
        _internalindexes.setContracttype_id(contracttype_id);
    }

    /**
    * Returns the city_id of this internalindexes.
    *
    * @return the city_id of this internalindexes
    */
    @Override
    public int getCity_id() {
        return _internalindexes.getCity_id();
    }

    /**
    * Sets the city_id of this internalindexes.
    *
    * @param city_id the city_id of this internalindexes
    */
    @Override
    public void setCity_id(int city_id) {
        _internalindexes.setCity_id(city_id);
    }

    /**
    * Returns the port_id of this internalindexes.
    *
    * @return the port_id of this internalindexes
    */
    @Override
    public int getPort_id() {
        return _internalindexes.getPort_id();
    }

    /**
    * Sets the port_id of this internalindexes.
    *
    * @param port_id the port_id of this internalindexes
    */
    @Override
    public void setPort_id(int port_id) {
        _internalindexes.setPort_id(port_id);
    }

    /**
    * Returns the airport_id of this internalindexes.
    *
    * @return the airport_id of this internalindexes
    */
    @Override
    public int getAirport_id() {
        return _internalindexes.getAirport_id();
    }

    /**
    * Sets the airport_id of this internalindexes.
    *
    * @param airport_id the airport_id of this internalindexes
    */
    @Override
    public void setAirport_id(int airport_id) {
        _internalindexes.setAirport_id(airport_id);
    }

    /**
    * Returns the state_id of this internalindexes.
    *
    * @return the state_id of this internalindexes
    */
    @Override
    public int getState_id() {
        return _internalindexes.getState_id();
    }

    /**
    * Sets the state_id of this internalindexes.
    *
    * @param state_id the state_id of this internalindexes
    */
    @Override
    public void setState_id(int state_id) {
        _internalindexes.setState_id(state_id);
    }

    /**
    * Returns the subregion_id of this internalindexes.
    *
    * @return the subregion_id of this internalindexes
    */
    @Override
    public int getSubregion_id() {
        return _internalindexes.getSubregion_id();
    }

    /**
    * Sets the subregion_id of this internalindexes.
    *
    * @param subregion_id the subregion_id of this internalindexes
    */
    @Override
    public void setSubregion_id(int subregion_id) {
        _internalindexes.setSubregion_id(subregion_id);
    }

    /**
    * Returns the country_id of this internalindexes.
    *
    * @return the country_id of this internalindexes
    */
    @Override
    public int getCountry_id() {
        return _internalindexes.getCountry_id();
    }

    /**
    * Sets the country_id of this internalindexes.
    *
    * @param country_id the country_id of this internalindexes
    */
    @Override
    public void setCountry_id(int country_id) {
        _internalindexes.setCountry_id(country_id);
    }

    /**
    * Returns the publish_id of this internalindexes.
    *
    * @return the publish_id of this internalindexes
    */
    @Override
    public int getPublish_id() {
        return _internalindexes.getPublish_id();
    }

    /**
    * Sets the publish_id of this internalindexes.
    *
    * @param publish_id the publish_id of this internalindexes
    */
    @Override
    public void setPublish_id(int publish_id) {
        _internalindexes.setPublish_id(publish_id);
    }

    /**
    * Returns the current_id of this internalindexes.
    *
    * @return the current_id of this internalindexes
    */
    @Override
    public int getCurrent_id() {
        return _internalindexes.getCurrent_id();
    }

    /**
    * Sets the current_id of this internalindexes.
    *
    * @param current_id the current_id of this internalindexes
    */
    @Override
    public void setCurrent_id(int current_id) {
        _internalindexes.setCurrent_id(current_id);
    }

    /**
    * Returns the price_highlight of this internalindexes.
    *
    * @return the price_highlight of this internalindexes
    */
    @Override
    public int getPrice_highlight() {
        return _internalindexes.getPrice_highlight();
    }

    /**
    * Sets the price_highlight of this internalindexes.
    *
    * @param price_highlight the price_highlight of this internalindexes
    */
    @Override
    public void setPrice_highlight(int price_highlight) {
        _internalindexes.setPrice_highlight(price_highlight);
    }

    /**
    * Returns the price_mainhighlight of this internalindexes.
    *
    * @return the price_mainhighlight of this internalindexes
    */
    @Override
    public int getPrice_mainhighlight() {
        return _internalindexes.getPrice_mainhighlight();
    }

    /**
    * Sets the price_mainhighlight of this internalindexes.
    *
    * @param price_mainhighlight the price_mainhighlight of this internalindexes
    */
    @Override
    public void setPrice_mainhighlight(int price_mainhighlight) {
        _internalindexes.setPrice_mainhighlight(price_mainhighlight);
    }

    /**
    * Returns the price_unqouted of this internalindexes.
    *
    * @return the price_unqouted of this internalindexes
    */
    @Override
    public int getPrice_unqouted() {
        return _internalindexes.getPrice_unqouted();
    }

    /**
    * Sets the price_unqouted of this internalindexes.
    *
    * @param price_unqouted the price_unqouted of this internalindexes
    */
    @Override
    public void setPrice_unqouted(int price_unqouted) {
        _internalindexes.setPrice_unqouted(price_unqouted);
    }

    /**
    * Returns the price_lastedit of this internalindexes.
    *
    * @return the price_lastedit of this internalindexes
    */
    @Override
    public java.util.Date getPrice_lastedit() {
        return _internalindexes.getPrice_lastedit();
    }

    /**
    * Sets the price_lastedit of this internalindexes.
    *
    * @param price_lastedit the price_lastedit of this internalindexes
    */
    @Override
    public void setPrice_lastedit(java.util.Date price_lastedit) {
        _internalindexes.setPrice_lastedit(price_lastedit);
    }

    /**
    * Returns the price_dateadded of this internalindexes.
    *
    * @return the price_dateadded of this internalindexes
    */
    @Override
    public java.util.Date getPrice_dateadded() {
        return _internalindexes.getPrice_dateadded();
    }

    /**
    * Sets the price_dateadded of this internalindexes.
    *
    * @param price_dateadded the price_dateadded of this internalindexes
    */
    @Override
    public void setPrice_dateadded(java.util.Date price_dateadded) {
        _internalindexes.setPrice_dateadded(price_dateadded);
    }

    @Override
    public boolean isNew() {
        return _internalindexes.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _internalindexes.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _internalindexes.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _internalindexes.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _internalindexes.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _internalindexes.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _internalindexes.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _internalindexes.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _internalindexes.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _internalindexes.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _internalindexes.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new InternalindexesWrapper((Internalindexes) _internalindexes.clone());
    }

    @Override
    public int compareTo(
        com.energyindex.liferay.sb.model.Internalindexes internalindexes) {
        return _internalindexes.compareTo(internalindexes);
    }

    @Override
    public int hashCode() {
        return _internalindexes.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<com.energyindex.liferay.sb.model.Internalindexes> toCacheModel() {
        return _internalindexes.toCacheModel();
    }

    @Override
    public com.energyindex.liferay.sb.model.Internalindexes toEscapedModel() {
        return new InternalindexesWrapper(_internalindexes.toEscapedModel());
    }

    @Override
    public com.energyindex.liferay.sb.model.Internalindexes toUnescapedModel() {
        return new InternalindexesWrapper(_internalindexes.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _internalindexes.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _internalindexes.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _internalindexes.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof InternalindexesWrapper)) {
            return false;
        }

        InternalindexesWrapper internalindexesWrapper = (InternalindexesWrapper) obj;

        if (Validator.equals(_internalindexes,
                    internalindexesWrapper._internalindexes)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Internalindexes getWrappedInternalindexes() {
        return _internalindexes;
    }

    @Override
    public Internalindexes getWrappedModel() {
        return _internalindexes;
    }

    @Override
    public void resetOriginalValues() {
        _internalindexes.resetOriginalValues();
    }
}
