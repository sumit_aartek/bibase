package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.model.PriceHoriz;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the price horiz service. This utility wraps {@link PriceHorizPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceHorizPersistence
 * @see PriceHorizPersistenceImpl
 * @generated
 */
public class PriceHorizUtil {
    private static PriceHorizPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(PriceHoriz priceHoriz) {
        getPersistence().clearCache(priceHoriz);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<PriceHoriz> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<PriceHoriz> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<PriceHoriz> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static PriceHoriz update(PriceHoriz priceHoriz)
        throws SystemException {
        return getPersistence().update(priceHoriz);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static PriceHoriz update(PriceHoriz priceHoriz,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(priceHoriz, serviceContext);
    }

    /**
    * Returns all the price horizs where product_id = &#63;.
    *
    * @param product_id the product_id
    * @return the matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehProduct_id(
        int product_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehProduct_id(product_id);
    }

    /**
    * Returns a range of all the price horizs where product_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param product_id the product_id
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @return the range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehProduct_id(
        int product_id, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehProduct_id(product_id, start, end);
    }

    /**
    * Returns an ordered range of all the price horizs where product_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param product_id the product_id
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehProduct_id(
        int product_id, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehProduct_id(product_id, start, end,
            orderByComparator);
    }

    /**
    * Returns the first price horiz in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz findBypricehProduct_id_First(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehProduct_id_First(product_id, orderByComparator);
    }

    /**
    * Returns the first price horiz in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price horiz, or <code>null</code> if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz fetchBypricehProduct_id_First(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBypricehProduct_id_First(product_id, orderByComparator);
    }

    /**
    * Returns the last price horiz in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz findBypricehProduct_id_Last(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehProduct_id_Last(product_id, orderByComparator);
    }

    /**
    * Returns the last price horiz in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price horiz, or <code>null</code> if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz fetchBypricehProduct_id_Last(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBypricehProduct_id_Last(product_id, orderByComparator);
    }

    /**
    * Returns the price horizs before and after the current price horiz in the ordered set where product_id = &#63;.
    *
    * @param priceHorizPK the primary key of the current price horiz
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz[] findBypricehProduct_id_PrevAndNext(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK,
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehProduct_id_PrevAndNext(priceHorizPK,
            product_id, orderByComparator);
    }

    /**
    * Removes all the price horizs where product_id = &#63; from the database.
    *
    * @param product_id the product_id
    * @throws SystemException if a system exception occurred
    */
    public static void removeBypricehProduct_id(int product_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeBypricehProduct_id(product_id);
    }

    /**
    * Returns the number of price horizs where product_id = &#63;.
    *
    * @param product_id the product_id
    * @return the number of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static int countBypricehProduct_id(int product_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBypricehProduct_id(product_id);
    }

    /**
    * Returns all the price horizs where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @return the matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_date(
        java.util.Date price_date)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_date(price_date);
    }

    /**
    * Returns a range of all the price horizs where price_date &ge; &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_date the price_date
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @return the range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_date(
        java.util.Date price_date, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_date(price_date, start, end);
    }

    /**
    * Returns an ordered range of all the price horizs where price_date &ge; &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_date the price_date
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_date(
        java.util.Date price_date, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_date(price_date, start, end, orderByComparator);
    }

    /**
    * Returns the first price horiz in the ordered set where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz findBypricehP_date_First(
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_date_First(price_date, orderByComparator);
    }

    /**
    * Returns the first price horiz in the ordered set where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price horiz, or <code>null</code> if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz fetchBypricehP_date_First(
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBypricehP_date_First(price_date, orderByComparator);
    }

    /**
    * Returns the last price horiz in the ordered set where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz findBypricehP_date_Last(
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_date_Last(price_date, orderByComparator);
    }

    /**
    * Returns the last price horiz in the ordered set where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price horiz, or <code>null</code> if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz fetchBypricehP_date_Last(
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBypricehP_date_Last(price_date, orderByComparator);
    }

    /**
    * Returns the price horizs before and after the current price horiz in the ordered set where price_date &ge; &#63;.
    *
    * @param priceHorizPK the primary key of the current price horiz
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz[] findBypricehP_date_PrevAndNext(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK,
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_date_PrevAndNext(priceHorizPK, price_date,
            orderByComparator);
    }

    /**
    * Returns all the price horizs where price_date &ge; all &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_dates the price_dates
    * @return the matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_date(
        java.util.Date[] price_dates)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_date(price_dates);
    }

    /**
    * Returns a range of all the price horizs where price_date &ge; all &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_dates the price_dates
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @return the range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_date(
        java.util.Date[] price_dates, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_date(price_dates, start, end);
    }

    /**
    * Returns an ordered range of all the price horizs where price_date &ge; all &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_dates the price_dates
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_date(
        java.util.Date[] price_dates, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_date(price_dates, start, end,
            orderByComparator);
    }

    /**
    * Removes all the price horizs where price_date &ge; &#63; from the database.
    *
    * @param price_date the price_date
    * @throws SystemException if a system exception occurred
    */
    public static void removeBypricehP_date(java.util.Date price_date)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeBypricehP_date(price_date);
    }

    /**
    * Returns the number of price horizs where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @return the number of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static int countBypricehP_date(java.util.Date price_date)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBypricehP_date(price_date);
    }

    /**
    * Returns the number of price horizs where price_date &ge; all &#63;.
    *
    * @param price_dates the price_dates
    * @return the number of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static int countBypricehP_date(java.util.Date[] price_dates)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBypricehP_date(price_dates);
    }

    /**
    * Returns all the price horizs where port_id = &#63;.
    *
    * @param port_id the port_id
    * @return the matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_id(
        int port_id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_id(port_id);
    }

    /**
    * Returns a range of all the price horizs where port_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_id the port_id
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @return the range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_id(
        int port_id, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_id(port_id, start, end);
    }

    /**
    * Returns an ordered range of all the price horizs where port_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_id the port_id
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_id(
        int port_id, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_id(port_id, start, end, orderByComparator);
    }

    /**
    * Returns the first price horiz in the ordered set where port_id = &#63;.
    *
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz findBypricehP_id_First(
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_id_First(port_id, orderByComparator);
    }

    /**
    * Returns the first price horiz in the ordered set where port_id = &#63;.
    *
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price horiz, or <code>null</code> if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz fetchBypricehP_id_First(
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBypricehP_id_First(port_id, orderByComparator);
    }

    /**
    * Returns the last price horiz in the ordered set where port_id = &#63;.
    *
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz findBypricehP_id_Last(
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_id_Last(port_id, orderByComparator);
    }

    /**
    * Returns the last price horiz in the ordered set where port_id = &#63;.
    *
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price horiz, or <code>null</code> if a matching price horiz could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz fetchBypricehP_id_Last(
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBypricehP_id_Last(port_id, orderByComparator);
    }

    /**
    * Returns the price horizs before and after the current price horiz in the ordered set where port_id = &#63;.
    *
    * @param priceHorizPK the primary key of the current price horiz
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz[] findBypricehP_id_PrevAndNext(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK,
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_id_PrevAndNext(priceHorizPK, port_id,
            orderByComparator);
    }

    /**
    * Returns all the price horizs where port_id = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_ids the port_ids
    * @return the matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_id(
        int[] port_ids)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_id(port_ids);
    }

    /**
    * Returns a range of all the price horizs where port_id = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_ids the port_ids
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @return the range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_id(
        int[] port_ids, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBypricehP_id(port_ids, start, end);
    }

    /**
    * Returns an ordered range of all the price horizs where port_id = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_ids the port_ids
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findBypricehP_id(
        int[] port_ids, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBypricehP_id(port_ids, start, end, orderByComparator);
    }

    /**
    * Removes all the price horizs where port_id = &#63; from the database.
    *
    * @param port_id the port_id
    * @throws SystemException if a system exception occurred
    */
    public static void removeBypricehP_id(int port_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeBypricehP_id(port_id);
    }

    /**
    * Returns the number of price horizs where port_id = &#63;.
    *
    * @param port_id the port_id
    * @return the number of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static int countBypricehP_id(int port_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBypricehP_id(port_id);
    }

    /**
    * Returns the number of price horizs where port_id = any &#63;.
    *
    * @param port_ids the port_ids
    * @return the number of matching price horizs
    * @throws SystemException if a system exception occurred
    */
    public static int countBypricehP_id(int[] port_ids)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBypricehP_id(port_ids);
    }

    /**
    * Caches the price horiz in the entity cache if it is enabled.
    *
    * @param priceHoriz the price horiz
    */
    public static void cacheResult(
        com.energyindex.liferay.sb.model.PriceHoriz priceHoriz) {
        getPersistence().cacheResult(priceHoriz);
    }

    /**
    * Caches the price horizs in the entity cache if it is enabled.
    *
    * @param priceHorizs the price horizs
    */
    public static void cacheResult(
        java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> priceHorizs) {
        getPersistence().cacheResult(priceHorizs);
    }

    /**
    * Creates a new price horiz with the primary key. Does not add the price horiz to the database.
    *
    * @param priceHorizPK the primary key for the new price horiz
    * @return the new price horiz
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz create(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK) {
        return getPersistence().create(priceHorizPK);
    }

    /**
    * Removes the price horiz with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param priceHorizPK the primary key of the price horiz
    * @return the price horiz that was removed
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz remove(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(priceHorizPK);
    }

    public static com.energyindex.liferay.sb.model.PriceHoriz updateImpl(
        com.energyindex.liferay.sb.model.PriceHoriz priceHoriz)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(priceHoriz);
    }

    /**
    * Returns the price horiz with the primary key or throws a {@link com.energyindex.liferay.sb.NoSuchPriceHorizException} if it could not be found.
    *
    * @param priceHorizPK the primary key of the price horiz
    * @return the price horiz
    * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz findByPrimaryKey(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK)
        throws com.energyindex.liferay.sb.NoSuchPriceHorizException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(priceHorizPK);
    }

    /**
    * Returns the price horiz with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param priceHorizPK the primary key of the price horiz
    * @return the price horiz, or <code>null</code> if a price horiz with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz fetchByPrimaryKey(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(priceHorizPK);
    }

    /**
    * Returns all the price horizs.
    *
    * @return the price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the price horizs.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @return the range of price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the price horizs.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the price horizs from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of price horizs.
    *
    * @return the number of price horizs
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PriceHorizPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PriceHorizPersistence) PortletBeanLocatorUtil.locate(com.energyindex.liferay.sb.service.ClpSerializer.getServletContextName(),
                    PriceHorizPersistence.class.getName());

            ReferenceRegistry.registerReference(PriceHorizUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(PriceHorizPersistence persistence) {
    }
}
