package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.model.Internalindexes;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the internalindexes service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see InternalindexesPersistenceImpl
 * @see InternalindexesUtil
 * @generated
 */
public interface InternalindexesPersistence extends BasePersistence<Internalindexes> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link InternalindexesUtil} to access the internalindexes persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the internalindexeses where product_id = &#63;.
    *
    * @param product_id the product_id
    * @return the matching internalindexeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.energyindex.liferay.sb.model.Internalindexes> findByfindProduct(
        int product_id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the internalindexeses where product_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param product_id the product_id
    * @param start the lower bound of the range of internalindexeses
    * @param end the upper bound of the range of internalindexeses (not inclusive)
    * @return the range of matching internalindexeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.energyindex.liferay.sb.model.Internalindexes> findByfindProduct(
        int product_id, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the internalindexeses where product_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param product_id the product_id
    * @param start the lower bound of the range of internalindexeses
    * @param end the upper bound of the range of internalindexeses (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching internalindexeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.energyindex.liferay.sb.model.Internalindexes> findByfindProduct(
        int product_id, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first internalindexes in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching internalindexes
    * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a matching internalindexes could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.energyindex.liferay.sb.model.Internalindexes findByfindProduct_First(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchInternalindexesException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first internalindexes in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching internalindexes, or <code>null</code> if a matching internalindexes could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.energyindex.liferay.sb.model.Internalindexes fetchByfindProduct_First(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last internalindexes in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching internalindexes
    * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a matching internalindexes could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.energyindex.liferay.sb.model.Internalindexes findByfindProduct_Last(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchInternalindexesException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last internalindexes in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching internalindexes, or <code>null</code> if a matching internalindexes could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.energyindex.liferay.sb.model.Internalindexes fetchByfindProduct_Last(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the internalindexeses before and after the current internalindexes in the ordered set where product_id = &#63;.
    *
    * @param price_id the primary key of the current internalindexes
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next internalindexes
    * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a internalindexes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.energyindex.liferay.sb.model.Internalindexes[] findByfindProduct_PrevAndNext(
        int price_id, int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchInternalindexesException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the internalindexeses where product_id = &#63; from the database.
    *
    * @param product_id the product_id
    * @throws SystemException if a system exception occurred
    */
    public void removeByfindProduct(int product_id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of internalindexeses where product_id = &#63;.
    *
    * @param product_id the product_id
    * @return the number of matching internalindexeses
    * @throws SystemException if a system exception occurred
    */
    public int countByfindProduct(int product_id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the internalindexes in the entity cache if it is enabled.
    *
    * @param internalindexes the internalindexes
    */
    public void cacheResult(
        com.energyindex.liferay.sb.model.Internalindexes internalindexes);

    /**
    * Caches the internalindexeses in the entity cache if it is enabled.
    *
    * @param internalindexeses the internalindexeses
    */
    public void cacheResult(
        java.util.List<com.energyindex.liferay.sb.model.Internalindexes> internalindexeses);

    /**
    * Creates a new internalindexes with the primary key. Does not add the internalindexes to the database.
    *
    * @param price_id the primary key for the new internalindexes
    * @return the new internalindexes
    */
    public com.energyindex.liferay.sb.model.Internalindexes create(int price_id);

    /**
    * Removes the internalindexes with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param price_id the primary key of the internalindexes
    * @return the internalindexes that was removed
    * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a internalindexes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.energyindex.liferay.sb.model.Internalindexes remove(int price_id)
        throws com.energyindex.liferay.sb.NoSuchInternalindexesException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.energyindex.liferay.sb.model.Internalindexes updateImpl(
        com.energyindex.liferay.sb.model.Internalindexes internalindexes)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the internalindexes with the primary key or throws a {@link com.energyindex.liferay.sb.NoSuchInternalindexesException} if it could not be found.
    *
    * @param price_id the primary key of the internalindexes
    * @return the internalindexes
    * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a internalindexes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.energyindex.liferay.sb.model.Internalindexes findByPrimaryKey(
        int price_id)
        throws com.energyindex.liferay.sb.NoSuchInternalindexesException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the internalindexes with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param price_id the primary key of the internalindexes
    * @return the internalindexes, or <code>null</code> if a internalindexes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.energyindex.liferay.sb.model.Internalindexes fetchByPrimaryKey(
        int price_id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the internalindexeses.
    *
    * @return the internalindexeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.energyindex.liferay.sb.model.Internalindexes> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the internalindexeses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of internalindexeses
    * @param end the upper bound of the range of internalindexeses (not inclusive)
    * @return the range of internalindexeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.energyindex.liferay.sb.model.Internalindexes> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the internalindexeses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of internalindexeses
    * @param end the upper bound of the range of internalindexeses (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of internalindexeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.energyindex.liferay.sb.model.Internalindexes> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the internalindexeses from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of internalindexeses.
    *
    * @return the number of internalindexeses
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
