package com.energyindex.liferay.sb.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Normalization. This utility wraps
 * {@link com.energyindex.liferay.sb.service.impl.NormalizationLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author InfinityFrame : Jason Gonin
 * @see NormalizationLocalService
 * @see com.energyindex.liferay.sb.service.base.NormalizationLocalServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.impl.NormalizationLocalServiceImpl
 * @generated
 */
public class NormalizationLocalServiceUtil {
    private static NormalizationLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.energyindex.liferay.sb.service.impl.NormalizationLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the normalization to the database. Also notifies the appropriate model listeners.
    *
    * @param normalization the normalization
    * @return the normalization that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization addNormalization(
        com.energyindex.liferay.sb.model.Normalization normalization)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addNormalization(normalization);
    }

    /**
    * Creates a new normalization with the primary key. Does not add the normalization to the database.
    *
    * @param normId the primary key for the new normalization
    * @return the new normalization
    */
    public static com.energyindex.liferay.sb.model.Normalization createNormalization(
        int normId) {
        return getService().createNormalization(normId);
    }

    /**
    * Deletes the normalization with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param normId the primary key of the normalization
    * @return the normalization that was removed
    * @throws PortalException if a normalization with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization deleteNormalization(
        int normId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteNormalization(normId);
    }

    /**
    * Deletes the normalization from the database. Also notifies the appropriate model listeners.
    *
    * @param normalization the normalization
    * @return the normalization that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization deleteNormalization(
        com.energyindex.liferay.sb.model.Normalization normalization)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteNormalization(normalization);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static com.energyindex.liferay.sb.model.Normalization fetchNormalization(
        int normId) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchNormalization(normId);
    }

    /**
    * Returns the normalization with the primary key.
    *
    * @param normId the primary key of the normalization
    * @return the normalization
    * @throws PortalException if a normalization with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization getNormalization(
        int normId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getNormalization(normId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the normalizations.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of normalizations
    * @param end the upper bound of the range of normalizations (not inclusive)
    * @return the range of normalizations
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.Normalization> getNormalizations(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getNormalizations(start, end);
    }

    /**
    * Returns the number of normalizations.
    *
    * @return the number of normalizations
    * @throws SystemException if a system exception occurred
    */
    public static int getNormalizationsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getNormalizationsCount();
    }

    /**
    * Updates the normalization in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param normalization the normalization
    * @return the normalization that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization updateNormalization(
        com.energyindex.liferay.sb.model.Normalization normalization)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateNormalization(normalization);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static void clearService() {
        _service = null;
    }

    public static NormalizationLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    NormalizationLocalService.class.getName());

            if (invokableLocalService instanceof NormalizationLocalService) {
                _service = (NormalizationLocalService) invokableLocalService;
            } else {
                _service = new NormalizationLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(NormalizationLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(NormalizationLocalService service) {
    }
}
