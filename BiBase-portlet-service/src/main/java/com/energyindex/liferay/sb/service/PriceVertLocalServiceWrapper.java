package com.energyindex.liferay.sb.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PriceVertLocalService}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVertLocalService
 * @generated
 */
public class PriceVertLocalServiceWrapper implements PriceVertLocalService,
    ServiceWrapper<PriceVertLocalService> {
    private PriceVertLocalService _priceVertLocalService;

    public PriceVertLocalServiceWrapper(
        PriceVertLocalService priceVertLocalService) {
        _priceVertLocalService = priceVertLocalService;
    }

    /**
    * Adds the price vert to the database. Also notifies the appropriate model listeners.
    *
    * @param priceVert the price vert
    * @return the price vert that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.PriceVert addPriceVert(
        com.energyindex.liferay.sb.model.PriceVert priceVert)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.addPriceVert(priceVert);
    }

    /**
    * Creates a new price vert with the primary key. Does not add the price vert to the database.
    *
    * @param price_id the primary key for the new price vert
    * @return the new price vert
    */
    @Override
    public com.energyindex.liferay.sb.model.PriceVert createPriceVert(
        long price_id) {
        return _priceVertLocalService.createPriceVert(price_id);
    }

    /**
    * Deletes the price vert with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param price_id the primary key of the price vert
    * @return the price vert that was removed
    * @throws PortalException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.PriceVert deletePriceVert(
        long price_id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.deletePriceVert(price_id);
    }

    /**
    * Deletes the price vert from the database. Also notifies the appropriate model listeners.
    *
    * @param priceVert the price vert
    * @return the price vert that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.PriceVert deletePriceVert(
        com.energyindex.liferay.sb.model.PriceVert priceVert)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.deletePriceVert(priceVert);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _priceVertLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert fetchPriceVert(
        long price_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.fetchPriceVert(price_id);
    }

    /**
    * Returns the price vert with the primary key.
    *
    * @param price_id the primary key of the price vert
    * @return the price vert
    * @throws PortalException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.PriceVert getPriceVert(
        long price_id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.getPriceVert(price_id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the price verts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @return the range of price verts
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<com.energyindex.liferay.sb.model.PriceVert> getPriceVerts(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.getPriceVerts(start, end);
    }

    /**
    * Returns the number of price verts.
    *
    * @return the number of price verts
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getPriceVertsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.getPriceVertsCount();
    }

    /**
    * Updates the price vert in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param priceVert the price vert
    * @return the price vert that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.PriceVert updatePriceVert(
        com.energyindex.liferay.sb.model.PriceVert priceVert)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.updatePriceVert(priceVert);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _priceVertLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _priceVertLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _priceVertLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert addPriceVert(
        long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.addPriceVert(userId, groupId,
            price_close, port_id, product_id, month, day, year, portId,
            serviceContext);
    }

    @Override
    public void addPriceVertResources(
        com.energyindex.liferay.sb.model.PriceVert price,
        java.lang.String[] groupPermissions, java.lang.String[] guestPermissions)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _priceVertLocalService.addPriceVertResources(price, groupPermissions,
            guestPermissions);
    }

    @Override
    public void addPriceVertResources(
        com.energyindex.liferay.sb.model.PriceVert price,
        boolean addGroupPermissions, boolean addGuestPermissions)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _priceVertLocalService.addPriceVertResources(price,
            addGroupPermissions, addGuestPermissions);
    }

    @Override
    public java.util.List<com.energyindex.liferay.sb.model.PriceVert> getPriceVertsByPortId(
        int portId) throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.getPriceVertsByPortId(portId);
    }

    @Override
    public java.util.List<com.energyindex.liferay.sb.model.PriceVert> getPriceVertsByPortId(
        int portId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.getPriceVertsByPortId(portId, start, end);
    }

    @Override
    public int getPriceVertsCountByPortId(int port_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.getPriceVertsCountByPortId(port_id);
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert updatePriceVert(
        int priceId, long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertLocalService.updatePriceVert(priceId, userId, groupId,
            price_close, port_id, product_id, month, day, year, portId,
            serviceContext);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PriceVertLocalService getWrappedPriceVertLocalService() {
        return _priceVertLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPriceVertLocalService(
        PriceVertLocalService priceVertLocalService) {
        _priceVertLocalService = priceVertLocalService;
    }

    @Override
    public PriceVertLocalService getWrappedService() {
        return _priceVertLocalService;
    }

    @Override
    public void setWrappedService(PriceVertLocalService priceVertLocalService) {
        _priceVertLocalService = priceVertLocalService;
    }
}
