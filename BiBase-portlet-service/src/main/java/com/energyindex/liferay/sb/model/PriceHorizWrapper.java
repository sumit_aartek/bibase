package com.energyindex.liferay.sb.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PriceHoriz}.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceHoriz
 * @generated
 */
public class PriceHorizWrapper implements PriceHoriz, ModelWrapper<PriceHoriz> {
    private PriceHoriz _priceHoriz;

    public PriceHorizWrapper(PriceHoriz priceHoriz) {
        _priceHoriz = priceHoriz;
    }

    @Override
    public Class<?> getModelClass() {
        return PriceHoriz.class;
    }

    @Override
    public String getModelClassName() {
        return PriceHoriz.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("priceH_id", getPriceH_id());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("userName", getUserName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("price_date", getPrice_date());
        attributes.put("product_id", getProduct_id());
        attributes.put("price_low", getPrice_low());
        attributes.put("price_high", getPrice_high());
        attributes.put("price_close", getPrice_close());
        attributes.put("price_change", getPrice_change());
        attributes.put("port_id", getPort_id());
        attributes.put("unit_id", getUnit_id());
        attributes.put("currency_id", getCurrency_id());
        attributes.put("contracttype_id", getContracttype_id());
        attributes.put("publish_id", getPublish_id());
        attributes.put("price_id", getPrice_id());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long priceH_id = (Long) attributes.get("priceH_id");

        if (priceH_id != null) {
            setPriceH_id(priceH_id);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String userName = (String) attributes.get("userName");

        if (userName != null) {
            setUserName(userName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Date price_date = (Date) attributes.get("price_date");

        if (price_date != null) {
            setPrice_date(price_date);
        }

        Integer product_id = (Integer) attributes.get("product_id");

        if (product_id != null) {
            setProduct_id(product_id);
        }

        Double price_low = (Double) attributes.get("price_low");

        if (price_low != null) {
            setPrice_low(price_low);
        }

        Double price_high = (Double) attributes.get("price_high");

        if (price_high != null) {
            setPrice_high(price_high);
        }

        Double price_close = (Double) attributes.get("price_close");

        if (price_close != null) {
            setPrice_close(price_close);
        }

        Double price_change = (Double) attributes.get("price_change");

        if (price_change != null) {
            setPrice_change(price_change);
        }

        Integer port_id = (Integer) attributes.get("port_id");

        if (port_id != null) {
            setPort_id(port_id);
        }

        Integer unit_id = (Integer) attributes.get("unit_id");

        if (unit_id != null) {
            setUnit_id(unit_id);
        }

        Integer currency_id = (Integer) attributes.get("currency_id");

        if (currency_id != null) {
            setCurrency_id(currency_id);
        }

        Integer contracttype_id = (Integer) attributes.get("contracttype_id");

        if (contracttype_id != null) {
            setContracttype_id(contracttype_id);
        }

        Integer publish_id = (Integer) attributes.get("publish_id");

        if (publish_id != null) {
            setPublish_id(publish_id);
        }

        Long price_id = (Long) attributes.get("price_id");

        if (price_id != null) {
            setPrice_id(price_id);
        }
    }

    /**
    * Returns the primary key of this price horiz.
    *
    * @return the primary key of this price horiz
    */
    @Override
    public com.energyindex.liferay.sb.service.persistence.PriceHorizPK getPrimaryKey() {
        return _priceHoriz.getPrimaryKey();
    }

    /**
    * Sets the primary key of this price horiz.
    *
    * @param primaryKey the primary key of this price horiz
    */
    @Override
    public void setPrimaryKey(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK primaryKey) {
        _priceHoriz.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the price h_id of this price horiz.
    *
    * @return the price h_id of this price horiz
    */
    @Override
    public long getPriceH_id() {
        return _priceHoriz.getPriceH_id();
    }

    /**
    * Sets the price h_id of this price horiz.
    *
    * @param priceH_id the price h_id of this price horiz
    */
    @Override
    public void setPriceH_id(long priceH_id) {
        _priceHoriz.setPriceH_id(priceH_id);
    }

    /**
    * Returns the group ID of this price horiz.
    *
    * @return the group ID of this price horiz
    */
    @Override
    public long getGroupId() {
        return _priceHoriz.getGroupId();
    }

    /**
    * Sets the group ID of this price horiz.
    *
    * @param groupId the group ID of this price horiz
    */
    @Override
    public void setGroupId(long groupId) {
        _priceHoriz.setGroupId(groupId);
    }

    /**
    * Returns the company ID of this price horiz.
    *
    * @return the company ID of this price horiz
    */
    @Override
    public long getCompanyId() {
        return _priceHoriz.getCompanyId();
    }

    /**
    * Sets the company ID of this price horiz.
    *
    * @param companyId the company ID of this price horiz
    */
    @Override
    public void setCompanyId(long companyId) {
        _priceHoriz.setCompanyId(companyId);
    }

    /**
    * Returns the user ID of this price horiz.
    *
    * @return the user ID of this price horiz
    */
    @Override
    public long getUserId() {
        return _priceHoriz.getUserId();
    }

    /**
    * Sets the user ID of this price horiz.
    *
    * @param userId the user ID of this price horiz
    */
    @Override
    public void setUserId(long userId) {
        _priceHoriz.setUserId(userId);
    }

    /**
    * Returns the user uuid of this price horiz.
    *
    * @return the user uuid of this price horiz
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceHoriz.getUserUuid();
    }

    /**
    * Sets the user uuid of this price horiz.
    *
    * @param userUuid the user uuid of this price horiz
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _priceHoriz.setUserUuid(userUuid);
    }

    /**
    * Returns the user name of this price horiz.
    *
    * @return the user name of this price horiz
    */
    @Override
    public java.lang.String getUserName() {
        return _priceHoriz.getUserName();
    }

    /**
    * Sets the user name of this price horiz.
    *
    * @param userName the user name of this price horiz
    */
    @Override
    public void setUserName(java.lang.String userName) {
        _priceHoriz.setUserName(userName);
    }

    /**
    * Returns the create date of this price horiz.
    *
    * @return the create date of this price horiz
    */
    @Override
    public java.util.Date getCreateDate() {
        return _priceHoriz.getCreateDate();
    }

    /**
    * Sets the create date of this price horiz.
    *
    * @param createDate the create date of this price horiz
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _priceHoriz.setCreateDate(createDate);
    }

    /**
    * Returns the modified date of this price horiz.
    *
    * @return the modified date of this price horiz
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _priceHoriz.getModifiedDate();
    }

    /**
    * Sets the modified date of this price horiz.
    *
    * @param modifiedDate the modified date of this price horiz
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _priceHoriz.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the price_date of this price horiz.
    *
    * @return the price_date of this price horiz
    */
    @Override
    public java.util.Date getPrice_date() {
        return _priceHoriz.getPrice_date();
    }

    /**
    * Sets the price_date of this price horiz.
    *
    * @param price_date the price_date of this price horiz
    */
    @Override
    public void setPrice_date(java.util.Date price_date) {
        _priceHoriz.setPrice_date(price_date);
    }

    /**
    * Returns the product_id of this price horiz.
    *
    * @return the product_id of this price horiz
    */
    @Override
    public int getProduct_id() {
        return _priceHoriz.getProduct_id();
    }

    /**
    * Sets the product_id of this price horiz.
    *
    * @param product_id the product_id of this price horiz
    */
    @Override
    public void setProduct_id(int product_id) {
        _priceHoriz.setProduct_id(product_id);
    }

    /**
    * Returns the price_low of this price horiz.
    *
    * @return the price_low of this price horiz
    */
    @Override
    public double getPrice_low() {
        return _priceHoriz.getPrice_low();
    }

    /**
    * Sets the price_low of this price horiz.
    *
    * @param price_low the price_low of this price horiz
    */
    @Override
    public void setPrice_low(double price_low) {
        _priceHoriz.setPrice_low(price_low);
    }

    /**
    * Returns the price_high of this price horiz.
    *
    * @return the price_high of this price horiz
    */
    @Override
    public double getPrice_high() {
        return _priceHoriz.getPrice_high();
    }

    /**
    * Sets the price_high of this price horiz.
    *
    * @param price_high the price_high of this price horiz
    */
    @Override
    public void setPrice_high(double price_high) {
        _priceHoriz.setPrice_high(price_high);
    }

    /**
    * Returns the price_close of this price horiz.
    *
    * @return the price_close of this price horiz
    */
    @Override
    public double getPrice_close() {
        return _priceHoriz.getPrice_close();
    }

    /**
    * Sets the price_close of this price horiz.
    *
    * @param price_close the price_close of this price horiz
    */
    @Override
    public void setPrice_close(double price_close) {
        _priceHoriz.setPrice_close(price_close);
    }

    /**
    * Returns the price_change of this price horiz.
    *
    * @return the price_change of this price horiz
    */
    @Override
    public double getPrice_change() {
        return _priceHoriz.getPrice_change();
    }

    /**
    * Sets the price_change of this price horiz.
    *
    * @param price_change the price_change of this price horiz
    */
    @Override
    public void setPrice_change(double price_change) {
        _priceHoriz.setPrice_change(price_change);
    }

    /**
    * Returns the port_id of this price horiz.
    *
    * @return the port_id of this price horiz
    */
    @Override
    public int getPort_id() {
        return _priceHoriz.getPort_id();
    }

    /**
    * Sets the port_id of this price horiz.
    *
    * @param port_id the port_id of this price horiz
    */
    @Override
    public void setPort_id(int port_id) {
        _priceHoriz.setPort_id(port_id);
    }

    /**
    * Returns the unit_id of this price horiz.
    *
    * @return the unit_id of this price horiz
    */
    @Override
    public int getUnit_id() {
        return _priceHoriz.getUnit_id();
    }

    /**
    * Sets the unit_id of this price horiz.
    *
    * @param unit_id the unit_id of this price horiz
    */
    @Override
    public void setUnit_id(int unit_id) {
        _priceHoriz.setUnit_id(unit_id);
    }

    /**
    * Returns the currency_id of this price horiz.
    *
    * @return the currency_id of this price horiz
    */
    @Override
    public int getCurrency_id() {
        return _priceHoriz.getCurrency_id();
    }

    /**
    * Sets the currency_id of this price horiz.
    *
    * @param currency_id the currency_id of this price horiz
    */
    @Override
    public void setCurrency_id(int currency_id) {
        _priceHoriz.setCurrency_id(currency_id);
    }

    /**
    * Returns the contracttype_id of this price horiz.
    *
    * @return the contracttype_id of this price horiz
    */
    @Override
    public int getContracttype_id() {
        return _priceHoriz.getContracttype_id();
    }

    /**
    * Sets the contracttype_id of this price horiz.
    *
    * @param contracttype_id the contracttype_id of this price horiz
    */
    @Override
    public void setContracttype_id(int contracttype_id) {
        _priceHoriz.setContracttype_id(contracttype_id);
    }

    /**
    * Returns the publish_id of this price horiz.
    *
    * @return the publish_id of this price horiz
    */
    @Override
    public int getPublish_id() {
        return _priceHoriz.getPublish_id();
    }

    /**
    * Sets the publish_id of this price horiz.
    *
    * @param publish_id the publish_id of this price horiz
    */
    @Override
    public void setPublish_id(int publish_id) {
        _priceHoriz.setPublish_id(publish_id);
    }

    /**
    * Returns the price_id of this price horiz.
    *
    * @return the price_id of this price horiz
    */
    @Override
    public long getPrice_id() {
        return _priceHoriz.getPrice_id();
    }

    /**
    * Sets the price_id of this price horiz.
    *
    * @param price_id the price_id of this price horiz
    */
    @Override
    public void setPrice_id(long price_id) {
        _priceHoriz.setPrice_id(price_id);
    }

    @Override
    public boolean isNew() {
        return _priceHoriz.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _priceHoriz.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _priceHoriz.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _priceHoriz.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _priceHoriz.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _priceHoriz.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _priceHoriz.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _priceHoriz.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _priceHoriz.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _priceHoriz.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _priceHoriz.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PriceHorizWrapper((PriceHoriz) _priceHoriz.clone());
    }

    @Override
    public int compareTo(com.energyindex.liferay.sb.model.PriceHoriz priceHoriz) {
        return _priceHoriz.compareTo(priceHoriz);
    }

    @Override
    public int hashCode() {
        return _priceHoriz.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<com.energyindex.liferay.sb.model.PriceHoriz> toCacheModel() {
        return _priceHoriz.toCacheModel();
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceHoriz toEscapedModel() {
        return new PriceHorizWrapper(_priceHoriz.toEscapedModel());
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceHoriz toUnescapedModel() {
        return new PriceHorizWrapper(_priceHoriz.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _priceHoriz.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _priceHoriz.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _priceHoriz.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PriceHorizWrapper)) {
            return false;
        }

        PriceHorizWrapper priceHorizWrapper = (PriceHorizWrapper) obj;

        if (Validator.equals(_priceHoriz, priceHorizWrapper._priceHoriz)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public PriceHoriz getWrappedPriceHoriz() {
        return _priceHoriz;
    }

    @Override
    public PriceHoriz getWrappedModel() {
        return _priceHoriz;
    }

    @Override
    public void resetOriginalValues() {
        _priceHoriz.resetOriginalValues();
    }
}
