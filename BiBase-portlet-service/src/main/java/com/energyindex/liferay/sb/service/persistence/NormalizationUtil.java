package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.model.Normalization;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the normalization service. This utility wraps {@link NormalizationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see NormalizationPersistence
 * @see NormalizationPersistenceImpl
 * @generated
 */
public class NormalizationUtil {
    private static NormalizationPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Normalization normalization) {
        getPersistence().clearCache(normalization);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Normalization> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Normalization> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Normalization> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Normalization update(Normalization normalization)
        throws SystemException {
        return getPersistence().update(normalization);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Normalization update(Normalization normalization,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(normalization, serviceContext);
    }

    /**
    * Returns all the normalizations where normType = &#63;.
    *
    * @param normType the norm type
    * @return the matching normalizations
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.Normalization> findByNormCollection(
        java.lang.String normType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByNormCollection(normType);
    }

    /**
    * Returns a range of all the normalizations where normType = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param normType the norm type
    * @param start the lower bound of the range of normalizations
    * @param end the upper bound of the range of normalizations (not inclusive)
    * @return the range of matching normalizations
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.Normalization> findByNormCollection(
        java.lang.String normType, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByNormCollection(normType, start, end);
    }

    /**
    * Returns an ordered range of all the normalizations where normType = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param normType the norm type
    * @param start the lower bound of the range of normalizations
    * @param end the upper bound of the range of normalizations (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching normalizations
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.Normalization> findByNormCollection(
        java.lang.String normType, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByNormCollection(normType, start, end, orderByComparator);
    }

    /**
    * Returns the first normalization in the ordered set where normType = &#63;.
    *
    * @param normType the norm type
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching normalization
    * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a matching normalization could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization findByNormCollection_First(
        java.lang.String normType,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchNormalizationException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByNormCollection_First(normType, orderByComparator);
    }

    /**
    * Returns the first normalization in the ordered set where normType = &#63;.
    *
    * @param normType the norm type
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching normalization, or <code>null</code> if a matching normalization could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization fetchByNormCollection_First(
        java.lang.String normType,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByNormCollection_First(normType, orderByComparator);
    }

    /**
    * Returns the last normalization in the ordered set where normType = &#63;.
    *
    * @param normType the norm type
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching normalization
    * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a matching normalization could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization findByNormCollection_Last(
        java.lang.String normType,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchNormalizationException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByNormCollection_Last(normType, orderByComparator);
    }

    /**
    * Returns the last normalization in the ordered set where normType = &#63;.
    *
    * @param normType the norm type
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching normalization, or <code>null</code> if a matching normalization could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization fetchByNormCollection_Last(
        java.lang.String normType,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByNormCollection_Last(normType, orderByComparator);
    }

    /**
    * Returns the normalizations before and after the current normalization in the ordered set where normType = &#63;.
    *
    * @param normId the primary key of the current normalization
    * @param normType the norm type
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next normalization
    * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a normalization with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization[] findByNormCollection_PrevAndNext(
        int normId, java.lang.String normType,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchNormalizationException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByNormCollection_PrevAndNext(normId, normType,
            orderByComparator);
    }

    /**
    * Removes all the normalizations where normType = &#63; from the database.
    *
    * @param normType the norm type
    * @throws SystemException if a system exception occurred
    */
    public static void removeByNormCollection(java.lang.String normType)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByNormCollection(normType);
    }

    /**
    * Returns the number of normalizations where normType = &#63;.
    *
    * @param normType the norm type
    * @return the number of matching normalizations
    * @throws SystemException if a system exception occurred
    */
    public static int countByNormCollection(java.lang.String normType)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByNormCollection(normType);
    }

    /**
    * Caches the normalization in the entity cache if it is enabled.
    *
    * @param normalization the normalization
    */
    public static void cacheResult(
        com.energyindex.liferay.sb.model.Normalization normalization) {
        getPersistence().cacheResult(normalization);
    }

    /**
    * Caches the normalizations in the entity cache if it is enabled.
    *
    * @param normalizations the normalizations
    */
    public static void cacheResult(
        java.util.List<com.energyindex.liferay.sb.model.Normalization> normalizations) {
        getPersistence().cacheResult(normalizations);
    }

    /**
    * Creates a new normalization with the primary key. Does not add the normalization to the database.
    *
    * @param normId the primary key for the new normalization
    * @return the new normalization
    */
    public static com.energyindex.liferay.sb.model.Normalization create(
        int normId) {
        return getPersistence().create(normId);
    }

    /**
    * Removes the normalization with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param normId the primary key of the normalization
    * @return the normalization that was removed
    * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a normalization with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization remove(
        int normId)
        throws com.energyindex.liferay.sb.NoSuchNormalizationException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(normId);
    }

    public static com.energyindex.liferay.sb.model.Normalization updateImpl(
        com.energyindex.liferay.sb.model.Normalization normalization)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(normalization);
    }

    /**
    * Returns the normalization with the primary key or throws a {@link com.energyindex.liferay.sb.NoSuchNormalizationException} if it could not be found.
    *
    * @param normId the primary key of the normalization
    * @return the normalization
    * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a normalization with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization findByPrimaryKey(
        int normId)
        throws com.energyindex.liferay.sb.NoSuchNormalizationException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(normId);
    }

    /**
    * Returns the normalization with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param normId the primary key of the normalization
    * @return the normalization, or <code>null</code> if a normalization with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.Normalization fetchByPrimaryKey(
        int normId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(normId);
    }

    /**
    * Returns all the normalizations.
    *
    * @return the normalizations
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.Normalization> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the normalizations.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of normalizations
    * @param end the upper bound of the range of normalizations (not inclusive)
    * @return the range of normalizations
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.Normalization> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the normalizations.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of normalizations
    * @param end the upper bound of the range of normalizations (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of normalizations
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.Normalization> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the normalizations from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of normalizations.
    *
    * @return the number of normalizations
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static NormalizationPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (NormalizationPersistence) PortletBeanLocatorUtil.locate(com.energyindex.liferay.sb.service.ClpSerializer.getServletContextName(),
                    NormalizationPersistence.class.getName());

            ReferenceRegistry.registerReference(NormalizationUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(NormalizationPersistence persistence) {
    }
}
