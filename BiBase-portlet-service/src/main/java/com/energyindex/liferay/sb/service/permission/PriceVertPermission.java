package com.energyindex.liferay.sb.service.permission;

import com.energyindex.liferay.sb.model.PriceVert;
import com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.security.permission.PermissionChecker;

/**not used duplic PriceVertpermition...*/
public class PriceVertPermission {


		public static void check(
				PermissionChecker permissionChecker, PriceVert price, String actionId)
			throws PortalException {

			if (!contains(permissionChecker, price, actionId)) {
				throw new PrincipalException();
			}
		}

		public static void check(
				PermissionChecker permissionChecker, long priceId, String actionId)
			throws PortalException, SystemException {

			if (!contains(permissionChecker, priceId, actionId)) {
				throw new PrincipalException();
			}
		}

		public static boolean contains(
			PermissionChecker permissionChecker, PriceVert price, String actionId) {
			if (permissionChecker.hasOwnerPermission(
					price.getCompanyId(), PriceVert.class.getName(), price.getPrice_id(),
					price.getUserId(), actionId)) {

				return true;
			}

			return permissionChecker.hasPermission(
				price.getGroupId(), PriceVert.class.getName(), price.getPrice_id(),
				actionId);
		}

		public static boolean contains(
				PermissionChecker permissionChecker, long price_id, String actionId)
			throws PortalException, SystemException {

			PriceVert price = PriceVertLocalServiceUtil.getPriceVert(price_id);

			return contains(permissionChecker, price, actionId);
		}

	}