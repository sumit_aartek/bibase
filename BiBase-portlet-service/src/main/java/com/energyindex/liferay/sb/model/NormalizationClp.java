package com.energyindex.liferay.sb.model;

import com.energyindex.liferay.sb.service.ClpSerializer;
import com.energyindex.liferay.sb.service.NormalizationLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class NormalizationClp extends BaseModelImpl<Normalization>
    implements Normalization {
    private int _normId;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private String _userUuid;
    private String _userName;
    private Date _createDate;
    private Date _modifiedDate;
    private String _searchExpr;
    private String _normType;
    private String _longDesc;
    private String _shortDesc;
    private int _isPrefix;
    private int _isPostfix;
    private BaseModel<?> _normalizationRemoteModel;
    private Class<?> _clpSerializerClass = com.energyindex.liferay.sb.service.ClpSerializer.class;

    public NormalizationClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Normalization.class;
    }

    @Override
    public String getModelClassName() {
        return Normalization.class.getName();
    }

    @Override
    public int getPrimaryKey() {
        return _normId;
    }

    @Override
    public void setPrimaryKey(int primaryKey) {
        setNormId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _normId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Integer) primaryKeyObj).intValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("normId", getNormId());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("userName", getUserName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("searchExpr", getSearchExpr());
        attributes.put("normType", getNormType());
        attributes.put("longDesc", getLongDesc());
        attributes.put("shortDesc", getShortDesc());
        attributes.put("isPrefix", getIsPrefix());
        attributes.put("isPostfix", getIsPostfix());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer normId = (Integer) attributes.get("normId");

        if (normId != null) {
            setNormId(normId);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String userName = (String) attributes.get("userName");

        if (userName != null) {
            setUserName(userName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String searchExpr = (String) attributes.get("searchExpr");

        if (searchExpr != null) {
            setSearchExpr(searchExpr);
        }

        String normType = (String) attributes.get("normType");

        if (normType != null) {
            setNormType(normType);
        }

        String longDesc = (String) attributes.get("longDesc");

        if (longDesc != null) {
            setLongDesc(longDesc);
        }

        String shortDesc = (String) attributes.get("shortDesc");

        if (shortDesc != null) {
            setShortDesc(shortDesc);
        }

        Integer isPrefix = (Integer) attributes.get("isPrefix");

        if (isPrefix != null) {
            setIsPrefix(isPrefix);
        }

        Integer isPostfix = (Integer) attributes.get("isPostfix");

        if (isPostfix != null) {
            setIsPostfix(isPostfix);
        }
    }

    @Override
    public int getNormId() {
        return _normId;
    }

    @Override
    public void setNormId(int normId) {
        _normId = normId;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setNormId", int.class);

                method.invoke(_normalizationRemoteModel, normId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getGroupId() {
        return _groupId;
    }

    @Override
    public void setGroupId(long groupId) {
        _groupId = groupId;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setGroupId", long.class);

                method.invoke(_normalizationRemoteModel, groupId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCompanyId() {
        return _companyId;
    }

    @Override
    public void setCompanyId(long companyId) {
        _companyId = companyId;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setCompanyId", long.class);

                method.invoke(_normalizationRemoteModel, companyId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_normalizationRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public String getUserName() {
        return _userName;
    }

    @Override
    public void setUserName(String userName) {
        _userName = userName;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setUserName", String.class);

                method.invoke(_normalizationRemoteModel, userName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_normalizationRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_normalizationRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getSearchExpr() {
        return _searchExpr;
    }

    @Override
    public void setSearchExpr(String searchExpr) {
        _searchExpr = searchExpr;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setSearchExpr", String.class);

                method.invoke(_normalizationRemoteModel, searchExpr);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNormType() {
        return _normType;
    }

    @Override
    public void setNormType(String normType) {
        _normType = normType;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setNormType", String.class);

                method.invoke(_normalizationRemoteModel, normType);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLongDesc() {
        return _longDesc;
    }

    @Override
    public void setLongDesc(String longDesc) {
        _longDesc = longDesc;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setLongDesc", String.class);

                method.invoke(_normalizationRemoteModel, longDesc);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getShortDesc() {
        return _shortDesc;
    }

    @Override
    public void setShortDesc(String shortDesc) {
        _shortDesc = shortDesc;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setShortDesc", String.class);

                method.invoke(_normalizationRemoteModel, shortDesc);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getIsPrefix() {
        return _isPrefix;
    }

    @Override
    public void setIsPrefix(int isPrefix) {
        _isPrefix = isPrefix;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setIsPrefix", int.class);

                method.invoke(_normalizationRemoteModel, isPrefix);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getIsPostfix() {
        return _isPostfix;
    }

    @Override
    public void setIsPostfix(int isPostfix) {
        _isPostfix = isPostfix;

        if (_normalizationRemoteModel != null) {
            try {
                Class<?> clazz = _normalizationRemoteModel.getClass();

                Method method = clazz.getMethod("setIsPostfix", int.class);

                method.invoke(_normalizationRemoteModel, isPostfix);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getNormalizationRemoteModel() {
        return _normalizationRemoteModel;
    }

    public void setNormalizationRemoteModel(
        BaseModel<?> normalizationRemoteModel) {
        _normalizationRemoteModel = normalizationRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _normalizationRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_normalizationRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            NormalizationLocalServiceUtil.addNormalization(this);
        } else {
            NormalizationLocalServiceUtil.updateNormalization(this);
        }
    }

    @Override
    public Normalization toEscapedModel() {
        return (Normalization) ProxyUtil.newProxyInstance(Normalization.class.getClassLoader(),
            new Class[] { Normalization.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        NormalizationClp clone = new NormalizationClp();

        clone.setNormId(getNormId());
        clone.setGroupId(getGroupId());
        clone.setCompanyId(getCompanyId());
        clone.setUserId(getUserId());
        clone.setUserName(getUserName());
        clone.setCreateDate(getCreateDate());
        clone.setModifiedDate(getModifiedDate());
        clone.setSearchExpr(getSearchExpr());
        clone.setNormType(getNormType());
        clone.setLongDesc(getLongDesc());
        clone.setShortDesc(getShortDesc());
        clone.setIsPrefix(getIsPrefix());
        clone.setIsPostfix(getIsPostfix());

        return clone;
    }

    @Override
    public int compareTo(Normalization normalization) {
        int value = 0;

        if (getNormId() < normalization.getNormId()) {
            value = -1;
        } else if (getNormId() > normalization.getNormId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof NormalizationClp)) {
            return false;
        }

        NormalizationClp normalization = (NormalizationClp) obj;

        int primaryKey = normalization.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(27);

        sb.append("{normId=");
        sb.append(getNormId());
        sb.append(", groupId=");
        sb.append(getGroupId());
        sb.append(", companyId=");
        sb.append(getCompanyId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", userName=");
        sb.append(getUserName());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", searchExpr=");
        sb.append(getSearchExpr());
        sb.append(", normType=");
        sb.append(getNormType());
        sb.append(", longDesc=");
        sb.append(getLongDesc());
        sb.append(", shortDesc=");
        sb.append(getShortDesc());
        sb.append(", isPrefix=");
        sb.append(getIsPrefix());
        sb.append(", isPostfix=");
        sb.append(getIsPostfix());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(43);

        sb.append("<model><model-name>");
        sb.append("com.energyindex.liferay.sb.model.Normalization");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>normId</column-name><column-value><![CDATA[");
        sb.append(getNormId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>groupId</column-name><column-value><![CDATA[");
        sb.append(getGroupId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>companyId</column-name><column-value><![CDATA[");
        sb.append(getCompanyId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userName</column-name><column-value><![CDATA[");
        sb.append(getUserName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>searchExpr</column-name><column-value><![CDATA[");
        sb.append(getSearchExpr());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>normType</column-name><column-value><![CDATA[");
        sb.append(getNormType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>longDesc</column-name><column-value><![CDATA[");
        sb.append(getLongDesc());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>shortDesc</column-name><column-value><![CDATA[");
        sb.append(getShortDesc());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>isPrefix</column-name><column-value><![CDATA[");
        sb.append(getIsPrefix());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>isPostfix</column-name><column-value><![CDATA[");
        sb.append(getIsPostfix());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
