package com.energyindex.liferay.sb.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Internalindexes service. Represents a row in the &quot;BiBase_Internalindexes&quot; database table, with each column mapped to a property of this class.
 *
 * @author InfinityFrame : Jason Gonin
 * @see InternalindexesModel
 * @see com.energyindex.liferay.sb.model.impl.InternalindexesImpl
 * @see com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl
 * @generated
 */
public interface Internalindexes extends InternalindexesModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.energyindex.liferay.sb.model.impl.InternalindexesImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
