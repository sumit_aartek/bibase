package com.energyindex.liferay.sb.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the PriceHoriz service. Represents a row in the &quot;BiBase_PriceHoriz&quot; database table, with each column mapped to a property of this class.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceHorizModel
 * @see com.energyindex.liferay.sb.model.impl.PriceHorizImpl
 * @see com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl
 * @generated
 */
public interface PriceHoriz extends PriceHorizModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.energyindex.liferay.sb.model.impl.PriceHorizImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
