package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.model.PriceVert;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the price vert service. This utility wraps {@link PriceVertPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVertPersistence
 * @see PriceVertPersistenceImpl
 * @generated
 */
public class PriceVertUtil {
    private static PriceVertPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(PriceVert priceVert) {
        getPersistence().clearCache(priceVert);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<PriceVert> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<PriceVert> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<PriceVert> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static PriceVert update(PriceVert priceVert)
        throws SystemException {
        return getPersistence().update(priceVert);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static PriceVert update(PriceVert priceVert,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(priceVert, serviceContext);
    }

    /**
    * Returns all the price verts where product_id = &#63;.
    *
    * @param product_id the product_id
    * @return the matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByproduct_id(
        int product_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByproduct_id(product_id);
    }

    /**
    * Returns a range of all the price verts where product_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param product_id the product_id
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @return the range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByproduct_id(
        int product_id, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByproduct_id(product_id, start, end);
    }

    /**
    * Returns an ordered range of all the price verts where product_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param product_id the product_id
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByproduct_id(
        int product_id, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByproduct_id(product_id, start, end, orderByComparator);
    }

    /**
    * Returns the first price vert in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert findByproduct_id_First(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByproduct_id_First(product_id, orderByComparator);
    }

    /**
    * Returns the first price vert in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price vert, or <code>null</code> if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert fetchByproduct_id_First(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByproduct_id_First(product_id, orderByComparator);
    }

    /**
    * Returns the last price vert in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert findByproduct_id_Last(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByproduct_id_Last(product_id, orderByComparator);
    }

    /**
    * Returns the last price vert in the ordered set where product_id = &#63;.
    *
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price vert, or <code>null</code> if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert fetchByproduct_id_Last(
        int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByproduct_id_Last(product_id, orderByComparator);
    }

    /**
    * Returns the price verts before and after the current price vert in the ordered set where product_id = &#63;.
    *
    * @param price_id the primary key of the current price vert
    * @param product_id the product_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert[] findByproduct_id_PrevAndNext(
        long price_id, int product_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByproduct_id_PrevAndNext(price_id, product_id,
            orderByComparator);
    }

    /**
    * Removes all the price verts where product_id = &#63; from the database.
    *
    * @param product_id the product_id
    * @throws SystemException if a system exception occurred
    */
    public static void removeByproduct_id(int product_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByproduct_id(product_id);
    }

    /**
    * Returns the number of price verts where product_id = &#63;.
    *
    * @param product_id the product_id
    * @return the number of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static int countByproduct_id(int product_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByproduct_id(product_id);
    }

    /**
    * Returns all the price verts where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @return the matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByprice_date(
        java.util.Date price_date)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByprice_date(price_date);
    }

    /**
    * Returns a range of all the price verts where price_date &ge; &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_date the price_date
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @return the range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByprice_date(
        java.util.Date price_date, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByprice_date(price_date, start, end);
    }

    /**
    * Returns an ordered range of all the price verts where price_date &ge; &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_date the price_date
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByprice_date(
        java.util.Date price_date, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByprice_date(price_date, start, end, orderByComparator);
    }

    /**
    * Returns the first price vert in the ordered set where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert findByprice_date_First(
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByprice_date_First(price_date, orderByComparator);
    }

    /**
    * Returns the first price vert in the ordered set where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price vert, or <code>null</code> if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert fetchByprice_date_First(
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByprice_date_First(price_date, orderByComparator);
    }

    /**
    * Returns the last price vert in the ordered set where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert findByprice_date_Last(
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByprice_date_Last(price_date, orderByComparator);
    }

    /**
    * Returns the last price vert in the ordered set where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price vert, or <code>null</code> if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert fetchByprice_date_Last(
        java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByprice_date_Last(price_date, orderByComparator);
    }

    /**
    * Returns the price verts before and after the current price vert in the ordered set where price_date &ge; &#63;.
    *
    * @param price_id the primary key of the current price vert
    * @param price_date the price_date
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert[] findByprice_date_PrevAndNext(
        long price_id, java.util.Date price_date,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByprice_date_PrevAndNext(price_id, price_date,
            orderByComparator);
    }

    /**
    * Returns all the price verts where price_date &ge; all &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_dates the price_dates
    * @return the matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByprice_date(
        java.util.Date[] price_dates)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByprice_date(price_dates);
    }

    /**
    * Returns a range of all the price verts where price_date &ge; all &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_dates the price_dates
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @return the range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByprice_date(
        java.util.Date[] price_dates, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByprice_date(price_dates, start, end);
    }

    /**
    * Returns an ordered range of all the price verts where price_date &ge; all &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param price_dates the price_dates
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByprice_date(
        java.util.Date[] price_dates, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByprice_date(price_dates, start, end, orderByComparator);
    }

    /**
    * Removes all the price verts where price_date &ge; &#63; from the database.
    *
    * @param price_date the price_date
    * @throws SystemException if a system exception occurred
    */
    public static void removeByprice_date(java.util.Date price_date)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByprice_date(price_date);
    }

    /**
    * Returns the number of price verts where price_date &ge; &#63;.
    *
    * @param price_date the price_date
    * @return the number of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static int countByprice_date(java.util.Date price_date)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByprice_date(price_date);
    }

    /**
    * Returns the number of price verts where price_date &ge; all &#63;.
    *
    * @param price_dates the price_dates
    * @return the number of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static int countByprice_date(java.util.Date[] price_dates)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByprice_date(price_dates);
    }

    /**
    * Returns all the price verts where port_id = &#63;.
    *
    * @param port_id the port_id
    * @return the matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByport_id(
        int port_id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByport_id(port_id);
    }

    /**
    * Returns a range of all the price verts where port_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_id the port_id
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @return the range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByport_id(
        int port_id, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByport_id(port_id, start, end);
    }

    /**
    * Returns an ordered range of all the price verts where port_id = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_id the port_id
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByport_id(
        int port_id, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByport_id(port_id, start, end, orderByComparator);
    }

    /**
    * Returns the first price vert in the ordered set where port_id = &#63;.
    *
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert findByport_id_First(
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByport_id_First(port_id, orderByComparator);
    }

    /**
    * Returns the first price vert in the ordered set where port_id = &#63;.
    *
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching price vert, or <code>null</code> if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert fetchByport_id_First(
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByport_id_First(port_id, orderByComparator);
    }

    /**
    * Returns the last price vert in the ordered set where port_id = &#63;.
    *
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert findByport_id_Last(
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByport_id_Last(port_id, orderByComparator);
    }

    /**
    * Returns the last price vert in the ordered set where port_id = &#63;.
    *
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching price vert, or <code>null</code> if a matching price vert could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert fetchByport_id_Last(
        int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByport_id_Last(port_id, orderByComparator);
    }

    /**
    * Returns the price verts before and after the current price vert in the ordered set where port_id = &#63;.
    *
    * @param price_id the primary key of the current price vert
    * @param port_id the port_id
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert[] findByport_id_PrevAndNext(
        long price_id, int port_id,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByport_id_PrevAndNext(price_id, port_id,
            orderByComparator);
    }

    /**
    * Returns all the price verts where port_id = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_ids the port_ids
    * @return the matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByport_id(
        int[] port_ids)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByport_id(port_ids);
    }

    /**
    * Returns a range of all the price verts where port_id = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_ids the port_ids
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @return the range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByport_id(
        int[] port_ids, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByport_id(port_ids, start, end);
    }

    /**
    * Returns an ordered range of all the price verts where port_id = any &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param port_ids the port_ids
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findByport_id(
        int[] port_ids, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByport_id(port_ids, start, end, orderByComparator);
    }

    /**
    * Removes all the price verts where port_id = &#63; from the database.
    *
    * @param port_id the port_id
    * @throws SystemException if a system exception occurred
    */
    public static void removeByport_id(int port_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByport_id(port_id);
    }

    /**
    * Returns the number of price verts where port_id = &#63;.
    *
    * @param port_id the port_id
    * @return the number of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static int countByport_id(int port_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByport_id(port_id);
    }

    /**
    * Returns the number of price verts where port_id = any &#63;.
    *
    * @param port_ids the port_ids
    * @return the number of matching price verts
    * @throws SystemException if a system exception occurred
    */
    public static int countByport_id(int[] port_ids)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByport_id(port_ids);
    }

    /**
    * Caches the price vert in the entity cache if it is enabled.
    *
    * @param priceVert the price vert
    */
    public static void cacheResult(
        com.energyindex.liferay.sb.model.PriceVert priceVert) {
        getPersistence().cacheResult(priceVert);
    }

    /**
    * Caches the price verts in the entity cache if it is enabled.
    *
    * @param priceVerts the price verts
    */
    public static void cacheResult(
        java.util.List<com.energyindex.liferay.sb.model.PriceVert> priceVerts) {
        getPersistence().cacheResult(priceVerts);
    }

    /**
    * Creates a new price vert with the primary key. Does not add the price vert to the database.
    *
    * @param price_id the primary key for the new price vert
    * @return the new price vert
    */
    public static com.energyindex.liferay.sb.model.PriceVert create(
        long price_id) {
        return getPersistence().create(price_id);
    }

    /**
    * Removes the price vert with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param price_id the primary key of the price vert
    * @return the price vert that was removed
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert remove(
        long price_id)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(price_id);
    }

    public static com.energyindex.liferay.sb.model.PriceVert updateImpl(
        com.energyindex.liferay.sb.model.PriceVert priceVert)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(priceVert);
    }

    /**
    * Returns the price vert with the primary key or throws a {@link com.energyindex.liferay.sb.NoSuchPriceVertException} if it could not be found.
    *
    * @param price_id the primary key of the price vert
    * @return the price vert
    * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert findByPrimaryKey(
        long price_id)
        throws com.energyindex.liferay.sb.NoSuchPriceVertException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(price_id);
    }

    /**
    * Returns the price vert with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param price_id the primary key of the price vert
    * @return the price vert, or <code>null</code> if a price vert with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceVert fetchByPrimaryKey(
        long price_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(price_id);
    }

    /**
    * Returns all the price verts.
    *
    * @return the price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the price verts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @return the range of price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the price verts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of price verts
    * @param end the upper bound of the range of price verts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of price verts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceVert> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the price verts from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of price verts.
    *
    * @return the number of price verts
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PriceVertPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PriceVertPersistence) PortletBeanLocatorUtil.locate(com.energyindex.liferay.sb.service.ClpSerializer.getServletContextName(),
                    PriceVertPersistence.class.getName());

            ReferenceRegistry.registerReference(PriceVertUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(PriceVertPersistence persistence) {
    }
}
