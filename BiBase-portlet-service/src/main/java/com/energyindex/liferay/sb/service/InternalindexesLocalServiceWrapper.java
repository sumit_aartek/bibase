package com.energyindex.liferay.sb.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InternalindexesLocalService}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see InternalindexesLocalService
 * @generated
 */
public class InternalindexesLocalServiceWrapper
    implements InternalindexesLocalService,
        ServiceWrapper<InternalindexesLocalService> {
    private InternalindexesLocalService _internalindexesLocalService;

    public InternalindexesLocalServiceWrapper(
        InternalindexesLocalService internalindexesLocalService) {
        _internalindexesLocalService = internalindexesLocalService;
    }

    /**
    * Adds the internalindexes to the database. Also notifies the appropriate model listeners.
    *
    * @param internalindexes the internalindexes
    * @return the internalindexes that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.Internalindexes addInternalindexes(
        com.energyindex.liferay.sb.model.Internalindexes internalindexes)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.addInternalindexes(internalindexes);
    }

    /**
    * Creates a new internalindexes with the primary key. Does not add the internalindexes to the database.
    *
    * @param price_id the primary key for the new internalindexes
    * @return the new internalindexes
    */
    @Override
    public com.energyindex.liferay.sb.model.Internalindexes createInternalindexes(
        int price_id) {
        return _internalindexesLocalService.createInternalindexes(price_id);
    }

    /**
    * Deletes the internalindexes with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param price_id the primary key of the internalindexes
    * @return the internalindexes that was removed
    * @throws PortalException if a internalindexes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.Internalindexes deleteInternalindexes(
        int price_id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.deleteInternalindexes(price_id);
    }

    /**
    * Deletes the internalindexes from the database. Also notifies the appropriate model listeners.
    *
    * @param internalindexes the internalindexes
    * @return the internalindexes that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.Internalindexes deleteInternalindexes(
        com.energyindex.liferay.sb.model.Internalindexes internalindexes)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.deleteInternalindexes(internalindexes);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _internalindexesLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public com.energyindex.liferay.sb.model.Internalindexes fetchInternalindexes(
        int price_id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.fetchInternalindexes(price_id);
    }

    /**
    * Returns the internalindexes with the primary key.
    *
    * @param price_id the primary key of the internalindexes
    * @return the internalindexes
    * @throws PortalException if a internalindexes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.Internalindexes getInternalindexes(
        int price_id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.getInternalindexes(price_id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the internalindexeses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of internalindexeses
    * @param end the upper bound of the range of internalindexeses (not inclusive)
    * @return the range of internalindexeses
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<com.energyindex.liferay.sb.model.Internalindexes> getInternalindexeses(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.getInternalindexeses(start, end);
    }

    /**
    * Returns the number of internalindexeses.
    *
    * @return the number of internalindexeses
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getInternalindexesesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.getInternalindexesesCount();
    }

    /**
    * Updates the internalindexes in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param internalindexes the internalindexes
    * @return the internalindexes that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.energyindex.liferay.sb.model.Internalindexes updateInternalindexes(
        com.energyindex.liferay.sb.model.Internalindexes internalindexes)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _internalindexesLocalService.updateInternalindexes(internalindexes);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _internalindexesLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _internalindexesLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _internalindexesLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public InternalindexesLocalService getWrappedInternalindexesLocalService() {
        return _internalindexesLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedInternalindexesLocalService(
        InternalindexesLocalService internalindexesLocalService) {
        _internalindexesLocalService = internalindexesLocalService;
    }

    @Override
    public InternalindexesLocalService getWrappedService() {
        return _internalindexesLocalService;
    }

    @Override
    public void setWrappedService(
        InternalindexesLocalService internalindexesLocalService) {
        _internalindexesLocalService = internalindexesLocalService;
    }
}
