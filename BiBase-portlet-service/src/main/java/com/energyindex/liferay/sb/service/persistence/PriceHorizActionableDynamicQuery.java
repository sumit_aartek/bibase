package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.model.PriceHoriz;
import com.energyindex.liferay.sb.service.PriceHorizLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author InfinityFrame : Jason Gonin
 * @generated
 */
public abstract class PriceHorizActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PriceHorizActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PriceHorizLocalServiceUtil.getService());
        setClass(PriceHoriz.class);

        setClassLoader(com.energyindex.liferay.sb.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("primaryKey.priceH_id");
    }
}
