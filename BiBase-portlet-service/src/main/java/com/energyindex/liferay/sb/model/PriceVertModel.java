package com.energyindex.liferay.sb.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.GroupedModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the PriceVert service. Represents a row in the &quot;BiBase_PriceVert&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link com.energyindex.liferay.sb.model.impl.PriceVertImpl}.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVert
 * @see com.energyindex.liferay.sb.model.impl.PriceVertImpl
 * @see com.energyindex.liferay.sb.model.impl.PriceVertModelImpl
 * @generated
 */
public interface PriceVertModel extends BaseModel<PriceVert>, GroupedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. All methods that expect a price vert model instance should use the {@link PriceVert} interface instead.
     */

    /**
     * Returns the primary key of this price vert.
     *
     * @return the primary key of this price vert
     */
    public long getPrimaryKey();

    /**
     * Sets the primary key of this price vert.
     *
     * @param primaryKey the primary key of this price vert
     */
    public void setPrimaryKey(long primaryKey);

    /**
     * Returns the price_id of this price vert.
     *
     * @return the price_id of this price vert
     */
    public long getPrice_id();

    /**
     * Sets the price_id of this price vert.
     *
     * @param price_id the price_id of this price vert
     */
    public void setPrice_id(long price_id);

    /**
     * Returns the group ID of this price vert.
     *
     * @return the group ID of this price vert
     */
    @Override
    public long getGroupId();

    /**
     * Sets the group ID of this price vert.
     *
     * @param groupId the group ID of this price vert
     */
    @Override
    public void setGroupId(long groupId);

    /**
     * Returns the company ID of this price vert.
     *
     * @return the company ID of this price vert
     */
    @Override
    public long getCompanyId();

    /**
     * Sets the company ID of this price vert.
     *
     * @param companyId the company ID of this price vert
     */
    @Override
    public void setCompanyId(long companyId);

    /**
     * Returns the user ID of this price vert.
     *
     * @return the user ID of this price vert
     */
    @Override
    public long getUserId();

    /**
     * Sets the user ID of this price vert.
     *
     * @param userId the user ID of this price vert
     */
    @Override
    public void setUserId(long userId);

    /**
     * Returns the user uuid of this price vert.
     *
     * @return the user uuid of this price vert
     * @throws SystemException if a system exception occurred
     */
    @Override
    public String getUserUuid() throws SystemException;

    /**
     * Sets the user uuid of this price vert.
     *
     * @param userUuid the user uuid of this price vert
     */
    @Override
    public void setUserUuid(String userUuid);

    /**
     * Returns the user name of this price vert.
     *
     * @return the user name of this price vert
     */
    @AutoEscape
    @Override
    public String getUserName();

    /**
     * Sets the user name of this price vert.
     *
     * @param userName the user name of this price vert
     */
    @Override
    public void setUserName(String userName);

    /**
     * Returns the create date of this price vert.
     *
     * @return the create date of this price vert
     */
    @Override
    public Date getCreateDate();

    /**
     * Sets the create date of this price vert.
     *
     * @param createDate the create date of this price vert
     */
    @Override
    public void setCreateDate(Date createDate);

    /**
     * Returns the modified date of this price vert.
     *
     * @return the modified date of this price vert
     */
    @Override
    public Date getModifiedDate();

    /**
     * Sets the modified date of this price vert.
     *
     * @param modifiedDate the modified date of this price vert
     */
    @Override
    public void setModifiedDate(Date modifiedDate);

    /**
     * Returns the price_date of this price vert.
     *
     * @return the price_date of this price vert
     */
    public Date getPrice_date();

    /**
     * Sets the price_date of this price vert.
     *
     * @param price_date the price_date of this price vert
     */
    public void setPrice_date(Date price_date);

    /**
     * Returns the product_id of this price vert.
     *
     * @return the product_id of this price vert
     */
    public int getProduct_id();

    /**
     * Sets the product_id of this price vert.
     *
     * @param product_id the product_id of this price vert
     */
    public void setProduct_id(int product_id);

    /**
     * Returns the price_low of this price vert.
     *
     * @return the price_low of this price vert
     */
    public double getPrice_low();

    /**
     * Sets the price_low of this price vert.
     *
     * @param price_low the price_low of this price vert
     */
    public void setPrice_low(double price_low);

    /**
     * Returns the price_high of this price vert.
     *
     * @return the price_high of this price vert
     */
    public double getPrice_high();

    /**
     * Sets the price_high of this price vert.
     *
     * @param price_high the price_high of this price vert
     */
    public void setPrice_high(double price_high);

    /**
     * Returns the price_close of this price vert.
     *
     * @return the price_close of this price vert
     */
    public double getPrice_close();

    /**
     * Sets the price_close of this price vert.
     *
     * @param price_close the price_close of this price vert
     */
    public void setPrice_close(double price_close);

    /**
     * Returns the price_change of this price vert.
     *
     * @return the price_change of this price vert
     */
    public double getPrice_change();

    /**
     * Sets the price_change of this price vert.
     *
     * @param price_change the price_change of this price vert
     */
    public void setPrice_change(double price_change);

    /**
     * Returns the port_id of this price vert.
     *
     * @return the port_id of this price vert
     */
    public int getPort_id();

    /**
     * Sets the port_id of this price vert.
     *
     * @param port_id the port_id of this price vert
     */
    public void setPort_id(int port_id);

    /**
     * Returns the unit_id of this price vert.
     *
     * @return the unit_id of this price vert
     */
    public int getUnit_id();

    /**
     * Sets the unit_id of this price vert.
     *
     * @param unit_id the unit_id of this price vert
     */
    public void setUnit_id(int unit_id);

    /**
     * Returns the currency_id of this price vert.
     *
     * @return the currency_id of this price vert
     */
    public int getCurrency_id();

    /**
     * Sets the currency_id of this price vert.
     *
     * @param currency_id the currency_id of this price vert
     */
    public void setCurrency_id(int currency_id);

    /**
     * Returns the contracttype_id of this price vert.
     *
     * @return the contracttype_id of this price vert
     */
    public int getContracttype_id();

    /**
     * Sets the contracttype_id of this price vert.
     *
     * @param contracttype_id the contracttype_id of this price vert
     */
    public void setContracttype_id(int contracttype_id);

    /**
     * Returns the publish_id of this price vert.
     *
     * @return the publish_id of this price vert
     */
    public int getPublish_id();

    /**
     * Sets the publish_id of this price vert.
     *
     * @param publish_id the publish_id of this price vert
     */
    public void setPublish_id(int publish_id);

    @Override
    public boolean isNew();

    @Override
    public void setNew(boolean n);

    @Override
    public boolean isCachedModel();

    @Override
    public void setCachedModel(boolean cachedModel);

    @Override
    public boolean isEscapedModel();

    @Override
    public Serializable getPrimaryKeyObj();

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj);

    @Override
    public ExpandoBridge getExpandoBridge();

    @Override
    public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

    @Override
    public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

    @Override
    public void setExpandoBridgeAttributes(ServiceContext serviceContext);

    @Override
    public Object clone();

    @Override
    public int compareTo(com.energyindex.liferay.sb.model.PriceVert priceVert);

    @Override
    public int hashCode();

    @Override
    public CacheModel<com.energyindex.liferay.sb.model.PriceVert> toCacheModel();

    @Override
    public com.energyindex.liferay.sb.model.PriceVert toEscapedModel();

    @Override
    public com.energyindex.liferay.sb.model.PriceVert toUnescapedModel();

    @Override
    public String toString();

    @Override
    public String toXmlString();
}
