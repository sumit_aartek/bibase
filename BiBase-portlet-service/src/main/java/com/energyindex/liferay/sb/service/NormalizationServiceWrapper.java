package com.energyindex.liferay.sb.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link NormalizationService}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see NormalizationService
 * @generated
 */
public class NormalizationServiceWrapper implements NormalizationService,
    ServiceWrapper<NormalizationService> {
    private NormalizationService _normalizationService;

    public NormalizationServiceWrapper(
        NormalizationService normalizationService) {
        _normalizationService = normalizationService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _normalizationService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _normalizationService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _normalizationService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public NormalizationService getWrappedNormalizationService() {
        return _normalizationService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedNormalizationService(
        NormalizationService normalizationService) {
        _normalizationService = normalizationService;
    }

    @Override
    public NormalizationService getWrappedService() {
        return _normalizationService;
    }

    @Override
    public void setWrappedService(NormalizationService normalizationService) {
        _normalizationService = normalizationService;
    }
}
