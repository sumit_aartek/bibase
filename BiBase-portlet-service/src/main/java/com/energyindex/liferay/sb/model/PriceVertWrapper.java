package com.energyindex.liferay.sb.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PriceVert}.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVert
 * @generated
 */
public class PriceVertWrapper implements PriceVert, ModelWrapper<PriceVert> {
    private PriceVert _priceVert;

    public PriceVertWrapper(PriceVert priceVert) {
        _priceVert = priceVert;
    }

    @Override
    public Class<?> getModelClass() {
        return PriceVert.class;
    }

    @Override
    public String getModelClassName() {
        return PriceVert.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("price_id", getPrice_id());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("userName", getUserName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("price_date", getPrice_date());
        attributes.put("product_id", getProduct_id());
        attributes.put("price_low", getPrice_low());
        attributes.put("price_high", getPrice_high());
        attributes.put("price_close", getPrice_close());
        attributes.put("price_change", getPrice_change());
        attributes.put("port_id", getPort_id());
        attributes.put("unit_id", getUnit_id());
        attributes.put("currency_id", getCurrency_id());
        attributes.put("contracttype_id", getContracttype_id());
        attributes.put("publish_id", getPublish_id());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long price_id = (Long) attributes.get("price_id");

        if (price_id != null) {
            setPrice_id(price_id);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String userName = (String) attributes.get("userName");

        if (userName != null) {
            setUserName(userName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Date price_date = (Date) attributes.get("price_date");

        if (price_date != null) {
            setPrice_date(price_date);
        }

        Integer product_id = (Integer) attributes.get("product_id");

        if (product_id != null) {
            setProduct_id(product_id);
        }

        Double price_low = (Double) attributes.get("price_low");

        if (price_low != null) {
            setPrice_low(price_low);
        }

        Double price_high = (Double) attributes.get("price_high");

        if (price_high != null) {
            setPrice_high(price_high);
        }

        Double price_close = (Double) attributes.get("price_close");

        if (price_close != null) {
            setPrice_close(price_close);
        }

        Double price_change = (Double) attributes.get("price_change");

        if (price_change != null) {
            setPrice_change(price_change);
        }

        Integer port_id = (Integer) attributes.get("port_id");

        if (port_id != null) {
            setPort_id(port_id);
        }

        Integer unit_id = (Integer) attributes.get("unit_id");

        if (unit_id != null) {
            setUnit_id(unit_id);
        }

        Integer currency_id = (Integer) attributes.get("currency_id");

        if (currency_id != null) {
            setCurrency_id(currency_id);
        }

        Integer contracttype_id = (Integer) attributes.get("contracttype_id");

        if (contracttype_id != null) {
            setContracttype_id(contracttype_id);
        }

        Integer publish_id = (Integer) attributes.get("publish_id");

        if (publish_id != null) {
            setPublish_id(publish_id);
        }
    }

    /**
    * Returns the primary key of this price vert.
    *
    * @return the primary key of this price vert
    */
    @Override
    public long getPrimaryKey() {
        return _priceVert.getPrimaryKey();
    }

    /**
    * Sets the primary key of this price vert.
    *
    * @param primaryKey the primary key of this price vert
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _priceVert.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the price_id of this price vert.
    *
    * @return the price_id of this price vert
    */
    @Override
    public long getPrice_id() {
        return _priceVert.getPrice_id();
    }

    /**
    * Sets the price_id of this price vert.
    *
    * @param price_id the price_id of this price vert
    */
    @Override
    public void setPrice_id(long price_id) {
        _priceVert.setPrice_id(price_id);
    }

    /**
    * Returns the group ID of this price vert.
    *
    * @return the group ID of this price vert
    */
    @Override
    public long getGroupId() {
        return _priceVert.getGroupId();
    }

    /**
    * Sets the group ID of this price vert.
    *
    * @param groupId the group ID of this price vert
    */
    @Override
    public void setGroupId(long groupId) {
        _priceVert.setGroupId(groupId);
    }

    /**
    * Returns the company ID of this price vert.
    *
    * @return the company ID of this price vert
    */
    @Override
    public long getCompanyId() {
        return _priceVert.getCompanyId();
    }

    /**
    * Sets the company ID of this price vert.
    *
    * @param companyId the company ID of this price vert
    */
    @Override
    public void setCompanyId(long companyId) {
        _priceVert.setCompanyId(companyId);
    }

    /**
    * Returns the user ID of this price vert.
    *
    * @return the user ID of this price vert
    */
    @Override
    public long getUserId() {
        return _priceVert.getUserId();
    }

    /**
    * Sets the user ID of this price vert.
    *
    * @param userId the user ID of this price vert
    */
    @Override
    public void setUserId(long userId) {
        _priceVert.setUserId(userId);
    }

    /**
    * Returns the user uuid of this price vert.
    *
    * @return the user uuid of this price vert
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _priceVert.getUserUuid();
    }

    /**
    * Sets the user uuid of this price vert.
    *
    * @param userUuid the user uuid of this price vert
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _priceVert.setUserUuid(userUuid);
    }

    /**
    * Returns the user name of this price vert.
    *
    * @return the user name of this price vert
    */
    @Override
    public java.lang.String getUserName() {
        return _priceVert.getUserName();
    }

    /**
    * Sets the user name of this price vert.
    *
    * @param userName the user name of this price vert
    */
    @Override
    public void setUserName(java.lang.String userName) {
        _priceVert.setUserName(userName);
    }

    /**
    * Returns the create date of this price vert.
    *
    * @return the create date of this price vert
    */
    @Override
    public java.util.Date getCreateDate() {
        return _priceVert.getCreateDate();
    }

    /**
    * Sets the create date of this price vert.
    *
    * @param createDate the create date of this price vert
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _priceVert.setCreateDate(createDate);
    }

    /**
    * Returns the modified date of this price vert.
    *
    * @return the modified date of this price vert
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _priceVert.getModifiedDate();
    }

    /**
    * Sets the modified date of this price vert.
    *
    * @param modifiedDate the modified date of this price vert
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _priceVert.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the price_date of this price vert.
    *
    * @return the price_date of this price vert
    */
    @Override
    public java.util.Date getPrice_date() {
        return _priceVert.getPrice_date();
    }

    /**
    * Sets the price_date of this price vert.
    *
    * @param price_date the price_date of this price vert
    */
    @Override
    public void setPrice_date(java.util.Date price_date) {
        _priceVert.setPrice_date(price_date);
    }

    /**
    * Returns the product_id of this price vert.
    *
    * @return the product_id of this price vert
    */
    @Override
    public int getProduct_id() {
        return _priceVert.getProduct_id();
    }

    /**
    * Sets the product_id of this price vert.
    *
    * @param product_id the product_id of this price vert
    */
    @Override
    public void setProduct_id(int product_id) {
        _priceVert.setProduct_id(product_id);
    }

    /**
    * Returns the price_low of this price vert.
    *
    * @return the price_low of this price vert
    */
    @Override
    public double getPrice_low() {
        return _priceVert.getPrice_low();
    }

    /**
    * Sets the price_low of this price vert.
    *
    * @param price_low the price_low of this price vert
    */
    @Override
    public void setPrice_low(double price_low) {
        _priceVert.setPrice_low(price_low);
    }

    /**
    * Returns the price_high of this price vert.
    *
    * @return the price_high of this price vert
    */
    @Override
    public double getPrice_high() {
        return _priceVert.getPrice_high();
    }

    /**
    * Sets the price_high of this price vert.
    *
    * @param price_high the price_high of this price vert
    */
    @Override
    public void setPrice_high(double price_high) {
        _priceVert.setPrice_high(price_high);
    }

    /**
    * Returns the price_close of this price vert.
    *
    * @return the price_close of this price vert
    */
    @Override
    public double getPrice_close() {
        return _priceVert.getPrice_close();
    }

    /**
    * Sets the price_close of this price vert.
    *
    * @param price_close the price_close of this price vert
    */
    @Override
    public void setPrice_close(double price_close) {
        _priceVert.setPrice_close(price_close);
    }

    /**
    * Returns the price_change of this price vert.
    *
    * @return the price_change of this price vert
    */
    @Override
    public double getPrice_change() {
        return _priceVert.getPrice_change();
    }

    /**
    * Sets the price_change of this price vert.
    *
    * @param price_change the price_change of this price vert
    */
    @Override
    public void setPrice_change(double price_change) {
        _priceVert.setPrice_change(price_change);
    }

    /**
    * Returns the port_id of this price vert.
    *
    * @return the port_id of this price vert
    */
    @Override
    public int getPort_id() {
        return _priceVert.getPort_id();
    }

    /**
    * Sets the port_id of this price vert.
    *
    * @param port_id the port_id of this price vert
    */
    @Override
    public void setPort_id(int port_id) {
        _priceVert.setPort_id(port_id);
    }

    /**
    * Returns the unit_id of this price vert.
    *
    * @return the unit_id of this price vert
    */
    @Override
    public int getUnit_id() {
        return _priceVert.getUnit_id();
    }

    /**
    * Sets the unit_id of this price vert.
    *
    * @param unit_id the unit_id of this price vert
    */
    @Override
    public void setUnit_id(int unit_id) {
        _priceVert.setUnit_id(unit_id);
    }

    /**
    * Returns the currency_id of this price vert.
    *
    * @return the currency_id of this price vert
    */
    @Override
    public int getCurrency_id() {
        return _priceVert.getCurrency_id();
    }

    /**
    * Sets the currency_id of this price vert.
    *
    * @param currency_id the currency_id of this price vert
    */
    @Override
    public void setCurrency_id(int currency_id) {
        _priceVert.setCurrency_id(currency_id);
    }

    /**
    * Returns the contracttype_id of this price vert.
    *
    * @return the contracttype_id of this price vert
    */
    @Override
    public int getContracttype_id() {
        return _priceVert.getContracttype_id();
    }

    /**
    * Sets the contracttype_id of this price vert.
    *
    * @param contracttype_id the contracttype_id of this price vert
    */
    @Override
    public void setContracttype_id(int contracttype_id) {
        _priceVert.setContracttype_id(contracttype_id);
    }

    /**
    * Returns the publish_id of this price vert.
    *
    * @return the publish_id of this price vert
    */
    @Override
    public int getPublish_id() {
        return _priceVert.getPublish_id();
    }

    /**
    * Sets the publish_id of this price vert.
    *
    * @param publish_id the publish_id of this price vert
    */
    @Override
    public void setPublish_id(int publish_id) {
        _priceVert.setPublish_id(publish_id);
    }

    @Override
    public boolean isNew() {
        return _priceVert.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _priceVert.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _priceVert.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _priceVert.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _priceVert.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _priceVert.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _priceVert.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _priceVert.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _priceVert.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _priceVert.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _priceVert.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PriceVertWrapper((PriceVert) _priceVert.clone());
    }

    @Override
    public int compareTo(com.energyindex.liferay.sb.model.PriceVert priceVert) {
        return _priceVert.compareTo(priceVert);
    }

    @Override
    public int hashCode() {
        return _priceVert.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<com.energyindex.liferay.sb.model.PriceVert> toCacheModel() {
        return _priceVert.toCacheModel();
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert toEscapedModel() {
        return new PriceVertWrapper(_priceVert.toEscapedModel());
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert toUnescapedModel() {
        return new PriceVertWrapper(_priceVert.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _priceVert.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _priceVert.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _priceVert.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PriceVertWrapper)) {
            return false;
        }

        PriceVertWrapper priceVertWrapper = (PriceVertWrapper) obj;

        if (Validator.equals(_priceVert, priceVertWrapper._priceVert)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public PriceVert getWrappedPriceVert() {
        return _priceVert;
    }

    @Override
    public PriceVert getWrappedModel() {
        return _priceVert;
    }

    @Override
    public void resetOriginalValues() {
        _priceVert.resetOriginalValues();
    }
}
