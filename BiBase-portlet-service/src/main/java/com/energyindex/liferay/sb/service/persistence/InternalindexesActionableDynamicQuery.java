package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.model.Internalindexes;
import com.energyindex.liferay.sb.service.InternalindexesLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author InfinityFrame : Jason Gonin
 * @generated
 */
public abstract class InternalindexesActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public InternalindexesActionableDynamicQuery() throws SystemException {
        setBaseLocalService(InternalindexesLocalServiceUtil.getService());
        setClass(Internalindexes.class);

        setClassLoader(com.energyindex.liferay.sb.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("price_id");
    }
}
