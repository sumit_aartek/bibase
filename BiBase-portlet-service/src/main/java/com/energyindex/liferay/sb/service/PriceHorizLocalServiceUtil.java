package com.energyindex.liferay.sb.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for PriceHoriz. This utility wraps
 * {@link com.energyindex.liferay.sb.service.impl.PriceHorizLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceHorizLocalService
 * @see com.energyindex.liferay.sb.service.base.PriceHorizLocalServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.impl.PriceHorizLocalServiceImpl
 * @generated
 */
public class PriceHorizLocalServiceUtil {
    private static PriceHorizLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.energyindex.liferay.sb.service.impl.PriceHorizLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the price horiz to the database. Also notifies the appropriate model listeners.
    *
    * @param priceHoriz the price horiz
    * @return the price horiz that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz addPriceHoriz(
        com.energyindex.liferay.sb.model.PriceHoriz priceHoriz)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addPriceHoriz(priceHoriz);
    }

    /**
    * Creates a new price horiz with the primary key. Does not add the price horiz to the database.
    *
    * @param priceHorizPK the primary key for the new price horiz
    * @return the new price horiz
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz createPriceHoriz(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK) {
        return getService().createPriceHoriz(priceHorizPK);
    }

    /**
    * Deletes the price horiz with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param priceHorizPK the primary key of the price horiz
    * @return the price horiz that was removed
    * @throws PortalException if a price horiz with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz deletePriceHoriz(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePriceHoriz(priceHorizPK);
    }

    /**
    * Deletes the price horiz from the database. Also notifies the appropriate model listeners.
    *
    * @param priceHoriz the price horiz
    * @return the price horiz that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz deletePriceHoriz(
        com.energyindex.liferay.sb.model.PriceHoriz priceHoriz)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePriceHoriz(priceHoriz);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static com.energyindex.liferay.sb.model.PriceHoriz fetchPriceHoriz(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchPriceHoriz(priceHorizPK);
    }

    /**
    * Returns the price horiz with the primary key.
    *
    * @param priceHorizPK the primary key of the price horiz
    * @return the price horiz
    * @throws PortalException if a price horiz with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz getPriceHoriz(
        com.energyindex.liferay.sb.service.persistence.PriceHorizPK priceHorizPK)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceHoriz(priceHorizPK);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the price horizs.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of price horizs
    * @param end the upper bound of the range of price horizs (not inclusive)
    * @return the range of price horizs
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.energyindex.liferay.sb.model.PriceHoriz> getPriceHorizs(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceHorizs(start, end);
    }

    /**
    * Returns the number of price horizs.
    *
    * @return the number of price horizs
    * @throws SystemException if a system exception occurred
    */
    public static int getPriceHorizsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceHorizsCount();
    }

    /**
    * Updates the price horiz in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param priceHoriz the price horiz
    * @return the price horiz that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.energyindex.liferay.sb.model.PriceHoriz updatePriceHoriz(
        com.energyindex.liferay.sb.model.PriceHoriz priceHoriz)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePriceHoriz(priceHoriz);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static void clearService() {
        _service = null;
    }

    public static PriceHorizLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    PriceHorizLocalService.class.getName());

            if (invokableLocalService instanceof PriceHorizLocalService) {
                _service = (PriceHorizLocalService) invokableLocalService;
            } else {
                _service = new PriceHorizLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(PriceHorizLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(PriceHorizLocalService service) {
    }
}
