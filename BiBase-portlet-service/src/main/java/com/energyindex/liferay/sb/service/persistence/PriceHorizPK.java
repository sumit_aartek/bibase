package com.energyindex.liferay.sb.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author InfinityFrame : Jason Gonin
 * @generated
 */
public class PriceHorizPK implements Comparable<PriceHorizPK>, Serializable {
    public long priceH_id;
    public long price_id;

    public PriceHorizPK() {
    }

    public PriceHorizPK(long priceH_id, long price_id) {
        this.priceH_id = priceH_id;
        this.price_id = price_id;
    }

    public long getPriceH_id() {
        return priceH_id;
    }

    public void setPriceH_id(long priceH_id) {
        this.priceH_id = priceH_id;
    }

    public long getPrice_id() {
        return price_id;
    }

    public void setPrice_id(long price_id) {
        this.price_id = price_id;
    }

    @Override
    public int compareTo(PriceHorizPK pk) {
        if (pk == null) {
            return -1;
        }

        int value = 0;

        if (priceH_id < pk.priceH_id) {
            value = -1;
        } else if (priceH_id > pk.priceH_id) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        if (price_id < pk.price_id) {
            value = -1;
        } else if (price_id > pk.price_id) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PriceHorizPK)) {
            return false;
        }

        PriceHorizPK pk = (PriceHorizPK) obj;

        if ((priceH_id == pk.priceH_id) && (price_id == pk.price_id)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (String.valueOf(priceH_id) + String.valueOf(price_id)).hashCode();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(10);

        sb.append(StringPool.OPEN_CURLY_BRACE);

        sb.append("priceH_id");
        sb.append(StringPool.EQUAL);
        sb.append(priceH_id);

        sb.append(StringPool.COMMA);
        sb.append(StringPool.SPACE);
        sb.append("price_id");
        sb.append(StringPool.EQUAL);
        sb.append(price_id);

        sb.append(StringPool.CLOSE_CURLY_BRACE);

        return sb.toString();
    }
}
