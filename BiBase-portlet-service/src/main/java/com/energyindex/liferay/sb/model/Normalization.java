package com.energyindex.liferay.sb.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Normalization service. Represents a row in the &quot;BiBase_Normalization&quot; database table, with each column mapped to a property of this class.
 *
 * @author InfinityFrame : Jason Gonin
 * @see NormalizationModel
 * @see com.energyindex.liferay.sb.model.impl.NormalizationImpl
 * @see com.energyindex.liferay.sb.model.impl.NormalizationModelImpl
 * @generated
 */
public interface Normalization extends NormalizationModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.energyindex.liferay.sb.model.impl.NormalizationImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
