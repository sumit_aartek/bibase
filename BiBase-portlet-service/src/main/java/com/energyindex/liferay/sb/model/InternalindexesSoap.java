package com.energyindex.liferay.sb.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.energyindex.liferay.sb.service.http.InternalindexesServiceSoap}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.http.InternalindexesServiceSoap
 * @generated
 */
public class InternalindexesSoap implements Serializable {
    private int _price_id;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private String _userName;
    private Date _createDate;
    private Date _modifiedDate;
    private int _product_id;
    private Date _price_date;
    private int _month_id;
    private int _price_contractyear;
    private double _price_low;
    private double _price_current;
    private double _price_high;
    private double _price_close;
    private int _price_unitquote;
    private int _currency_id;
    private int _unit_id;
    private int _price_quantity;
    private int _delivery_id;
    private int _priceindex_id;
    private int _company_id;
    private int _contracttype_id;
    private int _city_id;
    private int _port_id;
    private int _airport_id;
    private int _state_id;
    private int _subregion_id;
    private int _country_id;
    private int _publish_id;
    private int _current_id;
    private int _price_highlight;
    private int _price_mainhighlight;
    private int _price_unqouted;
    private Date _price_lastedit;
    private Date _price_dateadded;

    public InternalindexesSoap() {
    }

    public static InternalindexesSoap toSoapModel(Internalindexes model) {
        InternalindexesSoap soapModel = new InternalindexesSoap();

        soapModel.setPrice_id(model.getPrice_id());
        soapModel.setGroupId(model.getGroupId());
        soapModel.setCompanyId(model.getCompanyId());
        soapModel.setUserId(model.getUserId());
        soapModel.setUserName(model.getUserName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setProduct_id(model.getProduct_id());
        soapModel.setPrice_date(model.getPrice_date());
        soapModel.setMonth_id(model.getMonth_id());
        soapModel.setPrice_contractyear(model.getPrice_contractyear());
        soapModel.setPrice_low(model.getPrice_low());
        soapModel.setPrice_current(model.getPrice_current());
        soapModel.setPrice_high(model.getPrice_high());
        soapModel.setPrice_close(model.getPrice_close());
        soapModel.setPrice_unitquote(model.getPrice_unitquote());
        soapModel.setCurrency_id(model.getCurrency_id());
        soapModel.setUnit_id(model.getUnit_id());
        soapModel.setPrice_quantity(model.getPrice_quantity());
        soapModel.setDelivery_id(model.getDelivery_id());
        soapModel.setPriceindex_id(model.getPriceindex_id());
        soapModel.setCompany_id(model.getCompany_id());
        soapModel.setContracttype_id(model.getContracttype_id());
        soapModel.setCity_id(model.getCity_id());
        soapModel.setPort_id(model.getPort_id());
        soapModel.setAirport_id(model.getAirport_id());
        soapModel.setState_id(model.getState_id());
        soapModel.setSubregion_id(model.getSubregion_id());
        soapModel.setCountry_id(model.getCountry_id());
        soapModel.setPublish_id(model.getPublish_id());
        soapModel.setCurrent_id(model.getCurrent_id());
        soapModel.setPrice_highlight(model.getPrice_highlight());
        soapModel.setPrice_mainhighlight(model.getPrice_mainhighlight());
        soapModel.setPrice_unqouted(model.getPrice_unqouted());
        soapModel.setPrice_lastedit(model.getPrice_lastedit());
        soapModel.setPrice_dateadded(model.getPrice_dateadded());

        return soapModel;
    }

    public static InternalindexesSoap[] toSoapModels(Internalindexes[] models) {
        InternalindexesSoap[] soapModels = new InternalindexesSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static InternalindexesSoap[][] toSoapModels(
        Internalindexes[][] models) {
        InternalindexesSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new InternalindexesSoap[models.length][models[0].length];
        } else {
            soapModels = new InternalindexesSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static InternalindexesSoap[] toSoapModels(
        List<Internalindexes> models) {
        List<InternalindexesSoap> soapModels = new ArrayList<InternalindexesSoap>(models.size());

        for (Internalindexes model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new InternalindexesSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _price_id;
    }

    public void setPrimaryKey(int pk) {
        setPrice_id(pk);
    }

    public int getPrice_id() {
        return _price_id;
    }

    public void setPrice_id(int price_id) {
        _price_id = price_id;
    }

    public long getGroupId() {
        return _groupId;
    }

    public void setGroupId(long groupId) {
        _groupId = groupId;
    }

    public long getCompanyId() {
        return _companyId;
    }

    public void setCompanyId(long companyId) {
        _companyId = companyId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public String getUserName() {
        return _userName;
    }

    public void setUserName(String userName) {
        _userName = userName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public int getProduct_id() {
        return _product_id;
    }

    public void setProduct_id(int product_id) {
        _product_id = product_id;
    }

    public Date getPrice_date() {
        return _price_date;
    }

    public void setPrice_date(Date price_date) {
        _price_date = price_date;
    }

    public int getMonth_id() {
        return _month_id;
    }

    public void setMonth_id(int month_id) {
        _month_id = month_id;
    }

    public int getPrice_contractyear() {
        return _price_contractyear;
    }

    public void setPrice_contractyear(int price_contractyear) {
        _price_contractyear = price_contractyear;
    }

    public double getPrice_low() {
        return _price_low;
    }

    public void setPrice_low(double price_low) {
        _price_low = price_low;
    }

    public double getPrice_current() {
        return _price_current;
    }

    public void setPrice_current(double price_current) {
        _price_current = price_current;
    }

    public double getPrice_high() {
        return _price_high;
    }

    public void setPrice_high(double price_high) {
        _price_high = price_high;
    }

    public double getPrice_close() {
        return _price_close;
    }

    public void setPrice_close(double price_close) {
        _price_close = price_close;
    }

    public int getPrice_unitquote() {
        return _price_unitquote;
    }

    public void setPrice_unitquote(int price_unitquote) {
        _price_unitquote = price_unitquote;
    }

    public int getCurrency_id() {
        return _currency_id;
    }

    public void setCurrency_id(int currency_id) {
        _currency_id = currency_id;
    }

    public int getUnit_id() {
        return _unit_id;
    }

    public void setUnit_id(int unit_id) {
        _unit_id = unit_id;
    }

    public int getPrice_quantity() {
        return _price_quantity;
    }

    public void setPrice_quantity(int price_quantity) {
        _price_quantity = price_quantity;
    }

    public int getDelivery_id() {
        return _delivery_id;
    }

    public void setDelivery_id(int delivery_id) {
        _delivery_id = delivery_id;
    }

    public int getPriceindex_id() {
        return _priceindex_id;
    }

    public void setPriceindex_id(int priceindex_id) {
        _priceindex_id = priceindex_id;
    }

    public int getCompany_id() {
        return _company_id;
    }

    public void setCompany_id(int company_id) {
        _company_id = company_id;
    }

    public int getContracttype_id() {
        return _contracttype_id;
    }

    public void setContracttype_id(int contracttype_id) {
        _contracttype_id = contracttype_id;
    }

    public int getCity_id() {
        return _city_id;
    }

    public void setCity_id(int city_id) {
        _city_id = city_id;
    }

    public int getPort_id() {
        return _port_id;
    }

    public void setPort_id(int port_id) {
        _port_id = port_id;
    }

    public int getAirport_id() {
        return _airport_id;
    }

    public void setAirport_id(int airport_id) {
        _airport_id = airport_id;
    }

    public int getState_id() {
        return _state_id;
    }

    public void setState_id(int state_id) {
        _state_id = state_id;
    }

    public int getSubregion_id() {
        return _subregion_id;
    }

    public void setSubregion_id(int subregion_id) {
        _subregion_id = subregion_id;
    }

    public int getCountry_id() {
        return _country_id;
    }

    public void setCountry_id(int country_id) {
        _country_id = country_id;
    }

    public int getPublish_id() {
        return _publish_id;
    }

    public void setPublish_id(int publish_id) {
        _publish_id = publish_id;
    }

    public int getCurrent_id() {
        return _current_id;
    }

    public void setCurrent_id(int current_id) {
        _current_id = current_id;
    }

    public int getPrice_highlight() {
        return _price_highlight;
    }

    public void setPrice_highlight(int price_highlight) {
        _price_highlight = price_highlight;
    }

    public int getPrice_mainhighlight() {
        return _price_mainhighlight;
    }

    public void setPrice_mainhighlight(int price_mainhighlight) {
        _price_mainhighlight = price_mainhighlight;
    }

    public int getPrice_unqouted() {
        return _price_unqouted;
    }

    public void setPrice_unqouted(int price_unqouted) {
        _price_unqouted = price_unqouted;
    }

    public Date getPrice_lastedit() {
        return _price_lastedit;
    }

    public void setPrice_lastedit(Date price_lastedit) {
        _price_lastedit = price_lastedit;
    }

    public Date getPrice_dateadded() {
        return _price_dateadded;
    }

    public void setPrice_dateadded(Date price_dateadded) {
        _price_dateadded = price_dateadded;
    }
}
