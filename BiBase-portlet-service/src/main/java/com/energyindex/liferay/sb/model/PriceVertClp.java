package com.energyindex.liferay.sb.model;

import com.energyindex.liferay.sb.service.ClpSerializer;
import com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PriceVertClp extends BaseModelImpl<PriceVert> implements PriceVert {
    private long _price_id;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private String _userUuid;
    private String _userName;
    private Date _createDate;
    private Date _modifiedDate;
    private Date _price_date;
    private int _product_id;
    private double _price_low;
    private double _price_high;
    private double _price_close;
    private double _price_change;
    private int _port_id;
    private int _unit_id;
    private int _currency_id;
    private int _contracttype_id;
    private int _publish_id;
    private BaseModel<?> _priceVertRemoteModel;
    private Class<?> _clpSerializerClass = com.energyindex.liferay.sb.service.ClpSerializer.class;

    public PriceVertClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return PriceVert.class;
    }

    @Override
    public String getModelClassName() {
        return PriceVert.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _price_id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setPrice_id(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _price_id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("price_id", getPrice_id());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("userName", getUserName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("price_date", getPrice_date());
        attributes.put("product_id", getProduct_id());
        attributes.put("price_low", getPrice_low());
        attributes.put("price_high", getPrice_high());
        attributes.put("price_close", getPrice_close());
        attributes.put("price_change", getPrice_change());
        attributes.put("port_id", getPort_id());
        attributes.put("unit_id", getUnit_id());
        attributes.put("currency_id", getCurrency_id());
        attributes.put("contracttype_id", getContracttype_id());
        attributes.put("publish_id", getPublish_id());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long price_id = (Long) attributes.get("price_id");

        if (price_id != null) {
            setPrice_id(price_id);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String userName = (String) attributes.get("userName");

        if (userName != null) {
            setUserName(userName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Date price_date = (Date) attributes.get("price_date");

        if (price_date != null) {
            setPrice_date(price_date);
        }

        Integer product_id = (Integer) attributes.get("product_id");

        if (product_id != null) {
            setProduct_id(product_id);
        }

        Double price_low = (Double) attributes.get("price_low");

        if (price_low != null) {
            setPrice_low(price_low);
        }

        Double price_high = (Double) attributes.get("price_high");

        if (price_high != null) {
            setPrice_high(price_high);
        }

        Double price_close = (Double) attributes.get("price_close");

        if (price_close != null) {
            setPrice_close(price_close);
        }

        Double price_change = (Double) attributes.get("price_change");

        if (price_change != null) {
            setPrice_change(price_change);
        }

        Integer port_id = (Integer) attributes.get("port_id");

        if (port_id != null) {
            setPort_id(port_id);
        }

        Integer unit_id = (Integer) attributes.get("unit_id");

        if (unit_id != null) {
            setUnit_id(unit_id);
        }

        Integer currency_id = (Integer) attributes.get("currency_id");

        if (currency_id != null) {
            setCurrency_id(currency_id);
        }

        Integer contracttype_id = (Integer) attributes.get("contracttype_id");

        if (contracttype_id != null) {
            setContracttype_id(contracttype_id);
        }

        Integer publish_id = (Integer) attributes.get("publish_id");

        if (publish_id != null) {
            setPublish_id(publish_id);
        }
    }

    @Override
    public long getPrice_id() {
        return _price_id;
    }

    @Override
    public void setPrice_id(long price_id) {
        _price_id = price_id;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_id", long.class);

                method.invoke(_priceVertRemoteModel, price_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getGroupId() {
        return _groupId;
    }

    @Override
    public void setGroupId(long groupId) {
        _groupId = groupId;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setGroupId", long.class);

                method.invoke(_priceVertRemoteModel, groupId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCompanyId() {
        return _companyId;
    }

    @Override
    public void setCompanyId(long companyId) {
        _companyId = companyId;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setCompanyId", long.class);

                method.invoke(_priceVertRemoteModel, companyId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_priceVertRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public String getUserName() {
        return _userName;
    }

    @Override
    public void setUserName(String userName) {
        _userName = userName;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setUserName", String.class);

                method.invoke(_priceVertRemoteModel, userName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_priceVertRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_priceVertRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getPrice_date() {
        return _price_date;
    }

    @Override
    public void setPrice_date(Date price_date) {
        _price_date = price_date;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_date", Date.class);

                method.invoke(_priceVertRemoteModel, price_date);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getProduct_id() {
        return _product_id;
    }

    @Override
    public void setProduct_id(int product_id) {
        _product_id = product_id;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setProduct_id", int.class);

                method.invoke(_priceVertRemoteModel, product_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice_low() {
        return _price_low;
    }

    @Override
    public void setPrice_low(double price_low) {
        _price_low = price_low;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_low", double.class);

                method.invoke(_priceVertRemoteModel, price_low);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice_high() {
        return _price_high;
    }

    @Override
    public void setPrice_high(double price_high) {
        _price_high = price_high;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_high", double.class);

                method.invoke(_priceVertRemoteModel, price_high);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice_close() {
        return _price_close;
    }

    @Override
    public void setPrice_close(double price_close) {
        _price_close = price_close;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_close", double.class);

                method.invoke(_priceVertRemoteModel, price_close);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice_change() {
        return _price_change;
    }

    @Override
    public void setPrice_change(double price_change) {
        _price_change = price_change;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_change", double.class);

                method.invoke(_priceVertRemoteModel, price_change);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPort_id() {
        return _port_id;
    }

    @Override
    public void setPort_id(int port_id) {
        _port_id = port_id;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setPort_id", int.class);

                method.invoke(_priceVertRemoteModel, port_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getUnit_id() {
        return _unit_id;
    }

    @Override
    public void setUnit_id(int unit_id) {
        _unit_id = unit_id;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setUnit_id", int.class);

                method.invoke(_priceVertRemoteModel, unit_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCurrency_id() {
        return _currency_id;
    }

    @Override
    public void setCurrency_id(int currency_id) {
        _currency_id = currency_id;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setCurrency_id", int.class);

                method.invoke(_priceVertRemoteModel, currency_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getContracttype_id() {
        return _contracttype_id;
    }

    @Override
    public void setContracttype_id(int contracttype_id) {
        _contracttype_id = contracttype_id;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setContracttype_id", int.class);

                method.invoke(_priceVertRemoteModel, contracttype_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPublish_id() {
        return _publish_id;
    }

    @Override
    public void setPublish_id(int publish_id) {
        _publish_id = publish_id;

        if (_priceVertRemoteModel != null) {
            try {
                Class<?> clazz = _priceVertRemoteModel.getClass();

                Method method = clazz.getMethod("setPublish_id", int.class);

                method.invoke(_priceVertRemoteModel, publish_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getPriceVertRemoteModel() {
        return _priceVertRemoteModel;
    }

    public void setPriceVertRemoteModel(BaseModel<?> priceVertRemoteModel) {
        _priceVertRemoteModel = priceVertRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _priceVertRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_priceVertRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            PriceVertLocalServiceUtil.addPriceVert(this);
        } else {
            PriceVertLocalServiceUtil.updatePriceVert(this);
        }
    }

    @Override
    public PriceVert toEscapedModel() {
        return (PriceVert) ProxyUtil.newProxyInstance(PriceVert.class.getClassLoader(),
            new Class[] { PriceVert.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        PriceVertClp clone = new PriceVertClp();

        clone.setPrice_id(getPrice_id());
        clone.setGroupId(getGroupId());
        clone.setCompanyId(getCompanyId());
        clone.setUserId(getUserId());
        clone.setUserName(getUserName());
        clone.setCreateDate(getCreateDate());
        clone.setModifiedDate(getModifiedDate());
        clone.setPrice_date(getPrice_date());
        clone.setProduct_id(getProduct_id());
        clone.setPrice_low(getPrice_low());
        clone.setPrice_high(getPrice_high());
        clone.setPrice_close(getPrice_close());
        clone.setPrice_change(getPrice_change());
        clone.setPort_id(getPort_id());
        clone.setUnit_id(getUnit_id());
        clone.setCurrency_id(getCurrency_id());
        clone.setContracttype_id(getContracttype_id());
        clone.setPublish_id(getPublish_id());

        return clone;
    }

    @Override
    public int compareTo(PriceVert priceVert) {
        int value = 0;

        if (getProduct_id() < priceVert.getProduct_id()) {
            value = -1;
        } else if (getProduct_id() > priceVert.getProduct_id()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        value = DateUtil.compareTo(getPrice_date(), priceVert.getPrice_date());

        value = value * -1;

        if (value != 0) {
            return value;
        }

        if (getPort_id() < priceVert.getPort_id()) {
            value = -1;
        } else if (getPort_id() > priceVert.getPort_id()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PriceVertClp)) {
            return false;
        }

        PriceVertClp priceVert = (PriceVertClp) obj;

        long primaryKey = priceVert.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(37);

        sb.append("{price_id=");
        sb.append(getPrice_id());
        sb.append(", groupId=");
        sb.append(getGroupId());
        sb.append(", companyId=");
        sb.append(getCompanyId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", userName=");
        sb.append(getUserName());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", price_date=");
        sb.append(getPrice_date());
        sb.append(", product_id=");
        sb.append(getProduct_id());
        sb.append(", price_low=");
        sb.append(getPrice_low());
        sb.append(", price_high=");
        sb.append(getPrice_high());
        sb.append(", price_close=");
        sb.append(getPrice_close());
        sb.append(", price_change=");
        sb.append(getPrice_change());
        sb.append(", port_id=");
        sb.append(getPort_id());
        sb.append(", unit_id=");
        sb.append(getUnit_id());
        sb.append(", currency_id=");
        sb.append(getCurrency_id());
        sb.append(", contracttype_id=");
        sb.append(getContracttype_id());
        sb.append(", publish_id=");
        sb.append(getPublish_id());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(58);

        sb.append("<model><model-name>");
        sb.append("com.energyindex.liferay.sb.model.PriceVert");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>price_id</column-name><column-value><![CDATA[");
        sb.append(getPrice_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>groupId</column-name><column-value><![CDATA[");
        sb.append(getGroupId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>companyId</column-name><column-value><![CDATA[");
        sb.append(getCompanyId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userName</column-name><column-value><![CDATA[");
        sb.append(getUserName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_date</column-name><column-value><![CDATA[");
        sb.append(getPrice_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>product_id</column-name><column-value><![CDATA[");
        sb.append(getProduct_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_low</column-name><column-value><![CDATA[");
        sb.append(getPrice_low());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_high</column-name><column-value><![CDATA[");
        sb.append(getPrice_high());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_close</column-name><column-value><![CDATA[");
        sb.append(getPrice_close());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_change</column-name><column-value><![CDATA[");
        sb.append(getPrice_change());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>port_id</column-name><column-value><![CDATA[");
        sb.append(getPort_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>unit_id</column-name><column-value><![CDATA[");
        sb.append(getUnit_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>currency_id</column-name><column-value><![CDATA[");
        sb.append(getCurrency_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>contracttype_id</column-name><column-value><![CDATA[");
        sb.append(getContracttype_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>publish_id</column-name><column-value><![CDATA[");
        sb.append(getPublish_id());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
