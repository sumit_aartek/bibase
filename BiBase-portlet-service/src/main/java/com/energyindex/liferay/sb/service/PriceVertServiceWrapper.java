package com.energyindex.liferay.sb.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PriceVertService}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVertService
 * @generated
 */
public class PriceVertServiceWrapper implements PriceVertService,
    ServiceWrapper<PriceVertService> {
    private PriceVertService _priceVertService;

    public PriceVertServiceWrapper(PriceVertService priceVertService) {
        _priceVertService = priceVertService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _priceVertService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _priceVertService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _priceVertService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert addPriceVert(
        long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertService.addPriceVert(userId, groupId, price_close,
            port_id, product_id, month, day, year, portId, serviceContext);
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert deletePriceVert(
        long price_id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertService.deletePriceVert(price_id);
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert getPriceVert(long eventId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertService.getPriceVert(eventId);
    }

    @Override
    public com.energyindex.liferay.sb.model.PriceVert updatePriceVert(
        int priceId, long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _priceVertService.updatePriceVert(priceId, userId, groupId,
            price_close, port_id, product_id, month, day, year, portId,
            serviceContext);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PriceVertService getWrappedPriceVertService() {
        return _priceVertService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPriceVertService(PriceVertService priceVertService) {
        _priceVertService = priceVertService;
    }

    @Override
    public PriceVertService getWrappedService() {
        return _priceVertService;
    }

    @Override
    public void setWrappedService(PriceVertService priceVertService) {
        _priceVertService = priceVertService;
    }
}
