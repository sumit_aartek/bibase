package com.energyindex.liferay.sb.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the PriceVert service. Represents a row in the &quot;BiBase_PriceVert&quot; database table, with each column mapped to a property of this class.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVertModel
 * @see com.energyindex.liferay.sb.model.impl.PriceVertImpl
 * @see com.energyindex.liferay.sb.model.impl.PriceVertModelImpl
 * @generated
 */
public interface PriceVert extends PriceVertModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.energyindex.liferay.sb.model.impl.PriceVertImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
