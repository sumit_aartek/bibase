package com.energyindex.liferay.sb.model;

import com.energyindex.liferay.sb.service.persistence.PriceHorizPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.energyindex.liferay.sb.service.http.PriceHorizServiceSoap}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.http.PriceHorizServiceSoap
 * @generated
 */
public class PriceHorizSoap implements Serializable {
    private long _priceH_id;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private String _userName;
    private Date _createDate;
    private Date _modifiedDate;
    private Date _price_date;
    private int _product_id;
    private double _price_low;
    private double _price_high;
    private double _price_close;
    private double _price_change;
    private int _port_id;
    private int _unit_id;
    private int _currency_id;
    private int _contracttype_id;
    private int _publish_id;
    private long _price_id;

    public PriceHorizSoap() {
    }

    public static PriceHorizSoap toSoapModel(PriceHoriz model) {
        PriceHorizSoap soapModel = new PriceHorizSoap();

        soapModel.setPriceH_id(model.getPriceH_id());
        soapModel.setGroupId(model.getGroupId());
        soapModel.setCompanyId(model.getCompanyId());
        soapModel.setUserId(model.getUserId());
        soapModel.setUserName(model.getUserName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setPrice_date(model.getPrice_date());
        soapModel.setProduct_id(model.getProduct_id());
        soapModel.setPrice_low(model.getPrice_low());
        soapModel.setPrice_high(model.getPrice_high());
        soapModel.setPrice_close(model.getPrice_close());
        soapModel.setPrice_change(model.getPrice_change());
        soapModel.setPort_id(model.getPort_id());
        soapModel.setUnit_id(model.getUnit_id());
        soapModel.setCurrency_id(model.getCurrency_id());
        soapModel.setContracttype_id(model.getContracttype_id());
        soapModel.setPublish_id(model.getPublish_id());
        soapModel.setPrice_id(model.getPrice_id());

        return soapModel;
    }

    public static PriceHorizSoap[] toSoapModels(PriceHoriz[] models) {
        PriceHorizSoap[] soapModels = new PriceHorizSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PriceHorizSoap[][] toSoapModels(PriceHoriz[][] models) {
        PriceHorizSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PriceHorizSoap[models.length][models[0].length];
        } else {
            soapModels = new PriceHorizSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PriceHorizSoap[] toSoapModels(List<PriceHoriz> models) {
        List<PriceHorizSoap> soapModels = new ArrayList<PriceHorizSoap>(models.size());

        for (PriceHoriz model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PriceHorizSoap[soapModels.size()]);
    }

    public PriceHorizPK getPrimaryKey() {
        return new PriceHorizPK(_priceH_id, _price_id);
    }

    public void setPrimaryKey(PriceHorizPK pk) {
        setPriceH_id(pk.priceH_id);
        setPrice_id(pk.price_id);
    }

    public long getPriceH_id() {
        return _priceH_id;
    }

    public void setPriceH_id(long priceH_id) {
        _priceH_id = priceH_id;
    }

    public long getGroupId() {
        return _groupId;
    }

    public void setGroupId(long groupId) {
        _groupId = groupId;
    }

    public long getCompanyId() {
        return _companyId;
    }

    public void setCompanyId(long companyId) {
        _companyId = companyId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public String getUserName() {
        return _userName;
    }

    public void setUserName(String userName) {
        _userName = userName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public Date getPrice_date() {
        return _price_date;
    }

    public void setPrice_date(Date price_date) {
        _price_date = price_date;
    }

    public int getProduct_id() {
        return _product_id;
    }

    public void setProduct_id(int product_id) {
        _product_id = product_id;
    }

    public double getPrice_low() {
        return _price_low;
    }

    public void setPrice_low(double price_low) {
        _price_low = price_low;
    }

    public double getPrice_high() {
        return _price_high;
    }

    public void setPrice_high(double price_high) {
        _price_high = price_high;
    }

    public double getPrice_close() {
        return _price_close;
    }

    public void setPrice_close(double price_close) {
        _price_close = price_close;
    }

    public double getPrice_change() {
        return _price_change;
    }

    public void setPrice_change(double price_change) {
        _price_change = price_change;
    }

    public int getPort_id() {
        return _port_id;
    }

    public void setPort_id(int port_id) {
        _port_id = port_id;
    }

    public int getUnit_id() {
        return _unit_id;
    }

    public void setUnit_id(int unit_id) {
        _unit_id = unit_id;
    }

    public int getCurrency_id() {
        return _currency_id;
    }

    public void setCurrency_id(int currency_id) {
        _currency_id = currency_id;
    }

    public int getContracttype_id() {
        return _contracttype_id;
    }

    public void setContracttype_id(int contracttype_id) {
        _contracttype_id = contracttype_id;
    }

    public int getPublish_id() {
        return _publish_id;
    }

    public void setPublish_id(int publish_id) {
        _publish_id = publish_id;
    }

    public long getPrice_id() {
        return _price_id;
    }

    public void setPrice_id(long price_id) {
        _price_id = price_id;
    }
}
