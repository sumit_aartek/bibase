package com.energyindex.liferay.sb.util;
import com.liferay.portal.security.permission.ActionKeys;

/**
 * @author jason gonin
 */

public class PriceVertActionKeys extends ActionKeys{

	public static final String ADD_PRICEVERT = "ADD_PRICEVERT";
	
	public static final String DELETE_EVENT = "DELETE_EVENT";

	public static final String DELETE_LOCATION = "DELETE_LOCATION";

	public static final String UPDATE_EVENT = "UPDATE_EVENT";

	public static final String UPDATE_LOCATION = "UPDATE_LOCATION";

}
