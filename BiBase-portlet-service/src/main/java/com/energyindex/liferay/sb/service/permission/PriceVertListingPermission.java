package com.energyindex.liferay.sb.service.permission;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.security.permission.PermissionChecker;

public class PriceVertListingPermission {
	public static void check(
			PermissionChecker permissionChecker, long groupId, String actionId)
		throws PortalException {

		if (!contains(permissionChecker, groupId, actionId)) {
			throw new PrincipalException();
		}
	}

	public static boolean contains(
		PermissionChecker permissionChecker, long groupId, String actionId) {

		return permissionChecker.hasPermission(
			groupId, _CLASS_NAME, groupId, actionId);
	}

	private static final String _CLASS_NAME = "com.energyindex.liferay.sb.model"; /*notice slight difference with the eventlisting... that have a package impl under*/
}
