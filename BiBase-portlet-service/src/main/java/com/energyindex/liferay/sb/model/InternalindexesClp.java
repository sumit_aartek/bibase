package com.energyindex.liferay.sb.model;

import com.energyindex.liferay.sb.service.ClpSerializer;
import com.energyindex.liferay.sb.service.InternalindexesLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class InternalindexesClp extends BaseModelImpl<Internalindexes>
    implements Internalindexes {
    private int _price_id;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private String _userUuid;
    private String _userName;
    private Date _createDate;
    private Date _modifiedDate;
    private int _product_id;
    private Date _price_date;
    private int _month_id;
    private int _price_contractyear;
    private double _price_low;
    private double _price_current;
    private double _price_high;
    private double _price_close;
    private int _price_unitquote;
    private int _currency_id;
    private int _unit_id;
    private int _price_quantity;
    private int _delivery_id;
    private int _priceindex_id;
    private int _company_id;
    private int _contracttype_id;
    private int _city_id;
    private int _port_id;
    private int _airport_id;
    private int _state_id;
    private int _subregion_id;
    private int _country_id;
    private int _publish_id;
    private int _current_id;
    private int _price_highlight;
    private int _price_mainhighlight;
    private int _price_unqouted;
    private Date _price_lastedit;
    private Date _price_dateadded;
    private BaseModel<?> _internalindexesRemoteModel;
    private Class<?> _clpSerializerClass = com.energyindex.liferay.sb.service.ClpSerializer.class;

    public InternalindexesClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Internalindexes.class;
    }

    @Override
    public String getModelClassName() {
        return Internalindexes.class.getName();
    }

    @Override
    public int getPrimaryKey() {
        return _price_id;
    }

    @Override
    public void setPrimaryKey(int primaryKey) {
        setPrice_id(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _price_id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Integer) primaryKeyObj).intValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("price_id", getPrice_id());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("userName", getUserName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("product_id", getProduct_id());
        attributes.put("price_date", getPrice_date());
        attributes.put("month_id", getMonth_id());
        attributes.put("price_contractyear", getPrice_contractyear());
        attributes.put("price_low", getPrice_low());
        attributes.put("price_current", getPrice_current());
        attributes.put("price_high", getPrice_high());
        attributes.put("price_close", getPrice_close());
        attributes.put("price_unitquote", getPrice_unitquote());
        attributes.put("currency_id", getCurrency_id());
        attributes.put("unit_id", getUnit_id());
        attributes.put("price_quantity", getPrice_quantity());
        attributes.put("delivery_id", getDelivery_id());
        attributes.put("priceindex_id", getPriceindex_id());
        attributes.put("company_id", getCompany_id());
        attributes.put("contracttype_id", getContracttype_id());
        attributes.put("city_id", getCity_id());
        attributes.put("port_id", getPort_id());
        attributes.put("airport_id", getAirport_id());
        attributes.put("state_id", getState_id());
        attributes.put("subregion_id", getSubregion_id());
        attributes.put("country_id", getCountry_id());
        attributes.put("publish_id", getPublish_id());
        attributes.put("current_id", getCurrent_id());
        attributes.put("price_highlight", getPrice_highlight());
        attributes.put("price_mainhighlight", getPrice_mainhighlight());
        attributes.put("price_unqouted", getPrice_unqouted());
        attributes.put("price_lastedit", getPrice_lastedit());
        attributes.put("price_dateadded", getPrice_dateadded());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer price_id = (Integer) attributes.get("price_id");

        if (price_id != null) {
            setPrice_id(price_id);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String userName = (String) attributes.get("userName");

        if (userName != null) {
            setUserName(userName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        Integer product_id = (Integer) attributes.get("product_id");

        if (product_id != null) {
            setProduct_id(product_id);
        }

        Date price_date = (Date) attributes.get("price_date");

        if (price_date != null) {
            setPrice_date(price_date);
        }

        Integer month_id = (Integer) attributes.get("month_id");

        if (month_id != null) {
            setMonth_id(month_id);
        }

        Integer price_contractyear = (Integer) attributes.get(
                "price_contractyear");

        if (price_contractyear != null) {
            setPrice_contractyear(price_contractyear);
        }

        Double price_low = (Double) attributes.get("price_low");

        if (price_low != null) {
            setPrice_low(price_low);
        }

        Double price_current = (Double) attributes.get("price_current");

        if (price_current != null) {
            setPrice_current(price_current);
        }

        Double price_high = (Double) attributes.get("price_high");

        if (price_high != null) {
            setPrice_high(price_high);
        }

        Double price_close = (Double) attributes.get("price_close");

        if (price_close != null) {
            setPrice_close(price_close);
        }

        Integer price_unitquote = (Integer) attributes.get("price_unitquote");

        if (price_unitquote != null) {
            setPrice_unitquote(price_unitquote);
        }

        Integer currency_id = (Integer) attributes.get("currency_id");

        if (currency_id != null) {
            setCurrency_id(currency_id);
        }

        Integer unit_id = (Integer) attributes.get("unit_id");

        if (unit_id != null) {
            setUnit_id(unit_id);
        }

        Integer price_quantity = (Integer) attributes.get("price_quantity");

        if (price_quantity != null) {
            setPrice_quantity(price_quantity);
        }

        Integer delivery_id = (Integer) attributes.get("delivery_id");

        if (delivery_id != null) {
            setDelivery_id(delivery_id);
        }

        Integer priceindex_id = (Integer) attributes.get("priceindex_id");

        if (priceindex_id != null) {
            setPriceindex_id(priceindex_id);
        }

        Integer company_id = (Integer) attributes.get("company_id");

        if (company_id != null) {
            setCompany_id(company_id);
        }

        Integer contracttype_id = (Integer) attributes.get("contracttype_id");

        if (contracttype_id != null) {
            setContracttype_id(contracttype_id);
        }

        Integer city_id = (Integer) attributes.get("city_id");

        if (city_id != null) {
            setCity_id(city_id);
        }

        Integer port_id = (Integer) attributes.get("port_id");

        if (port_id != null) {
            setPort_id(port_id);
        }

        Integer airport_id = (Integer) attributes.get("airport_id");

        if (airport_id != null) {
            setAirport_id(airport_id);
        }

        Integer state_id = (Integer) attributes.get("state_id");

        if (state_id != null) {
            setState_id(state_id);
        }

        Integer subregion_id = (Integer) attributes.get("subregion_id");

        if (subregion_id != null) {
            setSubregion_id(subregion_id);
        }

        Integer country_id = (Integer) attributes.get("country_id");

        if (country_id != null) {
            setCountry_id(country_id);
        }

        Integer publish_id = (Integer) attributes.get("publish_id");

        if (publish_id != null) {
            setPublish_id(publish_id);
        }

        Integer current_id = (Integer) attributes.get("current_id");

        if (current_id != null) {
            setCurrent_id(current_id);
        }

        Integer price_highlight = (Integer) attributes.get("price_highlight");

        if (price_highlight != null) {
            setPrice_highlight(price_highlight);
        }

        Integer price_mainhighlight = (Integer) attributes.get(
                "price_mainhighlight");

        if (price_mainhighlight != null) {
            setPrice_mainhighlight(price_mainhighlight);
        }

        Integer price_unqouted = (Integer) attributes.get("price_unqouted");

        if (price_unqouted != null) {
            setPrice_unqouted(price_unqouted);
        }

        Date price_lastedit = (Date) attributes.get("price_lastedit");

        if (price_lastedit != null) {
            setPrice_lastedit(price_lastedit);
        }

        Date price_dateadded = (Date) attributes.get("price_dateadded");

        if (price_dateadded != null) {
            setPrice_dateadded(price_dateadded);
        }
    }

    @Override
    public int getPrice_id() {
        return _price_id;
    }

    @Override
    public void setPrice_id(int price_id) {
        _price_id = price_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_id", int.class);

                method.invoke(_internalindexesRemoteModel, price_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getGroupId() {
        return _groupId;
    }

    @Override
    public void setGroupId(long groupId) {
        _groupId = groupId;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setGroupId", long.class);

                method.invoke(_internalindexesRemoteModel, groupId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCompanyId() {
        return _companyId;
    }

    @Override
    public void setCompanyId(long companyId) {
        _companyId = companyId;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setCompanyId", long.class);

                method.invoke(_internalindexesRemoteModel, companyId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_internalindexesRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public String getUserName() {
        return _userName;
    }

    @Override
    public void setUserName(String userName) {
        _userName = userName;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setUserName", String.class);

                method.invoke(_internalindexesRemoteModel, userName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_internalindexesRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_internalindexesRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getProduct_id() {
        return _product_id;
    }

    @Override
    public void setProduct_id(int product_id) {
        _product_id = product_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setProduct_id", int.class);

                method.invoke(_internalindexesRemoteModel, product_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getPrice_date() {
        return _price_date;
    }

    @Override
    public void setPrice_date(Date price_date) {
        _price_date = price_date;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_date", Date.class);

                method.invoke(_internalindexesRemoteModel, price_date);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getMonth_id() {
        return _month_id;
    }

    @Override
    public void setMonth_id(int month_id) {
        _month_id = month_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setMonth_id", int.class);

                method.invoke(_internalindexesRemoteModel, month_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPrice_contractyear() {
        return _price_contractyear;
    }

    @Override
    public void setPrice_contractyear(int price_contractyear) {
        _price_contractyear = price_contractyear;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_contractyear",
                        int.class);

                method.invoke(_internalindexesRemoteModel, price_contractyear);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice_low() {
        return _price_low;
    }

    @Override
    public void setPrice_low(double price_low) {
        _price_low = price_low;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_low", double.class);

                method.invoke(_internalindexesRemoteModel, price_low);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice_current() {
        return _price_current;
    }

    @Override
    public void setPrice_current(double price_current) {
        _price_current = price_current;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_current", double.class);

                method.invoke(_internalindexesRemoteModel, price_current);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice_high() {
        return _price_high;
    }

    @Override
    public void setPrice_high(double price_high) {
        _price_high = price_high;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_high", double.class);

                method.invoke(_internalindexesRemoteModel, price_high);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice_close() {
        return _price_close;
    }

    @Override
    public void setPrice_close(double price_close) {
        _price_close = price_close;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_close", double.class);

                method.invoke(_internalindexesRemoteModel, price_close);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPrice_unitquote() {
        return _price_unitquote;
    }

    @Override
    public void setPrice_unitquote(int price_unitquote) {
        _price_unitquote = price_unitquote;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_unitquote", int.class);

                method.invoke(_internalindexesRemoteModel, price_unitquote);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCurrency_id() {
        return _currency_id;
    }

    @Override
    public void setCurrency_id(int currency_id) {
        _currency_id = currency_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setCurrency_id", int.class);

                method.invoke(_internalindexesRemoteModel, currency_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getUnit_id() {
        return _unit_id;
    }

    @Override
    public void setUnit_id(int unit_id) {
        _unit_id = unit_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setUnit_id", int.class);

                method.invoke(_internalindexesRemoteModel, unit_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPrice_quantity() {
        return _price_quantity;
    }

    @Override
    public void setPrice_quantity(int price_quantity) {
        _price_quantity = price_quantity;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_quantity", int.class);

                method.invoke(_internalindexesRemoteModel, price_quantity);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getDelivery_id() {
        return _delivery_id;
    }

    @Override
    public void setDelivery_id(int delivery_id) {
        _delivery_id = delivery_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setDelivery_id", int.class);

                method.invoke(_internalindexesRemoteModel, delivery_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPriceindex_id() {
        return _priceindex_id;
    }

    @Override
    public void setPriceindex_id(int priceindex_id) {
        _priceindex_id = priceindex_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPriceindex_id", int.class);

                method.invoke(_internalindexesRemoteModel, priceindex_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCompany_id() {
        return _company_id;
    }

    @Override
    public void setCompany_id(int company_id) {
        _company_id = company_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setCompany_id", int.class);

                method.invoke(_internalindexesRemoteModel, company_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getContracttype_id() {
        return _contracttype_id;
    }

    @Override
    public void setContracttype_id(int contracttype_id) {
        _contracttype_id = contracttype_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setContracttype_id", int.class);

                method.invoke(_internalindexesRemoteModel, contracttype_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCity_id() {
        return _city_id;
    }

    @Override
    public void setCity_id(int city_id) {
        _city_id = city_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setCity_id", int.class);

                method.invoke(_internalindexesRemoteModel, city_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPort_id() {
        return _port_id;
    }

    @Override
    public void setPort_id(int port_id) {
        _port_id = port_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPort_id", int.class);

                method.invoke(_internalindexesRemoteModel, port_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getAirport_id() {
        return _airport_id;
    }

    @Override
    public void setAirport_id(int airport_id) {
        _airport_id = airport_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setAirport_id", int.class);

                method.invoke(_internalindexesRemoteModel, airport_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getState_id() {
        return _state_id;
    }

    @Override
    public void setState_id(int state_id) {
        _state_id = state_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setState_id", int.class);

                method.invoke(_internalindexesRemoteModel, state_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getSubregion_id() {
        return _subregion_id;
    }

    @Override
    public void setSubregion_id(int subregion_id) {
        _subregion_id = subregion_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setSubregion_id", int.class);

                method.invoke(_internalindexesRemoteModel, subregion_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCountry_id() {
        return _country_id;
    }

    @Override
    public void setCountry_id(int country_id) {
        _country_id = country_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setCountry_id", int.class);

                method.invoke(_internalindexesRemoteModel, country_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPublish_id() {
        return _publish_id;
    }

    @Override
    public void setPublish_id(int publish_id) {
        _publish_id = publish_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPublish_id", int.class);

                method.invoke(_internalindexesRemoteModel, publish_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCurrent_id() {
        return _current_id;
    }

    @Override
    public void setCurrent_id(int current_id) {
        _current_id = current_id;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setCurrent_id", int.class);

                method.invoke(_internalindexesRemoteModel, current_id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPrice_highlight() {
        return _price_highlight;
    }

    @Override
    public void setPrice_highlight(int price_highlight) {
        _price_highlight = price_highlight;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_highlight", int.class);

                method.invoke(_internalindexesRemoteModel, price_highlight);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPrice_mainhighlight() {
        return _price_mainhighlight;
    }

    @Override
    public void setPrice_mainhighlight(int price_mainhighlight) {
        _price_mainhighlight = price_mainhighlight;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_mainhighlight",
                        int.class);

                method.invoke(_internalindexesRemoteModel, price_mainhighlight);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPrice_unqouted() {
        return _price_unqouted;
    }

    @Override
    public void setPrice_unqouted(int price_unqouted) {
        _price_unqouted = price_unqouted;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_unqouted", int.class);

                method.invoke(_internalindexesRemoteModel, price_unqouted);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getPrice_lastedit() {
        return _price_lastedit;
    }

    @Override
    public void setPrice_lastedit(Date price_lastedit) {
        _price_lastedit = price_lastedit;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_lastedit", Date.class);

                method.invoke(_internalindexesRemoteModel, price_lastedit);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getPrice_dateadded() {
        return _price_dateadded;
    }

    @Override
    public void setPrice_dateadded(Date price_dateadded) {
        _price_dateadded = price_dateadded;

        if (_internalindexesRemoteModel != null) {
            try {
                Class<?> clazz = _internalindexesRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice_dateadded", Date.class);

                method.invoke(_internalindexesRemoteModel, price_dateadded);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getInternalindexesRemoteModel() {
        return _internalindexesRemoteModel;
    }

    public void setInternalindexesRemoteModel(
        BaseModel<?> internalindexesRemoteModel) {
        _internalindexesRemoteModel = internalindexesRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _internalindexesRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_internalindexesRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            InternalindexesLocalServiceUtil.addInternalindexes(this);
        } else {
            InternalindexesLocalServiceUtil.updateInternalindexes(this);
        }
    }

    @Override
    public Internalindexes toEscapedModel() {
        return (Internalindexes) ProxyUtil.newProxyInstance(Internalindexes.class.getClassLoader(),
            new Class[] { Internalindexes.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        InternalindexesClp clone = new InternalindexesClp();

        clone.setPrice_id(getPrice_id());
        clone.setGroupId(getGroupId());
        clone.setCompanyId(getCompanyId());
        clone.setUserId(getUserId());
        clone.setUserName(getUserName());
        clone.setCreateDate(getCreateDate());
        clone.setModifiedDate(getModifiedDate());
        clone.setProduct_id(getProduct_id());
        clone.setPrice_date(getPrice_date());
        clone.setMonth_id(getMonth_id());
        clone.setPrice_contractyear(getPrice_contractyear());
        clone.setPrice_low(getPrice_low());
        clone.setPrice_current(getPrice_current());
        clone.setPrice_high(getPrice_high());
        clone.setPrice_close(getPrice_close());
        clone.setPrice_unitquote(getPrice_unitquote());
        clone.setCurrency_id(getCurrency_id());
        clone.setUnit_id(getUnit_id());
        clone.setPrice_quantity(getPrice_quantity());
        clone.setDelivery_id(getDelivery_id());
        clone.setPriceindex_id(getPriceindex_id());
        clone.setCompany_id(getCompany_id());
        clone.setContracttype_id(getContracttype_id());
        clone.setCity_id(getCity_id());
        clone.setPort_id(getPort_id());
        clone.setAirport_id(getAirport_id());
        clone.setState_id(getState_id());
        clone.setSubregion_id(getSubregion_id());
        clone.setCountry_id(getCountry_id());
        clone.setPublish_id(getPublish_id());
        clone.setCurrent_id(getCurrent_id());
        clone.setPrice_highlight(getPrice_highlight());
        clone.setPrice_mainhighlight(getPrice_mainhighlight());
        clone.setPrice_unqouted(getPrice_unqouted());
        clone.setPrice_lastedit(getPrice_lastedit());
        clone.setPrice_dateadded(getPrice_dateadded());

        return clone;
    }

    @Override
    public int compareTo(Internalindexes internalindexes) {
        int value = 0;

        if (getProduct_id() < internalindexes.getProduct_id()) {
            value = -1;
        } else if (getProduct_id() > internalindexes.getProduct_id()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof InternalindexesClp)) {
            return false;
        }

        InternalindexesClp internalindexes = (InternalindexesClp) obj;

        int primaryKey = internalindexes.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(73);

        sb.append("{price_id=");
        sb.append(getPrice_id());
        sb.append(", groupId=");
        sb.append(getGroupId());
        sb.append(", companyId=");
        sb.append(getCompanyId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", userName=");
        sb.append(getUserName());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", product_id=");
        sb.append(getProduct_id());
        sb.append(", price_date=");
        sb.append(getPrice_date());
        sb.append(", month_id=");
        sb.append(getMonth_id());
        sb.append(", price_contractyear=");
        sb.append(getPrice_contractyear());
        sb.append(", price_low=");
        sb.append(getPrice_low());
        sb.append(", price_current=");
        sb.append(getPrice_current());
        sb.append(", price_high=");
        sb.append(getPrice_high());
        sb.append(", price_close=");
        sb.append(getPrice_close());
        sb.append(", price_unitquote=");
        sb.append(getPrice_unitquote());
        sb.append(", currency_id=");
        sb.append(getCurrency_id());
        sb.append(", unit_id=");
        sb.append(getUnit_id());
        sb.append(", price_quantity=");
        sb.append(getPrice_quantity());
        sb.append(", delivery_id=");
        sb.append(getDelivery_id());
        sb.append(", priceindex_id=");
        sb.append(getPriceindex_id());
        sb.append(", company_id=");
        sb.append(getCompany_id());
        sb.append(", contracttype_id=");
        sb.append(getContracttype_id());
        sb.append(", city_id=");
        sb.append(getCity_id());
        sb.append(", port_id=");
        sb.append(getPort_id());
        sb.append(", airport_id=");
        sb.append(getAirport_id());
        sb.append(", state_id=");
        sb.append(getState_id());
        sb.append(", subregion_id=");
        sb.append(getSubregion_id());
        sb.append(", country_id=");
        sb.append(getCountry_id());
        sb.append(", publish_id=");
        sb.append(getPublish_id());
        sb.append(", current_id=");
        sb.append(getCurrent_id());
        sb.append(", price_highlight=");
        sb.append(getPrice_highlight());
        sb.append(", price_mainhighlight=");
        sb.append(getPrice_mainhighlight());
        sb.append(", price_unqouted=");
        sb.append(getPrice_unqouted());
        sb.append(", price_lastedit=");
        sb.append(getPrice_lastedit());
        sb.append(", price_dateadded=");
        sb.append(getPrice_dateadded());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(112);

        sb.append("<model><model-name>");
        sb.append("com.energyindex.liferay.sb.model.Internalindexes");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>price_id</column-name><column-value><![CDATA[");
        sb.append(getPrice_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>groupId</column-name><column-value><![CDATA[");
        sb.append(getGroupId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>companyId</column-name><column-value><![CDATA[");
        sb.append(getCompanyId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userName</column-name><column-value><![CDATA[");
        sb.append(getUserName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>product_id</column-name><column-value><![CDATA[");
        sb.append(getProduct_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_date</column-name><column-value><![CDATA[");
        sb.append(getPrice_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>month_id</column-name><column-value><![CDATA[");
        sb.append(getMonth_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_contractyear</column-name><column-value><![CDATA[");
        sb.append(getPrice_contractyear());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_low</column-name><column-value><![CDATA[");
        sb.append(getPrice_low());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_current</column-name><column-value><![CDATA[");
        sb.append(getPrice_current());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_high</column-name><column-value><![CDATA[");
        sb.append(getPrice_high());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_close</column-name><column-value><![CDATA[");
        sb.append(getPrice_close());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_unitquote</column-name><column-value><![CDATA[");
        sb.append(getPrice_unitquote());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>currency_id</column-name><column-value><![CDATA[");
        sb.append(getCurrency_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>unit_id</column-name><column-value><![CDATA[");
        sb.append(getUnit_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_quantity</column-name><column-value><![CDATA[");
        sb.append(getPrice_quantity());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>delivery_id</column-name><column-value><![CDATA[");
        sb.append(getDelivery_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>priceindex_id</column-name><column-value><![CDATA[");
        sb.append(getPriceindex_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>company_id</column-name><column-value><![CDATA[");
        sb.append(getCompany_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>contracttype_id</column-name><column-value><![CDATA[");
        sb.append(getContracttype_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>city_id</column-name><column-value><![CDATA[");
        sb.append(getCity_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>port_id</column-name><column-value><![CDATA[");
        sb.append(getPort_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>airport_id</column-name><column-value><![CDATA[");
        sb.append(getAirport_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>state_id</column-name><column-value><![CDATA[");
        sb.append(getState_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subregion_id</column-name><column-value><![CDATA[");
        sb.append(getSubregion_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>country_id</column-name><column-value><![CDATA[");
        sb.append(getCountry_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>publish_id</column-name><column-value><![CDATA[");
        sb.append(getPublish_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>current_id</column-name><column-value><![CDATA[");
        sb.append(getCurrent_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_highlight</column-name><column-value><![CDATA[");
        sb.append(getPrice_highlight());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_mainhighlight</column-name><column-value><![CDATA[");
        sb.append(getPrice_mainhighlight());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_unqouted</column-name><column-value><![CDATA[");
        sb.append(getPrice_unqouted());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_lastedit</column-name><column-value><![CDATA[");
        sb.append(getPrice_lastedit());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price_dateadded</column-name><column-value><![CDATA[");
        sb.append(getPrice_dateadded());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
