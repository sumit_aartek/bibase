package com.energyindex.liferay.sb.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.energyindex.liferay.sb.service.http.NormalizationServiceSoap}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.http.NormalizationServiceSoap
 * @generated
 */
public class NormalizationSoap implements Serializable {
    private int _normId;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private String _userName;
    private Date _createDate;
    private Date _modifiedDate;
    private String _searchExpr;
    private String _normType;
    private String _longDesc;
    private String _shortDesc;
    private int _isPrefix;
    private int _isPostfix;

    public NormalizationSoap() {
    }

    public static NormalizationSoap toSoapModel(Normalization model) {
        NormalizationSoap soapModel = new NormalizationSoap();

        soapModel.setNormId(model.getNormId());
        soapModel.setGroupId(model.getGroupId());
        soapModel.setCompanyId(model.getCompanyId());
        soapModel.setUserId(model.getUserId());
        soapModel.setUserName(model.getUserName());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setSearchExpr(model.getSearchExpr());
        soapModel.setNormType(model.getNormType());
        soapModel.setLongDesc(model.getLongDesc());
        soapModel.setShortDesc(model.getShortDesc());
        soapModel.setIsPrefix(model.getIsPrefix());
        soapModel.setIsPostfix(model.getIsPostfix());

        return soapModel;
    }

    public static NormalizationSoap[] toSoapModels(Normalization[] models) {
        NormalizationSoap[] soapModels = new NormalizationSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static NormalizationSoap[][] toSoapModels(Normalization[][] models) {
        NormalizationSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new NormalizationSoap[models.length][models[0].length];
        } else {
            soapModels = new NormalizationSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static NormalizationSoap[] toSoapModels(List<Normalization> models) {
        List<NormalizationSoap> soapModels = new ArrayList<NormalizationSoap>(models.size());

        for (Normalization model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new NormalizationSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _normId;
    }

    public void setPrimaryKey(int pk) {
        setNormId(pk);
    }

    public int getNormId() {
        return _normId;
    }

    public void setNormId(int normId) {
        _normId = normId;
    }

    public long getGroupId() {
        return _groupId;
    }

    public void setGroupId(long groupId) {
        _groupId = groupId;
    }

    public long getCompanyId() {
        return _companyId;
    }

    public void setCompanyId(long companyId) {
        _companyId = companyId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public String getUserName() {
        return _userName;
    }

    public void setUserName(String userName) {
        _userName = userName;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public String getSearchExpr() {
        return _searchExpr;
    }

    public void setSearchExpr(String searchExpr) {
        _searchExpr = searchExpr;
    }

    public String getNormType() {
        return _normType;
    }

    public void setNormType(String normType) {
        _normType = normType;
    }

    public String getLongDesc() {
        return _longDesc;
    }

    public void setLongDesc(String longDesc) {
        _longDesc = longDesc;
    }

    public String getShortDesc() {
        return _shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        _shortDesc = shortDesc;
    }

    public int getIsPrefix() {
        return _isPrefix;
    }

    public void setIsPrefix(int isPrefix) {
        _isPrefix = isPrefix;
    }

    public int getIsPostfix() {
        return _isPostfix;
    }

    public void setIsPostfix(int isPostfix) {
        _isPostfix = isPostfix;
    }
}
