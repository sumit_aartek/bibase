package com.energyindex.liferay.sb.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PriceHorizService}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceHorizService
 * @generated
 */
public class PriceHorizServiceWrapper implements PriceHorizService,
    ServiceWrapper<PriceHorizService> {
    private PriceHorizService _priceHorizService;

    public PriceHorizServiceWrapper(PriceHorizService priceHorizService) {
        _priceHorizService = priceHorizService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _priceHorizService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _priceHorizService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _priceHorizService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PriceHorizService getWrappedPriceHorizService() {
        return _priceHorizService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPriceHorizService(PriceHorizService priceHorizService) {
        _priceHorizService = priceHorizService;
    }

    @Override
    public PriceHorizService getWrappedService() {
        return _priceHorizService;
    }

    @Override
    public void setWrappedService(PriceHorizService priceHorizService) {
        _priceHorizService = priceHorizService;
    }
}
