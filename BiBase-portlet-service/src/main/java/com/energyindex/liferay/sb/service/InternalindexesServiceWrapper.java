package com.energyindex.liferay.sb.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InternalindexesService}.
 *
 * @author InfinityFrame : Jason Gonin
 * @see InternalindexesService
 * @generated
 */
public class InternalindexesServiceWrapper implements InternalindexesService,
    ServiceWrapper<InternalindexesService> {
    private InternalindexesService _internalindexesService;

    public InternalindexesServiceWrapper(
        InternalindexesService internalindexesService) {
        _internalindexesService = internalindexesService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _internalindexesService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _internalindexesService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _internalindexesService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public InternalindexesService getWrappedInternalindexesService() {
        return _internalindexesService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedInternalindexesService(
        InternalindexesService internalindexesService) {
        _internalindexesService = internalindexesService;
    }

    @Override
    public InternalindexesService getWrappedService() {
        return _internalindexesService;
    }

    @Override
    public void setWrappedService(InternalindexesService internalindexesService) {
        _internalindexesService = internalindexesService;
    }
}
