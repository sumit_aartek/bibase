package com.energyindex.liferay.sb.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for PriceVert. This utility wraps
 * {@link com.energyindex.liferay.sb.service.impl.PriceVertServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVertService
 * @see com.energyindex.liferay.sb.service.base.PriceVertServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.impl.PriceVertServiceImpl
 * @generated
 */
public class PriceVertServiceUtil {
    private static PriceVertService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.energyindex.liferay.sb.service.impl.PriceVertServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.energyindex.liferay.sb.model.PriceVert addPriceVert(
        long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addPriceVert(userId, groupId, price_close, port_id,
            product_id, month, day, year, portId, serviceContext);
    }

    public static com.energyindex.liferay.sb.model.PriceVert deletePriceVert(
        long price_id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePriceVert(price_id);
    }

    public static com.energyindex.liferay.sb.model.PriceVert getPriceVert(
        long eventId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPriceVert(eventId);
    }

    public static com.energyindex.liferay.sb.model.PriceVert updatePriceVert(
        int priceId, long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updatePriceVert(priceId, userId, groupId, price_close,
            port_id, product_id, month, day, year, portId, serviceContext);
    }

    public static void clearService() {
        _service = null;
    }

    public static PriceVertService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    PriceVertService.class.getName());

            if (invokableService instanceof PriceVertService) {
                _service = (PriceVertService) invokableService;
            } else {
                _service = new PriceVertServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(PriceVertServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(PriceVertService service) {
    }
}
