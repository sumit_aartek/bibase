package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.model.PriceVert;
import com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author InfinityFrame : Jason Gonin
 * @generated
 */
public abstract class PriceVertActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PriceVertActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PriceVertLocalServiceUtil.getService());
        setClass(PriceVert.class);

        setClassLoader(com.energyindex.liferay.sb.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("price_id");
    }
}
