package com.energyindex.liferay.sb.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Normalization}.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see Normalization
 * @generated
 */
public class NormalizationWrapper implements Normalization,
    ModelWrapper<Normalization> {
    private Normalization _normalization;

    public NormalizationWrapper(Normalization normalization) {
        _normalization = normalization;
    }

    @Override
    public Class<?> getModelClass() {
        return Normalization.class;
    }

    @Override
    public String getModelClassName() {
        return Normalization.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("normId", getNormId());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("userName", getUserName());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("searchExpr", getSearchExpr());
        attributes.put("normType", getNormType());
        attributes.put("longDesc", getLongDesc());
        attributes.put("shortDesc", getShortDesc());
        attributes.put("isPrefix", getIsPrefix());
        attributes.put("isPostfix", getIsPostfix());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Integer normId = (Integer) attributes.get("normId");

        if (normId != null) {
            setNormId(normId);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        String userName = (String) attributes.get("userName");

        if (userName != null) {
            setUserName(userName);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String searchExpr = (String) attributes.get("searchExpr");

        if (searchExpr != null) {
            setSearchExpr(searchExpr);
        }

        String normType = (String) attributes.get("normType");

        if (normType != null) {
            setNormType(normType);
        }

        String longDesc = (String) attributes.get("longDesc");

        if (longDesc != null) {
            setLongDesc(longDesc);
        }

        String shortDesc = (String) attributes.get("shortDesc");

        if (shortDesc != null) {
            setShortDesc(shortDesc);
        }

        Integer isPrefix = (Integer) attributes.get("isPrefix");

        if (isPrefix != null) {
            setIsPrefix(isPrefix);
        }

        Integer isPostfix = (Integer) attributes.get("isPostfix");

        if (isPostfix != null) {
            setIsPostfix(isPostfix);
        }
    }

    /**
    * Returns the primary key of this normalization.
    *
    * @return the primary key of this normalization
    */
    @Override
    public int getPrimaryKey() {
        return _normalization.getPrimaryKey();
    }

    /**
    * Sets the primary key of this normalization.
    *
    * @param primaryKey the primary key of this normalization
    */
    @Override
    public void setPrimaryKey(int primaryKey) {
        _normalization.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the norm ID of this normalization.
    *
    * @return the norm ID of this normalization
    */
    @Override
    public int getNormId() {
        return _normalization.getNormId();
    }

    /**
    * Sets the norm ID of this normalization.
    *
    * @param normId the norm ID of this normalization
    */
    @Override
    public void setNormId(int normId) {
        _normalization.setNormId(normId);
    }

    /**
    * Returns the group ID of this normalization.
    *
    * @return the group ID of this normalization
    */
    @Override
    public long getGroupId() {
        return _normalization.getGroupId();
    }

    /**
    * Sets the group ID of this normalization.
    *
    * @param groupId the group ID of this normalization
    */
    @Override
    public void setGroupId(long groupId) {
        _normalization.setGroupId(groupId);
    }

    /**
    * Returns the company ID of this normalization.
    *
    * @return the company ID of this normalization
    */
    @Override
    public long getCompanyId() {
        return _normalization.getCompanyId();
    }

    /**
    * Sets the company ID of this normalization.
    *
    * @param companyId the company ID of this normalization
    */
    @Override
    public void setCompanyId(long companyId) {
        _normalization.setCompanyId(companyId);
    }

    /**
    * Returns the user ID of this normalization.
    *
    * @return the user ID of this normalization
    */
    @Override
    public long getUserId() {
        return _normalization.getUserId();
    }

    /**
    * Sets the user ID of this normalization.
    *
    * @param userId the user ID of this normalization
    */
    @Override
    public void setUserId(long userId) {
        _normalization.setUserId(userId);
    }

    /**
    * Returns the user uuid of this normalization.
    *
    * @return the user uuid of this normalization
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _normalization.getUserUuid();
    }

    /**
    * Sets the user uuid of this normalization.
    *
    * @param userUuid the user uuid of this normalization
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _normalization.setUserUuid(userUuid);
    }

    /**
    * Returns the user name of this normalization.
    *
    * @return the user name of this normalization
    */
    @Override
    public java.lang.String getUserName() {
        return _normalization.getUserName();
    }

    /**
    * Sets the user name of this normalization.
    *
    * @param userName the user name of this normalization
    */
    @Override
    public void setUserName(java.lang.String userName) {
        _normalization.setUserName(userName);
    }

    /**
    * Returns the create date of this normalization.
    *
    * @return the create date of this normalization
    */
    @Override
    public java.util.Date getCreateDate() {
        return _normalization.getCreateDate();
    }

    /**
    * Sets the create date of this normalization.
    *
    * @param createDate the create date of this normalization
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _normalization.setCreateDate(createDate);
    }

    /**
    * Returns the modified date of this normalization.
    *
    * @return the modified date of this normalization
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _normalization.getModifiedDate();
    }

    /**
    * Sets the modified date of this normalization.
    *
    * @param modifiedDate the modified date of this normalization
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _normalization.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the search expr of this normalization.
    *
    * @return the search expr of this normalization
    */
    @Override
    public java.lang.String getSearchExpr() {
        return _normalization.getSearchExpr();
    }

    /**
    * Sets the search expr of this normalization.
    *
    * @param searchExpr the search expr of this normalization
    */
    @Override
    public void setSearchExpr(java.lang.String searchExpr) {
        _normalization.setSearchExpr(searchExpr);
    }

    /**
    * Returns the norm type of this normalization.
    *
    * @return the norm type of this normalization
    */
    @Override
    public java.lang.String getNormType() {
        return _normalization.getNormType();
    }

    /**
    * Sets the norm type of this normalization.
    *
    * @param normType the norm type of this normalization
    */
    @Override
    public void setNormType(java.lang.String normType) {
        _normalization.setNormType(normType);
    }

    /**
    * Returns the long desc of this normalization.
    *
    * @return the long desc of this normalization
    */
    @Override
    public java.lang.String getLongDesc() {
        return _normalization.getLongDesc();
    }

    /**
    * Sets the long desc of this normalization.
    *
    * @param longDesc the long desc of this normalization
    */
    @Override
    public void setLongDesc(java.lang.String longDesc) {
        _normalization.setLongDesc(longDesc);
    }

    /**
    * Returns the short desc of this normalization.
    *
    * @return the short desc of this normalization
    */
    @Override
    public java.lang.String getShortDesc() {
        return _normalization.getShortDesc();
    }

    /**
    * Sets the short desc of this normalization.
    *
    * @param shortDesc the short desc of this normalization
    */
    @Override
    public void setShortDesc(java.lang.String shortDesc) {
        _normalization.setShortDesc(shortDesc);
    }

    /**
    * Returns the is prefix of this normalization.
    *
    * @return the is prefix of this normalization
    */
    @Override
    public int getIsPrefix() {
        return _normalization.getIsPrefix();
    }

    /**
    * Sets the is prefix of this normalization.
    *
    * @param isPrefix the is prefix of this normalization
    */
    @Override
    public void setIsPrefix(int isPrefix) {
        _normalization.setIsPrefix(isPrefix);
    }

    /**
    * Returns the is postfix of this normalization.
    *
    * @return the is postfix of this normalization
    */
    @Override
    public int getIsPostfix() {
        return _normalization.getIsPostfix();
    }

    /**
    * Sets the is postfix of this normalization.
    *
    * @param isPostfix the is postfix of this normalization
    */
    @Override
    public void setIsPostfix(int isPostfix) {
        _normalization.setIsPostfix(isPostfix);
    }

    @Override
    public boolean isNew() {
        return _normalization.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _normalization.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _normalization.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _normalization.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _normalization.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _normalization.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _normalization.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _normalization.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _normalization.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _normalization.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _normalization.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new NormalizationWrapper((Normalization) _normalization.clone());
    }

    @Override
    public int compareTo(
        com.energyindex.liferay.sb.model.Normalization normalization) {
        return _normalization.compareTo(normalization);
    }

    @Override
    public int hashCode() {
        return _normalization.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<com.energyindex.liferay.sb.model.Normalization> toCacheModel() {
        return _normalization.toCacheModel();
    }

    @Override
    public com.energyindex.liferay.sb.model.Normalization toEscapedModel() {
        return new NormalizationWrapper(_normalization.toEscapedModel());
    }

    @Override
    public com.energyindex.liferay.sb.model.Normalization toUnescapedModel() {
        return new NormalizationWrapper(_normalization.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _normalization.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _normalization.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _normalization.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof NormalizationWrapper)) {
            return false;
        }

        NormalizationWrapper normalizationWrapper = (NormalizationWrapper) obj;

        if (Validator.equals(_normalization, normalizationWrapper._normalization)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Normalization getWrappedNormalization() {
        return _normalization;
    }

    @Override
    public Normalization getWrappedModel() {
        return _normalization;
    }

    @Override
    public void resetOriginalValues() {
        _normalization.resetOriginalValues();
    }
}
