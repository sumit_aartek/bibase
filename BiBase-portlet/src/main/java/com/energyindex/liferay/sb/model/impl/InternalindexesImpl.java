package com.energyindex.liferay.sb.model.impl;

/**
 * The extended model implementation for the Internalindexes service. Represents a row in the &quot;BiBase_Internalindexes&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.model.Internalindexes} interface.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 */
public class InternalindexesImpl extends InternalindexesBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a internalindexes model instance should use the {@link com.energyindex.liferay.sb.model.Internalindexes} interface instead.
     */
    public InternalindexesImpl() {
    }
}
