package com.energyindex.liferay.sb.model.impl;

import com.energyindex.liferay.sb.model.Normalization;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Normalization in entity cache.
 *
 * @author InfinityFrame : Jason Gonin
 * @see Normalization
 * @generated
 */
public class NormalizationCacheModel implements CacheModel<Normalization>,
    Externalizable {
    public int normId;
    public long groupId;
    public long companyId;
    public long userId;
    public String userName;
    public long createDate;
    public long modifiedDate;
    public String searchExpr;
    public String normType;
    public String longDesc;
    public String shortDesc;
    public int isPrefix;
    public int isPostfix;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(27);

        sb.append("{normId=");
        sb.append(normId);
        sb.append(", groupId=");
        sb.append(groupId);
        sb.append(", companyId=");
        sb.append(companyId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", userName=");
        sb.append(userName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", searchExpr=");
        sb.append(searchExpr);
        sb.append(", normType=");
        sb.append(normType);
        sb.append(", longDesc=");
        sb.append(longDesc);
        sb.append(", shortDesc=");
        sb.append(shortDesc);
        sb.append(", isPrefix=");
        sb.append(isPrefix);
        sb.append(", isPostfix=");
        sb.append(isPostfix);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Normalization toEntityModel() {
        NormalizationImpl normalizationImpl = new NormalizationImpl();

        normalizationImpl.setNormId(normId);
        normalizationImpl.setGroupId(groupId);
        normalizationImpl.setCompanyId(companyId);
        normalizationImpl.setUserId(userId);

        if (userName == null) {
            normalizationImpl.setUserName(StringPool.BLANK);
        } else {
            normalizationImpl.setUserName(userName);
        }

        if (createDate == Long.MIN_VALUE) {
            normalizationImpl.setCreateDate(null);
        } else {
            normalizationImpl.setCreateDate(new Date(createDate));
        }

        if (modifiedDate == Long.MIN_VALUE) {
            normalizationImpl.setModifiedDate(null);
        } else {
            normalizationImpl.setModifiedDate(new Date(modifiedDate));
        }

        if (searchExpr == null) {
            normalizationImpl.setSearchExpr(StringPool.BLANK);
        } else {
            normalizationImpl.setSearchExpr(searchExpr);
        }

        if (normType == null) {
            normalizationImpl.setNormType(StringPool.BLANK);
        } else {
            normalizationImpl.setNormType(normType);
        }

        if (longDesc == null) {
            normalizationImpl.setLongDesc(StringPool.BLANK);
        } else {
            normalizationImpl.setLongDesc(longDesc);
        }

        if (shortDesc == null) {
            normalizationImpl.setShortDesc(StringPool.BLANK);
        } else {
            normalizationImpl.setShortDesc(shortDesc);
        }

        normalizationImpl.setIsPrefix(isPrefix);
        normalizationImpl.setIsPostfix(isPostfix);

        normalizationImpl.resetOriginalValues();

        return normalizationImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        normId = objectInput.readInt();
        groupId = objectInput.readLong();
        companyId = objectInput.readLong();
        userId = objectInput.readLong();
        userName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifiedDate = objectInput.readLong();
        searchExpr = objectInput.readUTF();
        normType = objectInput.readUTF();
        longDesc = objectInput.readUTF();
        shortDesc = objectInput.readUTF();
        isPrefix = objectInput.readInt();
        isPostfix = objectInput.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeInt(normId);
        objectOutput.writeLong(groupId);
        objectOutput.writeLong(companyId);
        objectOutput.writeLong(userId);

        if (userName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(userName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifiedDate);

        if (searchExpr == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(searchExpr);
        }

        if (normType == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(normType);
        }

        if (longDesc == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(longDesc);
        }

        if (shortDesc == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(shortDesc);
        }

        objectOutput.writeInt(isPrefix);
        objectOutput.writeInt(isPostfix);
    }
}
