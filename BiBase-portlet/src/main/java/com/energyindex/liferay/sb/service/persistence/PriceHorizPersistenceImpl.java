package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.NoSuchPriceHorizException;
import com.energyindex.liferay.sb.model.PriceHoriz;
import com.energyindex.liferay.sb.model.impl.PriceHorizImpl;
import com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl;
import com.energyindex.liferay.sb.service.persistence.PriceHorizPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The persistence implementation for the price horiz service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceHorizPersistence
 * @see PriceHorizUtil
 * @generated
 */
public class PriceHorizPersistenceImpl extends BasePersistenceImpl<PriceHoriz>
    implements PriceHorizPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PriceHorizUtil} to access the price horiz persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PriceHorizImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, PriceHorizImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, PriceHorizImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHPRODUCT_ID =
        new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, PriceHorizImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBypricehProduct_id",
            new String[] {
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHPRODUCT_ID =
        new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, PriceHorizImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findBypricehProduct_id", new String[] { Integer.class.getName() },
            PriceHorizModelImpl.PRODUCT_ID_COLUMN_BITMASK |
            PriceHorizModelImpl.PORT_ID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PRICEHPRODUCT_ID = new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countBypricehProduct_id", new String[] { Integer.class.getName() });
    private static final String _FINDER_COLUMN_PRICEHPRODUCT_ID_PRODUCT_ID_2 = "priceHoriz.product_id = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_DATE =
        new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, PriceHorizImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBypricehP_date",
            new String[] {
                Date.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_DATE =
        new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countBypricehP_date",
            new String[] { Date.class.getName() });
    private static final String _FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_1 = "priceHoriz.price_date >= NULL";
    private static final String _FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_2 = "priceHoriz.price_date >= ?";
    private static final String _FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_4 = "(" +
        removeConjunction(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_1) + ")";
    private static final String _FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_5 = "(" +
        removeConjunction(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_2) + ")";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_ID =
        new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, PriceHorizImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBypricehP_id",
            new String[] {
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHP_ID =
        new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, PriceHorizImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBypricehP_id",
            new String[] { Integer.class.getName() },
            PriceHorizModelImpl.PORT_ID_COLUMN_BITMASK |
            PriceHorizModelImpl.PRODUCT_ID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PRICEHP_ID = new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBypricehP_id",
            new String[] { Integer.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_ID =
        new FinderPath(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countBypricehP_id",
            new String[] { Integer.class.getName() });
    private static final String _FINDER_COLUMN_PRICEHP_ID_PORT_ID_2 = "priceHoriz.port_id = ?";
    private static final String _FINDER_COLUMN_PRICEHP_ID_PORT_ID_5 = "(" +
        removeConjunction(_FINDER_COLUMN_PRICEHP_ID_PORT_ID_2) + ")";
    private static final String _SQL_SELECT_PRICEHORIZ = "SELECT priceHoriz FROM PriceHoriz priceHoriz";
    private static final String _SQL_SELECT_PRICEHORIZ_WHERE = "SELECT priceHoriz FROM PriceHoriz priceHoriz WHERE ";
    private static final String _SQL_COUNT_PRICEHORIZ = "SELECT COUNT(priceHoriz) FROM PriceHoriz priceHoriz";
    private static final String _SQL_COUNT_PRICEHORIZ_WHERE = "SELECT COUNT(priceHoriz) FROM PriceHoriz priceHoriz WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "priceHoriz.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PriceHoriz exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PriceHoriz exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PriceHorizPersistenceImpl.class);
    private static PriceHoriz _nullPriceHoriz = new PriceHorizImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<PriceHoriz> toCacheModel() {
                return _nullPriceHorizCacheModel;
            }
        };

    private static CacheModel<PriceHoriz> _nullPriceHorizCacheModel = new CacheModel<PriceHoriz>() {
            @Override
            public PriceHoriz toEntityModel() {
                return _nullPriceHoriz;
            }
        };

    public PriceHorizPersistenceImpl() {
        setModelClass(PriceHoriz.class);
    }

    /**
     * Returns all the price horizs where product_id = &#63;.
     *
     * @param product_id the product_id
     * @return the matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehProduct_id(int product_id)
        throws SystemException {
        return findBypricehProduct_id(product_id, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price horizs where product_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param product_id the product_id
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @return the range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehProduct_id(int product_id, int start,
        int end) throws SystemException {
        return findBypricehProduct_id(product_id, start, end, null);
    }

    /**
     * Returns an ordered range of all the price horizs where product_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param product_id the product_id
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehProduct_id(int product_id, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHPRODUCT_ID;
            finderArgs = new Object[] { product_id };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHPRODUCT_ID;
            finderArgs = new Object[] { product_id, start, end, orderByComparator };
        }

        List<PriceHoriz> list = (List<PriceHoriz>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceHoriz priceHoriz : list) {
                if ((product_id != priceHoriz.getProduct_id())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PRICEHORIZ_WHERE);

            query.append(_FINDER_COLUMN_PRICEHPRODUCT_ID_PRODUCT_ID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceHorizModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(product_id);

                if (!pagination) {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceHoriz>(list);
                } else {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first price horiz in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz findBypricehProduct_id_First(int product_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = fetchBypricehProduct_id_First(product_id,
                orderByComparator);

        if (priceHoriz != null) {
            return priceHoriz;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("product_id=");
        msg.append(product_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceHorizException(msg.toString());
    }

    /**
     * Returns the first price horiz in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price horiz, or <code>null</code> if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz fetchBypricehProduct_id_First(int product_id,
        OrderByComparator orderByComparator) throws SystemException {
        List<PriceHoriz> list = findBypricehProduct_id(product_id, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last price horiz in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz findBypricehProduct_id_Last(int product_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = fetchBypricehProduct_id_Last(product_id,
                orderByComparator);

        if (priceHoriz != null) {
            return priceHoriz;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("product_id=");
        msg.append(product_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceHorizException(msg.toString());
    }

    /**
     * Returns the last price horiz in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price horiz, or <code>null</code> if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz fetchBypricehProduct_id_Last(int product_id,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countBypricehProduct_id(product_id);

        if (count == 0) {
            return null;
        }

        List<PriceHoriz> list = findBypricehProduct_id(product_id, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the price horizs before and after the current price horiz in the ordered set where product_id = &#63;.
     *
     * @param priceHorizPK the primary key of the current price horiz
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz[] findBypricehProduct_id_PrevAndNext(
        PriceHorizPK priceHorizPK, int product_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = findByPrimaryKey(priceHorizPK);

        Session session = null;

        try {
            session = openSession();

            PriceHoriz[] array = new PriceHorizImpl[3];

            array[0] = getBypricehProduct_id_PrevAndNext(session, priceHoriz,
                    product_id, orderByComparator, true);

            array[1] = priceHoriz;

            array[2] = getBypricehProduct_id_PrevAndNext(session, priceHoriz,
                    product_id, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected PriceHoriz getBypricehProduct_id_PrevAndNext(Session session,
        PriceHoriz priceHoriz, int product_id,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PRICEHORIZ_WHERE);

        query.append(_FINDER_COLUMN_PRICEHPRODUCT_ID_PRODUCT_ID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PriceHorizModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(product_id);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(priceHoriz);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<PriceHoriz> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the price horizs where product_id = &#63; from the database.
     *
     * @param product_id the product_id
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeBypricehProduct_id(int product_id)
        throws SystemException {
        for (PriceHoriz priceHoriz : findBypricehProduct_id(product_id,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(priceHoriz);
        }
    }

    /**
     * Returns the number of price horizs where product_id = &#63;.
     *
     * @param product_id the product_id
     * @return the number of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBypricehProduct_id(int product_id)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PRICEHPRODUCT_ID;

        Object[] finderArgs = new Object[] { product_id };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PRICEHORIZ_WHERE);

            query.append(_FINDER_COLUMN_PRICEHPRODUCT_ID_PRODUCT_ID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(product_id);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the price horizs where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @return the matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_date(Date price_date)
        throws SystemException {
        return findBypricehP_date(price_date, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price horizs where price_date &ge; &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_date the price_date
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @return the range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_date(Date price_date, int start,
        int end) throws SystemException {
        return findBypricehP_date(price_date, start, end, null);
    }

    /**
     * Returns an ordered range of all the price horizs where price_date &ge; &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_date the price_date
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_date(Date price_date, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_DATE;
        finderArgs = new Object[] { price_date, start, end, orderByComparator };

        List<PriceHoriz> list = (List<PriceHoriz>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceHoriz priceHoriz : list) {
                if ((price_date.getTime() > priceHoriz.getPrice_date().getTime())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PRICEHORIZ_WHERE);

            boolean bindPrice_date = false;

            if (price_date == null) {
                query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_1);
            } else {
                bindPrice_date = true;

                query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceHorizModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindPrice_date) {
                    qPos.add(CalendarUtil.getTimestamp(price_date));
                }

                if (!pagination) {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceHoriz>(list);
                } else {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first price horiz in the ordered set where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz findBypricehP_date_First(Date price_date,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = fetchBypricehP_date_First(price_date,
                orderByComparator);

        if (priceHoriz != null) {
            return priceHoriz;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("price_date=");
        msg.append(price_date);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceHorizException(msg.toString());
    }

    /**
     * Returns the first price horiz in the ordered set where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price horiz, or <code>null</code> if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz fetchBypricehP_date_First(Date price_date,
        OrderByComparator orderByComparator) throws SystemException {
        List<PriceHoriz> list = findBypricehP_date(price_date, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last price horiz in the ordered set where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz findBypricehP_date_Last(Date price_date,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = fetchBypricehP_date_Last(price_date,
                orderByComparator);

        if (priceHoriz != null) {
            return priceHoriz;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("price_date=");
        msg.append(price_date);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceHorizException(msg.toString());
    }

    /**
     * Returns the last price horiz in the ordered set where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price horiz, or <code>null</code> if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz fetchBypricehP_date_Last(Date price_date,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countBypricehP_date(price_date);

        if (count == 0) {
            return null;
        }

        List<PriceHoriz> list = findBypricehP_date(price_date, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the price horizs before and after the current price horiz in the ordered set where price_date &ge; &#63;.
     *
     * @param priceHorizPK the primary key of the current price horiz
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz[] findBypricehP_date_PrevAndNext(
        PriceHorizPK priceHorizPK, Date price_date,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = findByPrimaryKey(priceHorizPK);

        Session session = null;

        try {
            session = openSession();

            PriceHoriz[] array = new PriceHorizImpl[3];

            array[0] = getBypricehP_date_PrevAndNext(session, priceHoriz,
                    price_date, orderByComparator, true);

            array[1] = priceHoriz;

            array[2] = getBypricehP_date_PrevAndNext(session, priceHoriz,
                    price_date, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected PriceHoriz getBypricehP_date_PrevAndNext(Session session,
        PriceHoriz priceHoriz, Date price_date,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PRICEHORIZ_WHERE);

        boolean bindPrice_date = false;

        if (price_date == null) {
            query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_1);
        } else {
            bindPrice_date = true;

            query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PriceHorizModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindPrice_date) {
            qPos.add(CalendarUtil.getTimestamp(price_date));
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(priceHoriz);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<PriceHoriz> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the price horizs where price_date &ge; all &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_dates the price_dates
     * @return the matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_date(Date[] price_dates)
        throws SystemException {
        return findBypricehP_date(price_dates, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price horizs where price_date &ge; all &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_dates the price_dates
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @return the range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_date(Date[] price_dates, int start,
        int end) throws SystemException {
        return findBypricehP_date(price_dates, start, end, null);
    }

    /**
     * Returns an ordered range of all the price horizs where price_date &ge; all &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_dates the price_dates
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_date(Date[] price_dates, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        if ((price_dates != null) && (price_dates.length == 1)) {
            return findBypricehP_date(price_dates[0], start, end,
                orderByComparator);
        }

        boolean pagination = true;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderArgs = new Object[] { StringUtil.merge(price_dates) };
        } else {
            finderArgs = new Object[] {
                    StringUtil.merge(price_dates),
                    
                    start, end, orderByComparator
                };
        }

        List<PriceHoriz> list = (List<PriceHoriz>) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_DATE,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceHoriz priceHoriz : list) {
                if (!ArrayUtil.contains(price_dates, priceHoriz.getPrice_date())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_SELECT_PRICEHORIZ_WHERE);

            boolean conjunctionable = false;

            if ((price_dates == null) || (price_dates.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < price_dates.length; i++) {
                    Date price_date = price_dates[i];

                    if (price_date == null) {
                        query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_4);
                    } else {
                        query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_5);
                    }

                    if ((i + 1) < price_dates.length) {
                        query.append(WHERE_AND);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceHorizModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (price_dates != null) {
                    qPos.add(price_dates);
                }

                if (!pagination) {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceHoriz>(list);
                } else {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_DATE,
                    finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_DATE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the price horizs where price_date &ge; &#63; from the database.
     *
     * @param price_date the price_date
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeBypricehP_date(Date price_date) throws SystemException {
        for (PriceHoriz priceHoriz : findBypricehP_date(price_date,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(priceHoriz);
        }
    }

    /**
     * Returns the number of price horizs where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @return the number of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBypricehP_date(Date price_date) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_DATE;

        Object[] finderArgs = new Object[] { price_date };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PRICEHORIZ_WHERE);

            boolean bindPrice_date = false;

            if (price_date == null) {
                query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_1);
            } else {
                bindPrice_date = true;

                query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindPrice_date) {
                    qPos.add(CalendarUtil.getTimestamp(price_date));
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of price horizs where price_date &ge; all &#63;.
     *
     * @param price_dates the price_dates
     * @return the number of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBypricehP_date(Date[] price_dates)
        throws SystemException {
        Object[] finderArgs = new Object[] { StringUtil.merge(price_dates) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_DATE,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_COUNT_PRICEHORIZ_WHERE);

            boolean conjunctionable = false;

            if ((price_dates == null) || (price_dates.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < price_dates.length; i++) {
                    Date price_date = price_dates[i];

                    if (price_date == null) {
                        query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_4);
                    } else {
                        query.append(_FINDER_COLUMN_PRICEHP_DATE_PRICE_DATE_5);
                    }

                    if ((i + 1) < price_dates.length) {
                        query.append(WHERE_AND);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (price_dates != null) {
                    qPos.add(price_dates);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_DATE,
                    finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_DATE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the price horizs where port_id = &#63;.
     *
     * @param port_id the port_id
     * @return the matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_id(int port_id)
        throws SystemException {
        return findBypricehP_id(port_id, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the price horizs where port_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_id the port_id
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @return the range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_id(int port_id, int start, int end)
        throws SystemException {
        return findBypricehP_id(port_id, start, end, null);
    }

    /**
     * Returns an ordered range of all the price horizs where port_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_id the port_id
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_id(int port_id, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHP_ID;
            finderArgs = new Object[] { port_id };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_ID;
            finderArgs = new Object[] { port_id, start, end, orderByComparator };
        }

        List<PriceHoriz> list = (List<PriceHoriz>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceHoriz priceHoriz : list) {
                if ((port_id != priceHoriz.getPort_id())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PRICEHORIZ_WHERE);

            query.append(_FINDER_COLUMN_PRICEHP_ID_PORT_ID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceHorizModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(port_id);

                if (!pagination) {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceHoriz>(list);
                } else {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first price horiz in the ordered set where port_id = &#63;.
     *
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz findBypricehP_id_First(int port_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = fetchBypricehP_id_First(port_id,
                orderByComparator);

        if (priceHoriz != null) {
            return priceHoriz;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("port_id=");
        msg.append(port_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceHorizException(msg.toString());
    }

    /**
     * Returns the first price horiz in the ordered set where port_id = &#63;.
     *
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price horiz, or <code>null</code> if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz fetchBypricehP_id_First(int port_id,
        OrderByComparator orderByComparator) throws SystemException {
        List<PriceHoriz> list = findBypricehP_id(port_id, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last price horiz in the ordered set where port_id = &#63;.
     *
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz findBypricehP_id_Last(int port_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = fetchBypricehP_id_Last(port_id,
                orderByComparator);

        if (priceHoriz != null) {
            return priceHoriz;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("port_id=");
        msg.append(port_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceHorizException(msg.toString());
    }

    /**
     * Returns the last price horiz in the ordered set where port_id = &#63;.
     *
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price horiz, or <code>null</code> if a matching price horiz could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz fetchBypricehP_id_Last(int port_id,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countBypricehP_id(port_id);

        if (count == 0) {
            return null;
        }

        List<PriceHoriz> list = findBypricehP_id(port_id, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the price horizs before and after the current price horiz in the ordered set where port_id = &#63;.
     *
     * @param priceHorizPK the primary key of the current price horiz
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz[] findBypricehP_id_PrevAndNext(
        PriceHorizPK priceHorizPK, int port_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = findByPrimaryKey(priceHorizPK);

        Session session = null;

        try {
            session = openSession();

            PriceHoriz[] array = new PriceHorizImpl[3];

            array[0] = getBypricehP_id_PrevAndNext(session, priceHoriz,
                    port_id, orderByComparator, true);

            array[1] = priceHoriz;

            array[2] = getBypricehP_id_PrevAndNext(session, priceHoriz,
                    port_id, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected PriceHoriz getBypricehP_id_PrevAndNext(Session session,
        PriceHoriz priceHoriz, int port_id,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PRICEHORIZ_WHERE);

        query.append(_FINDER_COLUMN_PRICEHP_ID_PORT_ID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PriceHorizModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(port_id);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(priceHoriz);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<PriceHoriz> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the price horizs where port_id = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_ids the port_ids
     * @return the matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_id(int[] port_ids)
        throws SystemException {
        return findBypricehP_id(port_ids, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the price horizs where port_id = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_ids the port_ids
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @return the range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_id(int[] port_ids, int start, int end)
        throws SystemException {
        return findBypricehP_id(port_ids, start, end, null);
    }

    /**
     * Returns an ordered range of all the price horizs where port_id = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_ids the port_ids
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findBypricehP_id(int[] port_ids, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        if ((port_ids != null) && (port_ids.length == 1)) {
            return findBypricehP_id(port_ids[0], start, end, orderByComparator);
        }

        boolean pagination = true;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderArgs = new Object[] { StringUtil.merge(port_ids) };
        } else {
            finderArgs = new Object[] {
                    StringUtil.merge(port_ids),
                    
                    start, end, orderByComparator
                };
        }

        List<PriceHoriz> list = (List<PriceHoriz>) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_ID,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceHoriz priceHoriz : list) {
                if (!ArrayUtil.contains(port_ids, priceHoriz.getPort_id())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_SELECT_PRICEHORIZ_WHERE);

            boolean conjunctionable = false;

            if ((port_ids == null) || (port_ids.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < port_ids.length; i++) {
                    query.append(_FINDER_COLUMN_PRICEHP_ID_PORT_ID_5);

                    if ((i + 1) < port_ids.length) {
                        query.append(WHERE_OR);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceHorizModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (port_ids != null) {
                    qPos.add(port_ids);
                }

                if (!pagination) {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceHoriz>(list);
                } else {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_ID,
                    finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICEHP_ID,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the price horizs where port_id = &#63; from the database.
     *
     * @param port_id the port_id
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeBypricehP_id(int port_id) throws SystemException {
        for (PriceHoriz priceHoriz : findBypricehP_id(port_id,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(priceHoriz);
        }
    }

    /**
     * Returns the number of price horizs where port_id = &#63;.
     *
     * @param port_id the port_id
     * @return the number of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBypricehP_id(int port_id) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PRICEHP_ID;

        Object[] finderArgs = new Object[] { port_id };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PRICEHORIZ_WHERE);

            query.append(_FINDER_COLUMN_PRICEHP_ID_PORT_ID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(port_id);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of price horizs where port_id = any &#63;.
     *
     * @param port_ids the port_ids
     * @return the number of matching price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBypricehP_id(int[] port_ids) throws SystemException {
        Object[] finderArgs = new Object[] { StringUtil.merge(port_ids) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_ID,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_COUNT_PRICEHORIZ_WHERE);

            boolean conjunctionable = false;

            if ((port_ids == null) || (port_ids.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < port_ids.length; i++) {
                    query.append(_FINDER_COLUMN_PRICEHP_ID_PORT_ID_5);

                    if ((i + 1) < port_ids.length) {
                        query.append(WHERE_OR);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (port_ids != null) {
                    qPos.add(port_ids);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_ID,
                    finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICEHP_ID,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the price horiz in the entity cache if it is enabled.
     *
     * @param priceHoriz the price horiz
     */
    @Override
    public void cacheResult(PriceHoriz priceHoriz) {
        EntityCacheUtil.putResult(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizImpl.class, priceHoriz.getPrimaryKey(), priceHoriz);

        priceHoriz.resetOriginalValues();
    }

    /**
     * Caches the price horizs in the entity cache if it is enabled.
     *
     * @param priceHorizs the price horizs
     */
    @Override
    public void cacheResult(List<PriceHoriz> priceHorizs) {
        for (PriceHoriz priceHoriz : priceHorizs) {
            if (EntityCacheUtil.getResult(
                        PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
                        PriceHorizImpl.class, priceHoriz.getPrimaryKey()) == null) {
                cacheResult(priceHoriz);
            } else {
                priceHoriz.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all price horizs.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PriceHorizImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PriceHorizImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the price horiz.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(PriceHoriz priceHoriz) {
        EntityCacheUtil.removeResult(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizImpl.class, priceHoriz.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<PriceHoriz> priceHorizs) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (PriceHoriz priceHoriz : priceHorizs) {
            EntityCacheUtil.removeResult(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
                PriceHorizImpl.class, priceHoriz.getPrimaryKey());
        }
    }

    /**
     * Creates a new price horiz with the primary key. Does not add the price horiz to the database.
     *
     * @param priceHorizPK the primary key for the new price horiz
     * @return the new price horiz
     */
    @Override
    public PriceHoriz create(PriceHorizPK priceHorizPK) {
        PriceHoriz priceHoriz = new PriceHorizImpl();

        priceHoriz.setNew(true);
        priceHoriz.setPrimaryKey(priceHorizPK);

        return priceHoriz;
    }

    /**
     * Removes the price horiz with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param priceHorizPK the primary key of the price horiz
     * @return the price horiz that was removed
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz remove(PriceHorizPK priceHorizPK)
        throws NoSuchPriceHorizException, SystemException {
        return remove((Serializable) priceHorizPK);
    }

    /**
     * Removes the price horiz with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the price horiz
     * @return the price horiz that was removed
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz remove(Serializable primaryKey)
        throws NoSuchPriceHorizException, SystemException {
        Session session = null;

        try {
            session = openSession();

            PriceHoriz priceHoriz = (PriceHoriz) session.get(PriceHorizImpl.class,
                    primaryKey);

            if (priceHoriz == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPriceHorizException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(priceHoriz);
        } catch (NoSuchPriceHorizException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected PriceHoriz removeImpl(PriceHoriz priceHoriz)
        throws SystemException {
        priceHoriz = toUnwrappedModel(priceHoriz);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(priceHoriz)) {
                priceHoriz = (PriceHoriz) session.get(PriceHorizImpl.class,
                        priceHoriz.getPrimaryKeyObj());
            }

            if (priceHoriz != null) {
                session.delete(priceHoriz);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (priceHoriz != null) {
            clearCache(priceHoriz);
        }

        return priceHoriz;
    }

    @Override
    public PriceHoriz updateImpl(
        com.energyindex.liferay.sb.model.PriceHoriz priceHoriz)
        throws SystemException {
        priceHoriz = toUnwrappedModel(priceHoriz);

        boolean isNew = priceHoriz.isNew();

        PriceHorizModelImpl priceHorizModelImpl = (PriceHorizModelImpl) priceHoriz;

        Session session = null;

        try {
            session = openSession();

            if (priceHoriz.isNew()) {
                session.save(priceHoriz);

                priceHoriz.setNew(false);
            } else {
                session.merge(priceHoriz);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !PriceHorizModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((priceHorizModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHPRODUCT_ID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        priceHorizModelImpl.getOriginalProduct_id()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRICEHPRODUCT_ID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHPRODUCT_ID,
                    args);

                args = new Object[] { priceHorizModelImpl.getProduct_id() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRICEHPRODUCT_ID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHPRODUCT_ID,
                    args);
            }

            if ((priceHorizModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHP_ID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        priceHorizModelImpl.getOriginalPort_id()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRICEHP_ID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHP_ID,
                    args);

                args = new Object[] { priceHorizModelImpl.getPort_id() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRICEHP_ID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRICEHP_ID,
                    args);
            }
        }

        EntityCacheUtil.putResult(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
            PriceHorizImpl.class, priceHoriz.getPrimaryKey(), priceHoriz);

        return priceHoriz;
    }

    protected PriceHoriz toUnwrappedModel(PriceHoriz priceHoriz) {
        if (priceHoriz instanceof PriceHorizImpl) {
            return priceHoriz;
        }

        PriceHorizImpl priceHorizImpl = new PriceHorizImpl();

        priceHorizImpl.setNew(priceHoriz.isNew());
        priceHorizImpl.setPrimaryKey(priceHoriz.getPrimaryKey());

        priceHorizImpl.setPriceH_id(priceHoriz.getPriceH_id());
        priceHorizImpl.setGroupId(priceHoriz.getGroupId());
        priceHorizImpl.setCompanyId(priceHoriz.getCompanyId());
        priceHorizImpl.setUserId(priceHoriz.getUserId());
        priceHorizImpl.setUserName(priceHoriz.getUserName());
        priceHorizImpl.setCreateDate(priceHoriz.getCreateDate());
        priceHorizImpl.setModifiedDate(priceHoriz.getModifiedDate());
        priceHorizImpl.setPrice_date(priceHoriz.getPrice_date());
        priceHorizImpl.setProduct_id(priceHoriz.getProduct_id());
        priceHorizImpl.setPrice_low(priceHoriz.getPrice_low());
        priceHorizImpl.setPrice_high(priceHoriz.getPrice_high());
        priceHorizImpl.setPrice_close(priceHoriz.getPrice_close());
        priceHorizImpl.setPrice_change(priceHoriz.getPrice_change());
        priceHorizImpl.setPort_id(priceHoriz.getPort_id());
        priceHorizImpl.setUnit_id(priceHoriz.getUnit_id());
        priceHorizImpl.setCurrency_id(priceHoriz.getCurrency_id());
        priceHorizImpl.setContracttype_id(priceHoriz.getContracttype_id());
        priceHorizImpl.setPublish_id(priceHoriz.getPublish_id());
        priceHorizImpl.setPrice_id(priceHoriz.getPrice_id());

        return priceHorizImpl;
    }

    /**
     * Returns the price horiz with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the price horiz
     * @return the price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz findByPrimaryKey(Serializable primaryKey)
        throws NoSuchPriceHorizException, SystemException {
        PriceHoriz priceHoriz = fetchByPrimaryKey(primaryKey);

        if (priceHoriz == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchPriceHorizException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return priceHoriz;
    }

    /**
     * Returns the price horiz with the primary key or throws a {@link com.energyindex.liferay.sb.NoSuchPriceHorizException} if it could not be found.
     *
     * @param priceHorizPK the primary key of the price horiz
     * @return the price horiz
     * @throws com.energyindex.liferay.sb.NoSuchPriceHorizException if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz findByPrimaryKey(PriceHorizPK priceHorizPK)
        throws NoSuchPriceHorizException, SystemException {
        return findByPrimaryKey((Serializable) priceHorizPK);
    }

    /**
     * Returns the price horiz with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the price horiz
     * @return the price horiz, or <code>null</code> if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        PriceHoriz priceHoriz = (PriceHoriz) EntityCacheUtil.getResult(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
                PriceHorizImpl.class, primaryKey);

        if (priceHoriz == _nullPriceHoriz) {
            return null;
        }

        if (priceHoriz == null) {
            Session session = null;

            try {
                session = openSession();

                priceHoriz = (PriceHoriz) session.get(PriceHorizImpl.class,
                        primaryKey);

                if (priceHoriz != null) {
                    cacheResult(priceHoriz);
                } else {
                    EntityCacheUtil.putResult(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
                        PriceHorizImpl.class, primaryKey, _nullPriceHoriz);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(PriceHorizModelImpl.ENTITY_CACHE_ENABLED,
                    PriceHorizImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return priceHoriz;
    }

    /**
     * Returns the price horiz with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param priceHorizPK the primary key of the price horiz
     * @return the price horiz, or <code>null</code> if a price horiz with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceHoriz fetchByPrimaryKey(PriceHorizPK priceHorizPK)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) priceHorizPK);
    }

    /**
     * Returns all the price horizs.
     *
     * @return the price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price horizs.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @return the range of price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the price horizs.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceHorizModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of price horizs
     * @param end the upper bound of the range of price horizs (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceHoriz> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<PriceHoriz> list = (List<PriceHoriz>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PRICEHORIZ);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PRICEHORIZ;

                if (pagination) {
                    sql = sql.concat(PriceHorizModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceHoriz>(list);
                } else {
                    list = (List<PriceHoriz>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the price horizs from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (PriceHoriz priceHoriz : findAll()) {
            remove(priceHoriz);
        }
    }

    /**
     * Returns the number of price horizs.
     *
     * @return the number of price horizs
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PRICEHORIZ);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the price horiz persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.energyindex.liferay.sb.model.PriceHoriz")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<PriceHoriz>> listenersList = new ArrayList<ModelListener<PriceHoriz>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<PriceHoriz>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PriceHorizImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
