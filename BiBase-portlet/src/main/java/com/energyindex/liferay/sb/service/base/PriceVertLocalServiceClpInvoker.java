package com.energyindex.liferay.sb.service.base;

import com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil;

import java.util.Arrays;

/**
 * @author InfinityFrame : Jason Gonin
 * @generated
 */
public class PriceVertLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName64;
    private String[] _methodParameterTypes64;
    private String _methodName65;
    private String[] _methodParameterTypes65;
    private String _methodName70;
    private String[] _methodParameterTypes70;
    private String _methodName71;
    private String[] _methodParameterTypes71;
    private String _methodName72;
    private String[] _methodParameterTypes72;
    private String _methodName73;
    private String[] _methodParameterTypes73;
    private String _methodName74;
    private String[] _methodParameterTypes74;
    private String _methodName75;
    private String[] _methodParameterTypes75;
    private String _methodName76;
    private String[] _methodParameterTypes76;
    private String _methodName77;
    private String[] _methodParameterTypes77;
    private String _methodName78;
    private String[] _methodParameterTypes78;
    private String _methodName79;
    private String[] _methodParameterTypes79;

    public PriceVertLocalServiceClpInvoker() {
        _methodName0 = "addPriceVert";

        _methodParameterTypes0 = new String[] {
                "com.energyindex.liferay.sb.model.PriceVert"
            };

        _methodName1 = "createPriceVert";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deletePriceVert";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deletePriceVert";

        _methodParameterTypes3 = new String[] {
                "com.energyindex.liferay.sb.model.PriceVert"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchPriceVert";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getPriceVert";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getPriceVerts";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getPriceVertsCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updatePriceVert";

        _methodParameterTypes15 = new String[] {
                "com.energyindex.liferay.sb.model.PriceVert"
            };

        _methodName64 = "getBeanIdentifier";

        _methodParameterTypes64 = new String[] {  };

        _methodName65 = "setBeanIdentifier";

        _methodParameterTypes65 = new String[] { "java.lang.String" };

        _methodName70 = "addPriceVert";

        _methodParameterTypes70 = new String[] {
                "long", "long", "long", "int", "int", "int", "int", "int",
                "long", "com.liferay.portal.service.ServiceContext"
            };

        _methodName71 = "addPriceVertResources";

        _methodParameterTypes71 = new String[] {
                "com.energyindex.liferay.sb.model.PriceVert",
                "java.lang.String[][]", "java.lang.String[][]"
            };

        _methodName72 = "addPriceVertResources";

        _methodParameterTypes72 = new String[] {
                "com.energyindex.liferay.sb.model.PriceVert", "boolean",
                "boolean"
            };

        _methodName73 = "deletePriceVert";

        _methodParameterTypes73 = new String[] {
                "com.energyindex.liferay.sb.model.PriceVert"
            };

        _methodName74 = "deletePriceVert";

        _methodParameterTypes74 = new String[] { "long" };

        _methodName75 = "getPriceVert";

        _methodParameterTypes75 = new String[] { "long" };

        _methodName76 = "getPriceVertsByPortId";

        _methodParameterTypes76 = new String[] { "int" };

        _methodName77 = "getPriceVertsByPortId";

        _methodParameterTypes77 = new String[] { "int", "int", "int" };

        _methodName78 = "getPriceVertsCountByPortId";

        _methodParameterTypes78 = new String[] { "int" };

        _methodName79 = "updatePriceVert";

        _methodParameterTypes79 = new String[] {
                "int", "long", "long", "long", "int", "int", "int", "int", "int",
                "long", "com.liferay.portal.service.ServiceContext"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return PriceVertLocalServiceUtil.addPriceVert((com.energyindex.liferay.sb.model.PriceVert) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return PriceVertLocalServiceUtil.createPriceVert(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return PriceVertLocalServiceUtil.deletePriceVert(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return PriceVertLocalServiceUtil.deletePriceVert((com.energyindex.liferay.sb.model.PriceVert) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return PriceVertLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return PriceVertLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return PriceVertLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return PriceVertLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return PriceVertLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return PriceVertLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return PriceVertLocalServiceUtil.fetchPriceVert(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return PriceVertLocalServiceUtil.getPriceVert(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return PriceVertLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return PriceVertLocalServiceUtil.getPriceVerts(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return PriceVertLocalServiceUtil.getPriceVertsCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return PriceVertLocalServiceUtil.updatePriceVert((com.energyindex.liferay.sb.model.PriceVert) arguments[0]);
        }

        if (_methodName64.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes64, parameterTypes)) {
            return PriceVertLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName65.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes65, parameterTypes)) {
            PriceVertLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName70.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes70, parameterTypes)) {
            return PriceVertLocalServiceUtil.addPriceVert(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                ((Integer) arguments[3]).intValue(),
                ((Integer) arguments[4]).intValue(),
                ((Integer) arguments[5]).intValue(),
                ((Integer) arguments[6]).intValue(),
                ((Integer) arguments[7]).intValue(),
                ((Long) arguments[8]).longValue(),
                (com.liferay.portal.service.ServiceContext) arguments[9]);
        }

        if (_methodName71.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes71, parameterTypes)) {
            PriceVertLocalServiceUtil.addPriceVertResources((com.energyindex.liferay.sb.model.PriceVert) arguments[0],
                (java.lang.String[]) arguments[1],
                (java.lang.String[]) arguments[2]);

            return null;
        }

        if (_methodName72.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes72, parameterTypes)) {
            PriceVertLocalServiceUtil.addPriceVertResources((com.energyindex.liferay.sb.model.PriceVert) arguments[0],
                ((Boolean) arguments[1]).booleanValue(),
                ((Boolean) arguments[2]).booleanValue());

            return null;
        }

        if (_methodName73.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes73, parameterTypes)) {
            return PriceVertLocalServiceUtil.deletePriceVert((com.energyindex.liferay.sb.model.PriceVert) arguments[0]);
        }

        if (_methodName74.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes74, parameterTypes)) {
            return PriceVertLocalServiceUtil.deletePriceVert(((Long) arguments[0]).longValue());
        }

        if (_methodName75.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes75, parameterTypes)) {
            return PriceVertLocalServiceUtil.getPriceVert(((Long) arguments[0]).longValue());
        }

        if (_methodName76.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes76, parameterTypes)) {
            return PriceVertLocalServiceUtil.getPriceVertsByPortId(((Integer) arguments[0]).intValue());
        }

        if (_methodName77.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes77, parameterTypes)) {
            return PriceVertLocalServiceUtil.getPriceVertsByPortId(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName78.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes78, parameterTypes)) {
            return PriceVertLocalServiceUtil.getPriceVertsCountByPortId(((Integer) arguments[0]).intValue());
        }

        if (_methodName79.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes79, parameterTypes)) {
            return PriceVertLocalServiceUtil.updatePriceVert(((Integer) arguments[0]).intValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                ((Long) arguments[3]).longValue(),
                ((Integer) arguments[4]).intValue(),
                ((Integer) arguments[5]).intValue(),
                ((Integer) arguments[6]).intValue(),
                ((Integer) arguments[7]).intValue(),
                ((Integer) arguments[8]).intValue(),
                ((Long) arguments[9]).longValue(),
                (com.liferay.portal.service.ServiceContext) arguments[10]);
        }

        throw new UnsupportedOperationException();
    }
}
