package com.energyindex.liferay.sb.service.impl;

import com.energyindex.liferay.sb.service.base.PriceHorizLocalServiceBaseImpl;

/**
 * The implementation of the price horiz local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.service.PriceHorizLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.base.PriceHorizLocalServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.PriceHorizLocalServiceUtil
 */
public class PriceHorizLocalServiceImpl extends PriceHorizLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.energyindex.liferay.sb.service.PriceHorizLocalServiceUtil} to access the price horiz local service.
     */
}
