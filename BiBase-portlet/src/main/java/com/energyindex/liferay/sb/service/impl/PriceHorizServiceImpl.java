package com.energyindex.liferay.sb.service.impl;

import com.energyindex.liferay.sb.service.base.PriceHorizServiceBaseImpl;

/**
 * The implementation of the price horiz remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.service.PriceHorizService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.base.PriceHorizServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.PriceHorizServiceUtil
 */
public class PriceHorizServiceImpl extends PriceHorizServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.energyindex.liferay.sb.service.PriceHorizServiceUtil} to access the price horiz remote service.
     */
}
