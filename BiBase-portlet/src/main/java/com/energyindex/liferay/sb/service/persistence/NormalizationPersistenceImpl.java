package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.NoSuchNormalizationException;
import com.energyindex.liferay.sb.model.Normalization;
import com.energyindex.liferay.sb.model.impl.NormalizationImpl;
import com.energyindex.liferay.sb.model.impl.NormalizationModelImpl;
import com.energyindex.liferay.sb.service.persistence.NormalizationPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the normalization service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see NormalizationPersistence
 * @see NormalizationUtil
 * @generated
 */
public class NormalizationPersistenceImpl extends BasePersistenceImpl<Normalization>
    implements NormalizationPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link NormalizationUtil} to access the normalization persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = NormalizationImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationModelImpl.FINDER_CACHE_ENABLED,
            NormalizationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationModelImpl.FINDER_CACHE_ENABLED,
            NormalizationImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NORMCOLLECTION =
        new FinderPath(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationModelImpl.FINDER_CACHE_ENABLED,
            NormalizationImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByNormCollection",
            new String[] {
                String.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NORMCOLLECTION =
        new FinderPath(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationModelImpl.FINDER_CACHE_ENABLED,
            NormalizationImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByNormCollection", new String[] { String.class.getName() },
            NormalizationModelImpl.NORMTYPE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_NORMCOLLECTION = new FinderPath(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByNormCollection",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_1 = "normalization.normType IS NULL";
    private static final String _FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_2 = "normalization.normType = ?";
    private static final String _FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_3 = "(normalization.normType IS NULL OR normalization.normType = '')";
    private static final String _SQL_SELECT_NORMALIZATION = "SELECT normalization FROM Normalization normalization";
    private static final String _SQL_SELECT_NORMALIZATION_WHERE = "SELECT normalization FROM Normalization normalization WHERE ";
    private static final String _SQL_COUNT_NORMALIZATION = "SELECT COUNT(normalization) FROM Normalization normalization";
    private static final String _SQL_COUNT_NORMALIZATION_WHERE = "SELECT COUNT(normalization) FROM Normalization normalization WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "normalization.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Normalization exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Normalization exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(NormalizationPersistenceImpl.class);
    private static Normalization _nullNormalization = new NormalizationImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Normalization> toCacheModel() {
                return _nullNormalizationCacheModel;
            }
        };

    private static CacheModel<Normalization> _nullNormalizationCacheModel = new CacheModel<Normalization>() {
            @Override
            public Normalization toEntityModel() {
                return _nullNormalization;
            }
        };

    public NormalizationPersistenceImpl() {
        setModelClass(Normalization.class);
    }

    /**
     * Returns all the normalizations where normType = &#63;.
     *
     * @param normType the norm type
     * @return the matching normalizations
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Normalization> findByNormCollection(String normType)
        throws SystemException {
        return findByNormCollection(normType, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the normalizations where normType = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param normType the norm type
     * @param start the lower bound of the range of normalizations
     * @param end the upper bound of the range of normalizations (not inclusive)
     * @return the range of matching normalizations
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Normalization> findByNormCollection(String normType, int start,
        int end) throws SystemException {
        return findByNormCollection(normType, start, end, null);
    }

    /**
     * Returns an ordered range of all the normalizations where normType = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param normType the norm type
     * @param start the lower bound of the range of normalizations
     * @param end the upper bound of the range of normalizations (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching normalizations
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Normalization> findByNormCollection(String normType, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NORMCOLLECTION;
            finderArgs = new Object[] { normType };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NORMCOLLECTION;
            finderArgs = new Object[] { normType, start, end, orderByComparator };
        }

        List<Normalization> list = (List<Normalization>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Normalization normalization : list) {
                if (!Validator.equals(normType, normalization.getNormType())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_NORMALIZATION_WHERE);

            boolean bindNormType = false;

            if (normType == null) {
                query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_1);
            } else if (normType.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_3);
            } else {
                bindNormType = true;

                query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(NormalizationModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindNormType) {
                    qPos.add(normType);
                }

                if (!pagination) {
                    list = (List<Normalization>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Normalization>(list);
                } else {
                    list = (List<Normalization>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first normalization in the ordered set where normType = &#63;.
     *
     * @param normType the norm type
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching normalization
     * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a matching normalization could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization findByNormCollection_First(String normType,
        OrderByComparator orderByComparator)
        throws NoSuchNormalizationException, SystemException {
        Normalization normalization = fetchByNormCollection_First(normType,
                orderByComparator);

        if (normalization != null) {
            return normalization;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("normType=");
        msg.append(normType);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchNormalizationException(msg.toString());
    }

    /**
     * Returns the first normalization in the ordered set where normType = &#63;.
     *
     * @param normType the norm type
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching normalization, or <code>null</code> if a matching normalization could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization fetchByNormCollection_First(String normType,
        OrderByComparator orderByComparator) throws SystemException {
        List<Normalization> list = findByNormCollection(normType, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last normalization in the ordered set where normType = &#63;.
     *
     * @param normType the norm type
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching normalization
     * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a matching normalization could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization findByNormCollection_Last(String normType,
        OrderByComparator orderByComparator)
        throws NoSuchNormalizationException, SystemException {
        Normalization normalization = fetchByNormCollection_Last(normType,
                orderByComparator);

        if (normalization != null) {
            return normalization;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("normType=");
        msg.append(normType);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchNormalizationException(msg.toString());
    }

    /**
     * Returns the last normalization in the ordered set where normType = &#63;.
     *
     * @param normType the norm type
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching normalization, or <code>null</code> if a matching normalization could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization fetchByNormCollection_Last(String normType,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByNormCollection(normType);

        if (count == 0) {
            return null;
        }

        List<Normalization> list = findByNormCollection(normType, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the normalizations before and after the current normalization in the ordered set where normType = &#63;.
     *
     * @param normId the primary key of the current normalization
     * @param normType the norm type
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next normalization
     * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a normalization with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization[] findByNormCollection_PrevAndNext(int normId,
        String normType, OrderByComparator orderByComparator)
        throws NoSuchNormalizationException, SystemException {
        Normalization normalization = findByPrimaryKey(normId);

        Session session = null;

        try {
            session = openSession();

            Normalization[] array = new NormalizationImpl[3];

            array[0] = getByNormCollection_PrevAndNext(session, normalization,
                    normType, orderByComparator, true);

            array[1] = normalization;

            array[2] = getByNormCollection_PrevAndNext(session, normalization,
                    normType, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Normalization getByNormCollection_PrevAndNext(Session session,
        Normalization normalization, String normType,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_NORMALIZATION_WHERE);

        boolean bindNormType = false;

        if (normType == null) {
            query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_1);
        } else if (normType.equals(StringPool.BLANK)) {
            query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_3);
        } else {
            bindNormType = true;

            query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(NormalizationModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindNormType) {
            qPos.add(normType);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(normalization);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Normalization> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the normalizations where normType = &#63; from the database.
     *
     * @param normType the norm type
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByNormCollection(String normType)
        throws SystemException {
        for (Normalization normalization : findByNormCollection(normType,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(normalization);
        }
    }

    /**
     * Returns the number of normalizations where normType = &#63;.
     *
     * @param normType the norm type
     * @return the number of matching normalizations
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByNormCollection(String normType) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_NORMCOLLECTION;

        Object[] finderArgs = new Object[] { normType };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_NORMALIZATION_WHERE);

            boolean bindNormType = false;

            if (normType == null) {
                query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_1);
            } else if (normType.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_3);
            } else {
                bindNormType = true;

                query.append(_FINDER_COLUMN_NORMCOLLECTION_NORMTYPE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindNormType) {
                    qPos.add(normType);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the normalization in the entity cache if it is enabled.
     *
     * @param normalization the normalization
     */
    @Override
    public void cacheResult(Normalization normalization) {
        EntityCacheUtil.putResult(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationImpl.class, normalization.getPrimaryKey(),
            normalization);

        normalization.resetOriginalValues();
    }

    /**
     * Caches the normalizations in the entity cache if it is enabled.
     *
     * @param normalizations the normalizations
     */
    @Override
    public void cacheResult(List<Normalization> normalizations) {
        for (Normalization normalization : normalizations) {
            if (EntityCacheUtil.getResult(
                        NormalizationModelImpl.ENTITY_CACHE_ENABLED,
                        NormalizationImpl.class, normalization.getPrimaryKey()) == null) {
                cacheResult(normalization);
            } else {
                normalization.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all normalizations.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(NormalizationImpl.class.getName());
        }

        EntityCacheUtil.clearCache(NormalizationImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the normalization.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Normalization normalization) {
        EntityCacheUtil.removeResult(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationImpl.class, normalization.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Normalization> normalizations) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Normalization normalization : normalizations) {
            EntityCacheUtil.removeResult(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
                NormalizationImpl.class, normalization.getPrimaryKey());
        }
    }

    /**
     * Creates a new normalization with the primary key. Does not add the normalization to the database.
     *
     * @param normId the primary key for the new normalization
     * @return the new normalization
     */
    @Override
    public Normalization create(int normId) {
        Normalization normalization = new NormalizationImpl();

        normalization.setNew(true);
        normalization.setPrimaryKey(normId);

        return normalization;
    }

    /**
     * Removes the normalization with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param normId the primary key of the normalization
     * @return the normalization that was removed
     * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a normalization with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization remove(int normId)
        throws NoSuchNormalizationException, SystemException {
        return remove((Serializable) normId);
    }

    /**
     * Removes the normalization with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the normalization
     * @return the normalization that was removed
     * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a normalization with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization remove(Serializable primaryKey)
        throws NoSuchNormalizationException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Normalization normalization = (Normalization) session.get(NormalizationImpl.class,
                    primaryKey);

            if (normalization == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchNormalizationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(normalization);
        } catch (NoSuchNormalizationException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Normalization removeImpl(Normalization normalization)
        throws SystemException {
        normalization = toUnwrappedModel(normalization);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(normalization)) {
                normalization = (Normalization) session.get(NormalizationImpl.class,
                        normalization.getPrimaryKeyObj());
            }

            if (normalization != null) {
                session.delete(normalization);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (normalization != null) {
            clearCache(normalization);
        }

        return normalization;
    }

    @Override
    public Normalization updateImpl(
        com.energyindex.liferay.sb.model.Normalization normalization)
        throws SystemException {
        normalization = toUnwrappedModel(normalization);

        boolean isNew = normalization.isNew();

        NormalizationModelImpl normalizationModelImpl = (NormalizationModelImpl) normalization;

        Session session = null;

        try {
            session = openSession();

            if (normalization.isNew()) {
                session.save(normalization);

                normalization.setNew(false);
            } else {
                session.merge(normalization);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !NormalizationModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((normalizationModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NORMCOLLECTION.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        normalizationModelImpl.getOriginalNormType()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NORMCOLLECTION,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NORMCOLLECTION,
                    args);

                args = new Object[] { normalizationModelImpl.getNormType() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NORMCOLLECTION,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NORMCOLLECTION,
                    args);
            }
        }

        EntityCacheUtil.putResult(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
            NormalizationImpl.class, normalization.getPrimaryKey(),
            normalization);

        return normalization;
    }

    protected Normalization toUnwrappedModel(Normalization normalization) {
        if (normalization instanceof NormalizationImpl) {
            return normalization;
        }

        NormalizationImpl normalizationImpl = new NormalizationImpl();

        normalizationImpl.setNew(normalization.isNew());
        normalizationImpl.setPrimaryKey(normalization.getPrimaryKey());

        normalizationImpl.setNormId(normalization.getNormId());
        normalizationImpl.setGroupId(normalization.getGroupId());
        normalizationImpl.setCompanyId(normalization.getCompanyId());
        normalizationImpl.setUserId(normalization.getUserId());
        normalizationImpl.setUserName(normalization.getUserName());
        normalizationImpl.setCreateDate(normalization.getCreateDate());
        normalizationImpl.setModifiedDate(normalization.getModifiedDate());
        normalizationImpl.setSearchExpr(normalization.getSearchExpr());
        normalizationImpl.setNormType(normalization.getNormType());
        normalizationImpl.setLongDesc(normalization.getLongDesc());
        normalizationImpl.setShortDesc(normalization.getShortDesc());
        normalizationImpl.setIsPrefix(normalization.getIsPrefix());
        normalizationImpl.setIsPostfix(normalization.getIsPostfix());

        return normalizationImpl;
    }

    /**
     * Returns the normalization with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the normalization
     * @return the normalization
     * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a normalization with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization findByPrimaryKey(Serializable primaryKey)
        throws NoSuchNormalizationException, SystemException {
        Normalization normalization = fetchByPrimaryKey(primaryKey);

        if (normalization == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchNormalizationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return normalization;
    }

    /**
     * Returns the normalization with the primary key or throws a {@link com.energyindex.liferay.sb.NoSuchNormalizationException} if it could not be found.
     *
     * @param normId the primary key of the normalization
     * @return the normalization
     * @throws com.energyindex.liferay.sb.NoSuchNormalizationException if a normalization with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization findByPrimaryKey(int normId)
        throws NoSuchNormalizationException, SystemException {
        return findByPrimaryKey((Serializable) normId);
    }

    /**
     * Returns the normalization with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the normalization
     * @return the normalization, or <code>null</code> if a normalization with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Normalization normalization = (Normalization) EntityCacheUtil.getResult(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
                NormalizationImpl.class, primaryKey);

        if (normalization == _nullNormalization) {
            return null;
        }

        if (normalization == null) {
            Session session = null;

            try {
                session = openSession();

                normalization = (Normalization) session.get(NormalizationImpl.class,
                        primaryKey);

                if (normalization != null) {
                    cacheResult(normalization);
                } else {
                    EntityCacheUtil.putResult(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
                        NormalizationImpl.class, primaryKey, _nullNormalization);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(NormalizationModelImpl.ENTITY_CACHE_ENABLED,
                    NormalizationImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return normalization;
    }

    /**
     * Returns the normalization with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param normId the primary key of the normalization
     * @return the normalization, or <code>null</code> if a normalization with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Normalization fetchByPrimaryKey(int normId)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) normId);
    }

    /**
     * Returns all the normalizations.
     *
     * @return the normalizations
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Normalization> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the normalizations.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of normalizations
     * @param end the upper bound of the range of normalizations (not inclusive)
     * @return the range of normalizations
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Normalization> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the normalizations.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.NormalizationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of normalizations
     * @param end the upper bound of the range of normalizations (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of normalizations
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Normalization> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Normalization> list = (List<Normalization>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_NORMALIZATION);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_NORMALIZATION;

                if (pagination) {
                    sql = sql.concat(NormalizationModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Normalization>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Normalization>(list);
                } else {
                    list = (List<Normalization>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the normalizations from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Normalization normalization : findAll()) {
            remove(normalization);
        }
    }

    /**
     * Returns the number of normalizations.
     *
     * @return the number of normalizations
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_NORMALIZATION);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the normalization persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.energyindex.liferay.sb.model.Normalization")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Normalization>> listenersList = new ArrayList<ModelListener<Normalization>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Normalization>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(NormalizationImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
