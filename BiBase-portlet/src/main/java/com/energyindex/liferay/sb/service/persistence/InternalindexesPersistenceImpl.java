package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.NoSuchInternalindexesException;
import com.energyindex.liferay.sb.model.Internalindexes;
import com.energyindex.liferay.sb.model.impl.InternalindexesImpl;
import com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl;
import com.energyindex.liferay.sb.service.persistence.InternalindexesPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the internalindexes service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see InternalindexesPersistence
 * @see InternalindexesUtil
 * @generated
 */
public class InternalindexesPersistenceImpl extends BasePersistenceImpl<Internalindexes>
    implements InternalindexesPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link InternalindexesUtil} to access the internalindexes persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = InternalindexesImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesModelImpl.FINDER_CACHE_ENABLED,
            InternalindexesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesModelImpl.FINDER_CACHE_ENABLED,
            InternalindexesImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDPRODUCT =
        new FinderPath(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesModelImpl.FINDER_CACHE_ENABLED,
            InternalindexesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByfindProduct",
            new String[] {
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDPRODUCT =
        new FinderPath(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesModelImpl.FINDER_CACHE_ENABLED,
            InternalindexesImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByfindProduct",
            new String[] { Integer.class.getName() },
            InternalindexesModelImpl.PRODUCT_ID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FINDPRODUCT = new FinderPath(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByfindProduct",
            new String[] { Integer.class.getName() });
    private static final String _FINDER_COLUMN_FINDPRODUCT_PRODUCT_ID_2 = "internalindexes.product_id = ?";
    private static final String _SQL_SELECT_INTERNALINDEXES = "SELECT internalindexes FROM Internalindexes internalindexes";
    private static final String _SQL_SELECT_INTERNALINDEXES_WHERE = "SELECT internalindexes FROM Internalindexes internalindexes WHERE ";
    private static final String _SQL_COUNT_INTERNALINDEXES = "SELECT COUNT(internalindexes) FROM Internalindexes internalindexes";
    private static final String _SQL_COUNT_INTERNALINDEXES_WHERE = "SELECT COUNT(internalindexes) FROM Internalindexes internalindexes WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "internalindexes.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Internalindexes exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Internalindexes exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(InternalindexesPersistenceImpl.class);
    private static Internalindexes _nullInternalindexes = new InternalindexesImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Internalindexes> toCacheModel() {
                return _nullInternalindexesCacheModel;
            }
        };

    private static CacheModel<Internalindexes> _nullInternalindexesCacheModel = new CacheModel<Internalindexes>() {
            @Override
            public Internalindexes toEntityModel() {
                return _nullInternalindexes;
            }
        };

    public InternalindexesPersistenceImpl() {
        setModelClass(Internalindexes.class);
    }

    /**
     * Returns all the internalindexeses where product_id = &#63;.
     *
     * @param product_id the product_id
     * @return the matching internalindexeses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Internalindexes> findByfindProduct(int product_id)
        throws SystemException {
        return findByfindProduct(product_id, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the internalindexeses where product_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param product_id the product_id
     * @param start the lower bound of the range of internalindexeses
     * @param end the upper bound of the range of internalindexeses (not inclusive)
     * @return the range of matching internalindexeses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Internalindexes> findByfindProduct(int product_id, int start,
        int end) throws SystemException {
        return findByfindProduct(product_id, start, end, null);
    }

    /**
     * Returns an ordered range of all the internalindexeses where product_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param product_id the product_id
     * @param start the lower bound of the range of internalindexeses
     * @param end the upper bound of the range of internalindexeses (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching internalindexeses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Internalindexes> findByfindProduct(int product_id, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDPRODUCT;
            finderArgs = new Object[] { product_id };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDPRODUCT;
            finderArgs = new Object[] { product_id, start, end, orderByComparator };
        }

        List<Internalindexes> list = (List<Internalindexes>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Internalindexes internalindexes : list) {
                if ((product_id != internalindexes.getProduct_id())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_INTERNALINDEXES_WHERE);

            query.append(_FINDER_COLUMN_FINDPRODUCT_PRODUCT_ID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(InternalindexesModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(product_id);

                if (!pagination) {
                    list = (List<Internalindexes>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Internalindexes>(list);
                } else {
                    list = (List<Internalindexes>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first internalindexes in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching internalindexes
     * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a matching internalindexes could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes findByfindProduct_First(int product_id,
        OrderByComparator orderByComparator)
        throws NoSuchInternalindexesException, SystemException {
        Internalindexes internalindexes = fetchByfindProduct_First(product_id,
                orderByComparator);

        if (internalindexes != null) {
            return internalindexes;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("product_id=");
        msg.append(product_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchInternalindexesException(msg.toString());
    }

    /**
     * Returns the first internalindexes in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching internalindexes, or <code>null</code> if a matching internalindexes could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes fetchByfindProduct_First(int product_id,
        OrderByComparator orderByComparator) throws SystemException {
        List<Internalindexes> list = findByfindProduct(product_id, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last internalindexes in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching internalindexes
     * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a matching internalindexes could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes findByfindProduct_Last(int product_id,
        OrderByComparator orderByComparator)
        throws NoSuchInternalindexesException, SystemException {
        Internalindexes internalindexes = fetchByfindProduct_Last(product_id,
                orderByComparator);

        if (internalindexes != null) {
            return internalindexes;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("product_id=");
        msg.append(product_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchInternalindexesException(msg.toString());
    }

    /**
     * Returns the last internalindexes in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching internalindexes, or <code>null</code> if a matching internalindexes could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes fetchByfindProduct_Last(int product_id,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByfindProduct(product_id);

        if (count == 0) {
            return null;
        }

        List<Internalindexes> list = findByfindProduct(product_id, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the internalindexeses before and after the current internalindexes in the ordered set where product_id = &#63;.
     *
     * @param price_id the primary key of the current internalindexes
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next internalindexes
     * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a internalindexes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes[] findByfindProduct_PrevAndNext(int price_id,
        int product_id, OrderByComparator orderByComparator)
        throws NoSuchInternalindexesException, SystemException {
        Internalindexes internalindexes = findByPrimaryKey(price_id);

        Session session = null;

        try {
            session = openSession();

            Internalindexes[] array = new InternalindexesImpl[3];

            array[0] = getByfindProduct_PrevAndNext(session, internalindexes,
                    product_id, orderByComparator, true);

            array[1] = internalindexes;

            array[2] = getByfindProduct_PrevAndNext(session, internalindexes,
                    product_id, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Internalindexes getByfindProduct_PrevAndNext(Session session,
        Internalindexes internalindexes, int product_id,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_INTERNALINDEXES_WHERE);

        query.append(_FINDER_COLUMN_FINDPRODUCT_PRODUCT_ID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(InternalindexesModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(product_id);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(internalindexes);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Internalindexes> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the internalindexeses where product_id = &#63; from the database.
     *
     * @param product_id the product_id
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByfindProduct(int product_id) throws SystemException {
        for (Internalindexes internalindexes : findByfindProduct(product_id,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(internalindexes);
        }
    }

    /**
     * Returns the number of internalindexeses where product_id = &#63;.
     *
     * @param product_id the product_id
     * @return the number of matching internalindexeses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByfindProduct(int product_id) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDPRODUCT;

        Object[] finderArgs = new Object[] { product_id };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_INTERNALINDEXES_WHERE);

            query.append(_FINDER_COLUMN_FINDPRODUCT_PRODUCT_ID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(product_id);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the internalindexes in the entity cache if it is enabled.
     *
     * @param internalindexes the internalindexes
     */
    @Override
    public void cacheResult(Internalindexes internalindexes) {
        EntityCacheUtil.putResult(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesImpl.class, internalindexes.getPrimaryKey(),
            internalindexes);

        internalindexes.resetOriginalValues();
    }

    /**
     * Caches the internalindexeses in the entity cache if it is enabled.
     *
     * @param internalindexeses the internalindexeses
     */
    @Override
    public void cacheResult(List<Internalindexes> internalindexeses) {
        for (Internalindexes internalindexes : internalindexeses) {
            if (EntityCacheUtil.getResult(
                        InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
                        InternalindexesImpl.class,
                        internalindexes.getPrimaryKey()) == null) {
                cacheResult(internalindexes);
            } else {
                internalindexes.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all internalindexeses.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(InternalindexesImpl.class.getName());
        }

        EntityCacheUtil.clearCache(InternalindexesImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the internalindexes.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Internalindexes internalindexes) {
        EntityCacheUtil.removeResult(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesImpl.class, internalindexes.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Internalindexes> internalindexeses) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Internalindexes internalindexes : internalindexeses) {
            EntityCacheUtil.removeResult(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
                InternalindexesImpl.class, internalindexes.getPrimaryKey());
        }
    }

    /**
     * Creates a new internalindexes with the primary key. Does not add the internalindexes to the database.
     *
     * @param price_id the primary key for the new internalindexes
     * @return the new internalindexes
     */
    @Override
    public Internalindexes create(int price_id) {
        Internalindexes internalindexes = new InternalindexesImpl();

        internalindexes.setNew(true);
        internalindexes.setPrimaryKey(price_id);

        return internalindexes;
    }

    /**
     * Removes the internalindexes with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param price_id the primary key of the internalindexes
     * @return the internalindexes that was removed
     * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a internalindexes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes remove(int price_id)
        throws NoSuchInternalindexesException, SystemException {
        return remove((Serializable) price_id);
    }

    /**
     * Removes the internalindexes with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the internalindexes
     * @return the internalindexes that was removed
     * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a internalindexes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes remove(Serializable primaryKey)
        throws NoSuchInternalindexesException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Internalindexes internalindexes = (Internalindexes) session.get(InternalindexesImpl.class,
                    primaryKey);

            if (internalindexes == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchInternalindexesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(internalindexes);
        } catch (NoSuchInternalindexesException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Internalindexes removeImpl(Internalindexes internalindexes)
        throws SystemException {
        internalindexes = toUnwrappedModel(internalindexes);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(internalindexes)) {
                internalindexes = (Internalindexes) session.get(InternalindexesImpl.class,
                        internalindexes.getPrimaryKeyObj());
            }

            if (internalindexes != null) {
                session.delete(internalindexes);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (internalindexes != null) {
            clearCache(internalindexes);
        }

        return internalindexes;
    }

    @Override
    public Internalindexes updateImpl(
        com.energyindex.liferay.sb.model.Internalindexes internalindexes)
        throws SystemException {
        internalindexes = toUnwrappedModel(internalindexes);

        boolean isNew = internalindexes.isNew();

        InternalindexesModelImpl internalindexesModelImpl = (InternalindexesModelImpl) internalindexes;

        Session session = null;

        try {
            session = openSession();

            if (internalindexes.isNew()) {
                session.save(internalindexes);

                internalindexes.setNew(false);
            } else {
                session.merge(internalindexes);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !InternalindexesModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((internalindexesModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDPRODUCT.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        internalindexesModelImpl.getOriginalProduct_id()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FINDPRODUCT,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDPRODUCT,
                    args);

                args = new Object[] { internalindexesModelImpl.getProduct_id() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FINDPRODUCT,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDPRODUCT,
                    args);
            }
        }

        EntityCacheUtil.putResult(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
            InternalindexesImpl.class, internalindexes.getPrimaryKey(),
            internalindexes);

        return internalindexes;
    }

    protected Internalindexes toUnwrappedModel(Internalindexes internalindexes) {
        if (internalindexes instanceof InternalindexesImpl) {
            return internalindexes;
        }

        InternalindexesImpl internalindexesImpl = new InternalindexesImpl();

        internalindexesImpl.setNew(internalindexes.isNew());
        internalindexesImpl.setPrimaryKey(internalindexes.getPrimaryKey());

        internalindexesImpl.setPrice_id(internalindexes.getPrice_id());
        internalindexesImpl.setGroupId(internalindexes.getGroupId());
        internalindexesImpl.setCompanyId(internalindexes.getCompanyId());
        internalindexesImpl.setUserId(internalindexes.getUserId());
        internalindexesImpl.setUserName(internalindexes.getUserName());
        internalindexesImpl.setCreateDate(internalindexes.getCreateDate());
        internalindexesImpl.setModifiedDate(internalindexes.getModifiedDate());
        internalindexesImpl.setProduct_id(internalindexes.getProduct_id());
        internalindexesImpl.setPrice_date(internalindexes.getPrice_date());
        internalindexesImpl.setMonth_id(internalindexes.getMonth_id());
        internalindexesImpl.setPrice_contractyear(internalindexes.getPrice_contractyear());
        internalindexesImpl.setPrice_low(internalindexes.getPrice_low());
        internalindexesImpl.setPrice_current(internalindexes.getPrice_current());
        internalindexesImpl.setPrice_high(internalindexes.getPrice_high());
        internalindexesImpl.setPrice_close(internalindexes.getPrice_close());
        internalindexesImpl.setPrice_unitquote(internalindexes.getPrice_unitquote());
        internalindexesImpl.setCurrency_id(internalindexes.getCurrency_id());
        internalindexesImpl.setUnit_id(internalindexes.getUnit_id());
        internalindexesImpl.setPrice_quantity(internalindexes.getPrice_quantity());
        internalindexesImpl.setDelivery_id(internalindexes.getDelivery_id());
        internalindexesImpl.setPriceindex_id(internalindexes.getPriceindex_id());
        internalindexesImpl.setCompany_id(internalindexes.getCompany_id());
        internalindexesImpl.setContracttype_id(internalindexes.getContracttype_id());
        internalindexesImpl.setCity_id(internalindexes.getCity_id());
        internalindexesImpl.setPort_id(internalindexes.getPort_id());
        internalindexesImpl.setAirport_id(internalindexes.getAirport_id());
        internalindexesImpl.setState_id(internalindexes.getState_id());
        internalindexesImpl.setSubregion_id(internalindexes.getSubregion_id());
        internalindexesImpl.setCountry_id(internalindexes.getCountry_id());
        internalindexesImpl.setPublish_id(internalindexes.getPublish_id());
        internalindexesImpl.setCurrent_id(internalindexes.getCurrent_id());
        internalindexesImpl.setPrice_highlight(internalindexes.getPrice_highlight());
        internalindexesImpl.setPrice_mainhighlight(internalindexes.getPrice_mainhighlight());
        internalindexesImpl.setPrice_unqouted(internalindexes.getPrice_unqouted());
        internalindexesImpl.setPrice_lastedit(internalindexes.getPrice_lastedit());
        internalindexesImpl.setPrice_dateadded(internalindexes.getPrice_dateadded());

        return internalindexesImpl;
    }

    /**
     * Returns the internalindexes with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the internalindexes
     * @return the internalindexes
     * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a internalindexes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes findByPrimaryKey(Serializable primaryKey)
        throws NoSuchInternalindexesException, SystemException {
        Internalindexes internalindexes = fetchByPrimaryKey(primaryKey);

        if (internalindexes == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchInternalindexesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return internalindexes;
    }

    /**
     * Returns the internalindexes with the primary key or throws a {@link com.energyindex.liferay.sb.NoSuchInternalindexesException} if it could not be found.
     *
     * @param price_id the primary key of the internalindexes
     * @return the internalindexes
     * @throws com.energyindex.liferay.sb.NoSuchInternalindexesException if a internalindexes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes findByPrimaryKey(int price_id)
        throws NoSuchInternalindexesException, SystemException {
        return findByPrimaryKey((Serializable) price_id);
    }

    /**
     * Returns the internalindexes with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the internalindexes
     * @return the internalindexes, or <code>null</code> if a internalindexes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Internalindexes internalindexes = (Internalindexes) EntityCacheUtil.getResult(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
                InternalindexesImpl.class, primaryKey);

        if (internalindexes == _nullInternalindexes) {
            return null;
        }

        if (internalindexes == null) {
            Session session = null;

            try {
                session = openSession();

                internalindexes = (Internalindexes) session.get(InternalindexesImpl.class,
                        primaryKey);

                if (internalindexes != null) {
                    cacheResult(internalindexes);
                } else {
                    EntityCacheUtil.putResult(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
                        InternalindexesImpl.class, primaryKey,
                        _nullInternalindexes);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(InternalindexesModelImpl.ENTITY_CACHE_ENABLED,
                    InternalindexesImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return internalindexes;
    }

    /**
     * Returns the internalindexes with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param price_id the primary key of the internalindexes
     * @return the internalindexes, or <code>null</code> if a internalindexes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Internalindexes fetchByPrimaryKey(int price_id)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) price_id);
    }

    /**
     * Returns all the internalindexeses.
     *
     * @return the internalindexeses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Internalindexes> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the internalindexeses.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of internalindexeses
     * @param end the upper bound of the range of internalindexeses (not inclusive)
     * @return the range of internalindexeses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Internalindexes> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the internalindexeses.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.InternalindexesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of internalindexeses
     * @param end the upper bound of the range of internalindexeses (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of internalindexeses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Internalindexes> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Internalindexes> list = (List<Internalindexes>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_INTERNALINDEXES);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_INTERNALINDEXES;

                if (pagination) {
                    sql = sql.concat(InternalindexesModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Internalindexes>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Internalindexes>(list);
                } else {
                    list = (List<Internalindexes>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the internalindexeses from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Internalindexes internalindexes : findAll()) {
            remove(internalindexes);
        }
    }

    /**
     * Returns the number of internalindexeses.
     *
     * @return the number of internalindexeses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_INTERNALINDEXES);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the internalindexes persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.energyindex.liferay.sb.model.Internalindexes")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Internalindexes>> listenersList = new ArrayList<ModelListener<Internalindexes>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Internalindexes>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(InternalindexesImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
