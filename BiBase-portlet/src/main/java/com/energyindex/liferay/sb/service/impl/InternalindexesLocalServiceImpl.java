package com.energyindex.liferay.sb.service.impl;

import com.energyindex.liferay.sb.service.base.InternalindexesLocalServiceBaseImpl;

/**
 * The implementation of the internalindexes local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.service.InternalindexesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.base.InternalindexesLocalServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.InternalindexesLocalServiceUtil
 */
public class InternalindexesLocalServiceImpl
    extends InternalindexesLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.energyindex.liferay.sb.service.InternalindexesLocalServiceUtil} to access the internalindexes local service.
     */
}
