package com.energyindex.liferay.sb.service.base;

import com.energyindex.liferay.sb.service.PriceVertServiceUtil;

import java.util.Arrays;

/**
 * @author InfinityFrame : Jason Gonin
 * @generated
 */
public class PriceVertServiceClpInvoker {
    private String _methodName48;
    private String[] _methodParameterTypes48;
    private String _methodName49;
    private String[] _methodParameterTypes49;
    private String _methodName54;
    private String[] _methodParameterTypes54;
    private String _methodName55;
    private String[] _methodParameterTypes55;
    private String _methodName56;
    private String[] _methodParameterTypes56;
    private String _methodName57;
    private String[] _methodParameterTypes57;

    public PriceVertServiceClpInvoker() {
        _methodName48 = "getBeanIdentifier";

        _methodParameterTypes48 = new String[] {  };

        _methodName49 = "setBeanIdentifier";

        _methodParameterTypes49 = new String[] { "java.lang.String" };

        _methodName54 = "addPriceVert";

        _methodParameterTypes54 = new String[] {
                "long", "long", "long", "int", "int", "int", "int", "int",
                "long", "com.liferay.portal.service.ServiceContext"
            };

        _methodName55 = "deletePriceVert";

        _methodParameterTypes55 = new String[] { "long" };

        _methodName56 = "getPriceVert";

        _methodParameterTypes56 = new String[] { "long" };

        _methodName57 = "updatePriceVert";

        _methodParameterTypes57 = new String[] {
                "int", "long", "long", "long", "int", "int", "int", "int", "int",
                "long", "com.liferay.portal.service.ServiceContext"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName48.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes48, parameterTypes)) {
            return PriceVertServiceUtil.getBeanIdentifier();
        }

        if (_methodName49.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes49, parameterTypes)) {
            PriceVertServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName54.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes54, parameterTypes)) {
            return PriceVertServiceUtil.addPriceVert(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                ((Integer) arguments[3]).intValue(),
                ((Integer) arguments[4]).intValue(),
                ((Integer) arguments[5]).intValue(),
                ((Integer) arguments[6]).intValue(),
                ((Integer) arguments[7]).intValue(),
                ((Long) arguments[8]).longValue(),
                (com.liferay.portal.service.ServiceContext) arguments[9]);
        }

        if (_methodName55.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes55, parameterTypes)) {
            return PriceVertServiceUtil.deletePriceVert(((Long) arguments[0]).longValue());
        }

        if (_methodName56.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes56, parameterTypes)) {
            return PriceVertServiceUtil.getPriceVert(((Long) arguments[0]).longValue());
        }

        if (_methodName57.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes57, parameterTypes)) {
            return PriceVertServiceUtil.updatePriceVert(((Integer) arguments[0]).intValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                ((Long) arguments[3]).longValue(),
                ((Integer) arguments[4]).intValue(),
                ((Integer) arguments[5]).intValue(),
                ((Integer) arguments[6]).intValue(),
                ((Integer) arguments[7]).intValue(),
                ((Integer) arguments[8]).intValue(),
                ((Long) arguments[9]).longValue(),
                (com.liferay.portal.service.ServiceContext) arguments[10]);
        }

        throw new UnsupportedOperationException();
    }
}
