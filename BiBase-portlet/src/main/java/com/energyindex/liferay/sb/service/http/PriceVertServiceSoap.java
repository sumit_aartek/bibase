package com.energyindex.liferay.sb.service.http;

import com.energyindex.liferay.sb.service.PriceVertServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link com.energyindex.liferay.sb.service.PriceVertServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link com.energyindex.liferay.sb.model.PriceVertSoap}.
 * If the method in the service utility returns a
 * {@link com.energyindex.liferay.sb.model.PriceVert}, that is translated to a
 * {@link com.energyindex.liferay.sb.model.PriceVertSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVertServiceHttp
 * @see com.energyindex.liferay.sb.model.PriceVertSoap
 * @see com.energyindex.liferay.sb.service.PriceVertServiceUtil
 * @generated
 */
public class PriceVertServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(PriceVertServiceSoap.class);

    public static com.energyindex.liferay.sb.model.PriceVertSoap addPriceVert(
        long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws RemoteException {
        try {
            com.energyindex.liferay.sb.model.PriceVert returnValue = PriceVertServiceUtil.addPriceVert(userId,
                    groupId, price_close, port_id, product_id, month, day,
                    year, portId, serviceContext);

            return com.energyindex.liferay.sb.model.PriceVertSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static com.energyindex.liferay.sb.model.PriceVertSoap deletePriceVert(
        long price_id) throws RemoteException {
        try {
            com.energyindex.liferay.sb.model.PriceVert returnValue = PriceVertServiceUtil.deletePriceVert(price_id);

            return com.energyindex.liferay.sb.model.PriceVertSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static com.energyindex.liferay.sb.model.PriceVertSoap getPriceVert(
        long eventId) throws RemoteException {
        try {
            com.energyindex.liferay.sb.model.PriceVert returnValue = PriceVertServiceUtil.getPriceVert(eventId);

            return com.energyindex.liferay.sb.model.PriceVertSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static com.energyindex.liferay.sb.model.PriceVertSoap updatePriceVert(
        int priceId, long userId, long groupId, long price_close, int port_id,
        int product_id, int month, int day, int year, long portId,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws RemoteException {
        try {
            com.energyindex.liferay.sb.model.PriceVert returnValue = PriceVertServiceUtil.updatePriceVert(priceId,
                    userId, groupId, price_close, port_id, product_id, month,
                    day, year, portId, serviceContext);

            return com.energyindex.liferay.sb.model.PriceVertSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
