package com.energyindex.liferay.sb.model.impl;

import com.energyindex.liferay.sb.model.Internalindexes;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Internalindexes in entity cache.
 *
 * @author InfinityFrame : Jason Gonin
 * @see Internalindexes
 * @generated
 */
public class InternalindexesCacheModel implements CacheModel<Internalindexes>,
    Externalizable {
    public int price_id;
    public long groupId;
    public long companyId;
    public long userId;
    public String userName;
    public long createDate;
    public long modifiedDate;
    public int product_id;
    public long price_date;
    public int month_id;
    public int price_contractyear;
    public double price_low;
    public double price_current;
    public double price_high;
    public double price_close;
    public int price_unitquote;
    public int currency_id;
    public int unit_id;
    public int price_quantity;
    public int delivery_id;
    public int priceindex_id;
    public int company_id;
    public int contracttype_id;
    public int city_id;
    public int port_id;
    public int airport_id;
    public int state_id;
    public int subregion_id;
    public int country_id;
    public int publish_id;
    public int current_id;
    public int price_highlight;
    public int price_mainhighlight;
    public int price_unqouted;
    public long price_lastedit;
    public long price_dateadded;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(73);

        sb.append("{price_id=");
        sb.append(price_id);
        sb.append(", groupId=");
        sb.append(groupId);
        sb.append(", companyId=");
        sb.append(companyId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", userName=");
        sb.append(userName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", product_id=");
        sb.append(product_id);
        sb.append(", price_date=");
        sb.append(price_date);
        sb.append(", month_id=");
        sb.append(month_id);
        sb.append(", price_contractyear=");
        sb.append(price_contractyear);
        sb.append(", price_low=");
        sb.append(price_low);
        sb.append(", price_current=");
        sb.append(price_current);
        sb.append(", price_high=");
        sb.append(price_high);
        sb.append(", price_close=");
        sb.append(price_close);
        sb.append(", price_unitquote=");
        sb.append(price_unitquote);
        sb.append(", currency_id=");
        sb.append(currency_id);
        sb.append(", unit_id=");
        sb.append(unit_id);
        sb.append(", price_quantity=");
        sb.append(price_quantity);
        sb.append(", delivery_id=");
        sb.append(delivery_id);
        sb.append(", priceindex_id=");
        sb.append(priceindex_id);
        sb.append(", company_id=");
        sb.append(company_id);
        sb.append(", contracttype_id=");
        sb.append(contracttype_id);
        sb.append(", city_id=");
        sb.append(city_id);
        sb.append(", port_id=");
        sb.append(port_id);
        sb.append(", airport_id=");
        sb.append(airport_id);
        sb.append(", state_id=");
        sb.append(state_id);
        sb.append(", subregion_id=");
        sb.append(subregion_id);
        sb.append(", country_id=");
        sb.append(country_id);
        sb.append(", publish_id=");
        sb.append(publish_id);
        sb.append(", current_id=");
        sb.append(current_id);
        sb.append(", price_highlight=");
        sb.append(price_highlight);
        sb.append(", price_mainhighlight=");
        sb.append(price_mainhighlight);
        sb.append(", price_unqouted=");
        sb.append(price_unqouted);
        sb.append(", price_lastedit=");
        sb.append(price_lastedit);
        sb.append(", price_dateadded=");
        sb.append(price_dateadded);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Internalindexes toEntityModel() {
        InternalindexesImpl internalindexesImpl = new InternalindexesImpl();

        internalindexesImpl.setPrice_id(price_id);
        internalindexesImpl.setGroupId(groupId);
        internalindexesImpl.setCompanyId(companyId);
        internalindexesImpl.setUserId(userId);

        if (userName == null) {
            internalindexesImpl.setUserName(StringPool.BLANK);
        } else {
            internalindexesImpl.setUserName(userName);
        }

        if (createDate == Long.MIN_VALUE) {
            internalindexesImpl.setCreateDate(null);
        } else {
            internalindexesImpl.setCreateDate(new Date(createDate));
        }

        if (modifiedDate == Long.MIN_VALUE) {
            internalindexesImpl.setModifiedDate(null);
        } else {
            internalindexesImpl.setModifiedDate(new Date(modifiedDate));
        }

        internalindexesImpl.setProduct_id(product_id);

        if (price_date == Long.MIN_VALUE) {
            internalindexesImpl.setPrice_date(null);
        } else {
            internalindexesImpl.setPrice_date(new Date(price_date));
        }

        internalindexesImpl.setMonth_id(month_id);
        internalindexesImpl.setPrice_contractyear(price_contractyear);
        internalindexesImpl.setPrice_low(price_low);
        internalindexesImpl.setPrice_current(price_current);
        internalindexesImpl.setPrice_high(price_high);
        internalindexesImpl.setPrice_close(price_close);
        internalindexesImpl.setPrice_unitquote(price_unitquote);
        internalindexesImpl.setCurrency_id(currency_id);
        internalindexesImpl.setUnit_id(unit_id);
        internalindexesImpl.setPrice_quantity(price_quantity);
        internalindexesImpl.setDelivery_id(delivery_id);
        internalindexesImpl.setPriceindex_id(priceindex_id);
        internalindexesImpl.setCompany_id(company_id);
        internalindexesImpl.setContracttype_id(contracttype_id);
        internalindexesImpl.setCity_id(city_id);
        internalindexesImpl.setPort_id(port_id);
        internalindexesImpl.setAirport_id(airport_id);
        internalindexesImpl.setState_id(state_id);
        internalindexesImpl.setSubregion_id(subregion_id);
        internalindexesImpl.setCountry_id(country_id);
        internalindexesImpl.setPublish_id(publish_id);
        internalindexesImpl.setCurrent_id(current_id);
        internalindexesImpl.setPrice_highlight(price_highlight);
        internalindexesImpl.setPrice_mainhighlight(price_mainhighlight);
        internalindexesImpl.setPrice_unqouted(price_unqouted);

        if (price_lastedit == Long.MIN_VALUE) {
            internalindexesImpl.setPrice_lastedit(null);
        } else {
            internalindexesImpl.setPrice_lastedit(new Date(price_lastedit));
        }

        if (price_dateadded == Long.MIN_VALUE) {
            internalindexesImpl.setPrice_dateadded(null);
        } else {
            internalindexesImpl.setPrice_dateadded(new Date(price_dateadded));
        }

        internalindexesImpl.resetOriginalValues();

        return internalindexesImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        price_id = objectInput.readInt();
        groupId = objectInput.readLong();
        companyId = objectInput.readLong();
        userId = objectInput.readLong();
        userName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifiedDate = objectInput.readLong();
        product_id = objectInput.readInt();
        price_date = objectInput.readLong();
        month_id = objectInput.readInt();
        price_contractyear = objectInput.readInt();
        price_low = objectInput.readDouble();
        price_current = objectInput.readDouble();
        price_high = objectInput.readDouble();
        price_close = objectInput.readDouble();
        price_unitquote = objectInput.readInt();
        currency_id = objectInput.readInt();
        unit_id = objectInput.readInt();
        price_quantity = objectInput.readInt();
        delivery_id = objectInput.readInt();
        priceindex_id = objectInput.readInt();
        company_id = objectInput.readInt();
        contracttype_id = objectInput.readInt();
        city_id = objectInput.readInt();
        port_id = objectInput.readInt();
        airport_id = objectInput.readInt();
        state_id = objectInput.readInt();
        subregion_id = objectInput.readInt();
        country_id = objectInput.readInt();
        publish_id = objectInput.readInt();
        current_id = objectInput.readInt();
        price_highlight = objectInput.readInt();
        price_mainhighlight = objectInput.readInt();
        price_unqouted = objectInput.readInt();
        price_lastedit = objectInput.readLong();
        price_dateadded = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeInt(price_id);
        objectOutput.writeLong(groupId);
        objectOutput.writeLong(companyId);
        objectOutput.writeLong(userId);

        if (userName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(userName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifiedDate);
        objectOutput.writeInt(product_id);
        objectOutput.writeLong(price_date);
        objectOutput.writeInt(month_id);
        objectOutput.writeInt(price_contractyear);
        objectOutput.writeDouble(price_low);
        objectOutput.writeDouble(price_current);
        objectOutput.writeDouble(price_high);
        objectOutput.writeDouble(price_close);
        objectOutput.writeInt(price_unitquote);
        objectOutput.writeInt(currency_id);
        objectOutput.writeInt(unit_id);
        objectOutput.writeInt(price_quantity);
        objectOutput.writeInt(delivery_id);
        objectOutput.writeInt(priceindex_id);
        objectOutput.writeInt(company_id);
        objectOutput.writeInt(contracttype_id);
        objectOutput.writeInt(city_id);
        objectOutput.writeInt(port_id);
        objectOutput.writeInt(airport_id);
        objectOutput.writeInt(state_id);
        objectOutput.writeInt(subregion_id);
        objectOutput.writeInt(country_id);
        objectOutput.writeInt(publish_id);
        objectOutput.writeInt(current_id);
        objectOutput.writeInt(price_highlight);
        objectOutput.writeInt(price_mainhighlight);
        objectOutput.writeInt(price_unqouted);
        objectOutput.writeLong(price_lastedit);
        objectOutput.writeLong(price_dateadded);
    }
}
