package com.energyindex.liferay.sb.service.impl;

import com.energyindex.liferay.sb.service.base.InternalindexesServiceBaseImpl;

/**
 * The implementation of the internalindexes remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.service.InternalindexesService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.base.InternalindexesServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.InternalindexesServiceUtil
 */
public class InternalindexesServiceImpl extends InternalindexesServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.energyindex.liferay.sb.service.InternalindexesServiceUtil} to access the internalindexes remote service.
     */
}
