package com.energyindex.liferay.sb.service.impl;

import com.energyindex.liferay.sb.service.base.PriceVertLocalServiceBaseImpl;
import java.util.Date;
import java.util.List;

import com.energyindex.liferay.sb.model.PriceVert;
import com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil;
import com.energyindex.liferay.sb.service.base.PriceVertLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;

import java.util.Calendar;

/**
 * The implementation of the price vert local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.service.PriceVertLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.base.PriceVertLocalServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil
 */
public class PriceVertLocalServiceImpl extends PriceVertLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil} to access the price vert local service.
     */
	public PriceVert addPriceVert(long userId, long groupId, long price_close,
			int port_id,int product_id, int month, int day, int year, long portId,
			ServiceContext serviceContext) throws PortalException,
			SystemException {

		User user = userPersistence.findByPrimaryKey(userId);

		Date now = new Date();

		long priceId = counterLocalService.increment(PriceVert.class.getName());

		PriceVert price = priceVertPersistence.create(priceId);

		price.setPrice_close(price_close);

		Calendar dateCal = CalendarFactoryUtil.getCalendar(user.getTimeZone());
		dateCal.set(year, month, day, 0, 0);
		Date date = dateCal.getTime();
		price.setPrice_date(date);
		
		price.setProduct_id(product_id);
		price.setPort_id(port_id);
		
		super.addPriceVert(price);

		// Resources

		if (serviceContext.isAddGroupPermissions()
				|| serviceContext.isAddGuestPermissions()) {

			addPriceVertResources(price,
					serviceContext.isAddGroupPermissions(),
					serviceContext.isAddGuestPermissions());
		} else {
			addPriceVertResources(price, serviceContext.getGroupPermissions(),
					serviceContext.getGuestPermissions());
		}

		return price;
	}

	public void addPriceVertResources(PriceVert price,
			String[] groupPermissions, String[] guestPermissions)
			throws PortalException, SystemException {

		resourceLocalService.addModelResources(price.getCompanyId(),
				price.getGroupId(), price.getUserId(),
				PriceVert.class.getName(), price.getPrice_id(),
				groupPermissions, guestPermissions);
	}

	public void addPriceVertResources(PriceVert price,
			boolean addGroupPermissions, boolean addGuestPermissions)
			throws PortalException, SystemException {

		resourceLocalService.addResources(price.getCompanyId(),
				price.getGroupId(), price.getUserId(),
				PriceVert.class.getName(), price.getPrice_id(), false,
				addGroupPermissions, addGuestPermissions);
	}

	public PriceVert deletePriceVert(PriceVert price) throws SystemException {

		return priceVertPersistence.remove(price);
	}

	public PriceVert deletePriceVert(long priceId) throws PortalException,
			SystemException {

		PriceVert price = priceVertPersistence.findByPrimaryKey(priceId);

		return deletePriceVert(price);
	}

	public PriceVert getPriceVert(long priceId) throws SystemException,
			PortalException {

		return priceVertPersistence.findByPrimaryKey(priceId);
	}

	public List<PriceVert> getPriceVertsByPortId(int portId)
			throws SystemException {
		return priceVertPersistence.findByport_id(portId);
	}

	public List<PriceVert> getPriceVertsByPortId(int portId, int start, int end)
			throws SystemException {
		return priceVertPersistence.findByport_id(portId, start, end);
	}

	public int getPriceVertsCountByPortId(int port_id) throws SystemException {
		return priceVertPersistence.countByport_id(port_id);
	}

	public PriceVert updatePriceVert(int priceId, long userId, long groupId, long price_close,
			int port_id,int product_id, int month, int day, int year, long portId,
			ServiceContext serviceContext) throws PortalException,
			SystemException {

		User user = userPersistence.findByPrimaryKey(priceId);

		Date now = new Date();

		PriceVert price = PriceVertLocalServiceUtil.fetchPriceVert(priceId);

		price.setPrice_close(price_close);

		Calendar dateCal = CalendarFactoryUtil.getCalendar(user.getTimeZone());
		dateCal.set(year, month, day, 0, 0);
		Date date = dateCal.getTime();
		
		price.setPrice_date(date);
		price.setPort_id(port_id);
		price.setProduct_id(product_id);
		
		super.updatePriceVert(price);

		return price;
	}
}
