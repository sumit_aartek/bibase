package com.energyindex.liferay.sb.service.impl;

import com.energyindex.liferay.sb.service.base.NormalizationServiceBaseImpl;

/**
 * The implementation of the normalization remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.service.NormalizationService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.base.NormalizationServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.NormalizationServiceUtil
 */
public class NormalizationServiceImpl extends NormalizationServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.energyindex.liferay.sb.service.NormalizationServiceUtil} to access the normalization remote service.
     */
}
