package com.energyindex.liferay.sb.service.impl;

import com.energyindex.liferay.sb.service.base.PriceVertServiceBaseImpl;
import com.energyindex.liferay.sb.model.PriceVert;
import com.energyindex.liferay.sb.service.PriceVertLocalServiceUtil;
import com.energyindex.liferay.sb.service.base.PriceVertServiceBaseImpl;
import com.energyindex.liferay.sb.service.permission.PriceVertListingPermission;
import com.energyindex.liferay.sb.util.PriceVertActionKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the price vert remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.service.PriceVertService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.base.PriceVertServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.PriceVertServiceUtil
 */
public class PriceVertServiceImpl extends PriceVertServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.energyindex.liferay.sb.service.PriceVertServiceUtil} to access the price vert remote service.
     */
	public PriceVert addPriceVert(long userId, long groupId, long price_close,
			int port_id,int product_id, int month, int day, int year, long portId,
			ServiceContext serviceContext)
		       throws PortalException, SystemException {

		PriceVertListingPermission.check(getPermissionChecker(), groupId,
		         PriceVertActionKeys.ADD_EVENT);

		    return PriceVertLocalServiceUtil.addPriceVert( userId,  groupId,  price_close,
					 port_id, product_id,  month,  day,  year,  portId,
					 serviceContext);
		}

		public PriceVert deletePriceVert(long price_id) throws PortalException,
		       SystemException {

			PriceVertListingPermission.check(getPermissionChecker(), price_id,
		         PriceVertActionKeys.DELETE_EVENT);

		    return PriceVertLocalServiceUtil.deletePriceVert(price_id);
		}

		public PriceVert getPriceVert(long eventId) throws PortalException, SystemException {

			PriceVertListingPermission.check(getPermissionChecker(), eventId,
		         PriceVertActionKeys.VIEW);

		    return PriceVertLocalServiceUtil.getPriceVert(eventId);
		}

		public PriceVert updatePriceVert(int priceId, long userId, long groupId, long price_close,
				int port_id,int product_id, int month, int day, int year, long portId,
				ServiceContext serviceContext)
		       throws PortalException, SystemException {

		    PriceVertListingPermission.check(getPermissionChecker(), priceId,
		         PriceVertActionKeys.UPDATE_EVENT);

		    return PriceVertLocalServiceUtil.updatePriceVert(priceId, userId, groupId, price_close, port_id, product_id, month, day, year, portId, serviceContext);
		}

}
