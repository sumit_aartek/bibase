package com.energyindex.liferay.sb.model.impl;

import com.energyindex.liferay.sb.model.PriceHoriz;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing PriceHoriz in entity cache.
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceHoriz
 * @generated
 */
public class PriceHorizCacheModel implements CacheModel<PriceHoriz>,
    Externalizable {
    public long priceH_id;
    public long groupId;
    public long companyId;
    public long userId;
    public String userName;
    public long createDate;
    public long modifiedDate;
    public long price_date;
    public int product_id;
    public double price_low;
    public double price_high;
    public double price_close;
    public double price_change;
    public int port_id;
    public int unit_id;
    public int currency_id;
    public int contracttype_id;
    public int publish_id;
    public long price_id;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(39);

        sb.append("{priceH_id=");
        sb.append(priceH_id);
        sb.append(", groupId=");
        sb.append(groupId);
        sb.append(", companyId=");
        sb.append(companyId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", userName=");
        sb.append(userName);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", price_date=");
        sb.append(price_date);
        sb.append(", product_id=");
        sb.append(product_id);
        sb.append(", price_low=");
        sb.append(price_low);
        sb.append(", price_high=");
        sb.append(price_high);
        sb.append(", price_close=");
        sb.append(price_close);
        sb.append(", price_change=");
        sb.append(price_change);
        sb.append(", port_id=");
        sb.append(port_id);
        sb.append(", unit_id=");
        sb.append(unit_id);
        sb.append(", currency_id=");
        sb.append(currency_id);
        sb.append(", contracttype_id=");
        sb.append(contracttype_id);
        sb.append(", publish_id=");
        sb.append(publish_id);
        sb.append(", price_id=");
        sb.append(price_id);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public PriceHoriz toEntityModel() {
        PriceHorizImpl priceHorizImpl = new PriceHorizImpl();

        priceHorizImpl.setPriceH_id(priceH_id);
        priceHorizImpl.setGroupId(groupId);
        priceHorizImpl.setCompanyId(companyId);
        priceHorizImpl.setUserId(userId);

        if (userName == null) {
            priceHorizImpl.setUserName(StringPool.BLANK);
        } else {
            priceHorizImpl.setUserName(userName);
        }

        if (createDate == Long.MIN_VALUE) {
            priceHorizImpl.setCreateDate(null);
        } else {
            priceHorizImpl.setCreateDate(new Date(createDate));
        }

        if (modifiedDate == Long.MIN_VALUE) {
            priceHorizImpl.setModifiedDate(null);
        } else {
            priceHorizImpl.setModifiedDate(new Date(modifiedDate));
        }

        if (price_date == Long.MIN_VALUE) {
            priceHorizImpl.setPrice_date(null);
        } else {
            priceHorizImpl.setPrice_date(new Date(price_date));
        }

        priceHorizImpl.setProduct_id(product_id);
        priceHorizImpl.setPrice_low(price_low);
        priceHorizImpl.setPrice_high(price_high);
        priceHorizImpl.setPrice_close(price_close);
        priceHorizImpl.setPrice_change(price_change);
        priceHorizImpl.setPort_id(port_id);
        priceHorizImpl.setUnit_id(unit_id);
        priceHorizImpl.setCurrency_id(currency_id);
        priceHorizImpl.setContracttype_id(contracttype_id);
        priceHorizImpl.setPublish_id(publish_id);
        priceHorizImpl.setPrice_id(price_id);

        priceHorizImpl.resetOriginalValues();

        return priceHorizImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        priceH_id = objectInput.readLong();
        groupId = objectInput.readLong();
        companyId = objectInput.readLong();
        userId = objectInput.readLong();
        userName = objectInput.readUTF();
        createDate = objectInput.readLong();
        modifiedDate = objectInput.readLong();
        price_date = objectInput.readLong();
        product_id = objectInput.readInt();
        price_low = objectInput.readDouble();
        price_high = objectInput.readDouble();
        price_close = objectInput.readDouble();
        price_change = objectInput.readDouble();
        port_id = objectInput.readInt();
        unit_id = objectInput.readInt();
        currency_id = objectInput.readInt();
        contracttype_id = objectInput.readInt();
        publish_id = objectInput.readInt();
        price_id = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(priceH_id);
        objectOutput.writeLong(groupId);
        objectOutput.writeLong(companyId);
        objectOutput.writeLong(userId);

        if (userName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(userName);
        }

        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifiedDate);
        objectOutput.writeLong(price_date);
        objectOutput.writeInt(product_id);
        objectOutput.writeDouble(price_low);
        objectOutput.writeDouble(price_high);
        objectOutput.writeDouble(price_close);
        objectOutput.writeDouble(price_change);
        objectOutput.writeInt(port_id);
        objectOutput.writeInt(unit_id);
        objectOutput.writeInt(currency_id);
        objectOutput.writeInt(contracttype_id);
        objectOutput.writeInt(publish_id);
        objectOutput.writeLong(price_id);
    }
}
