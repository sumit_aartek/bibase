package com.energyindex.liferay.sb.service.impl;

import com.energyindex.liferay.sb.service.base.NormalizationLocalServiceBaseImpl;

/**
 * The implementation of the normalization local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.service.NormalizationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see com.energyindex.liferay.sb.service.base.NormalizationLocalServiceBaseImpl
 * @see com.energyindex.liferay.sb.service.NormalizationLocalServiceUtil
 */
public class NormalizationLocalServiceImpl
    extends NormalizationLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.energyindex.liferay.sb.service.NormalizationLocalServiceUtil} to access the normalization local service.
     */
}
