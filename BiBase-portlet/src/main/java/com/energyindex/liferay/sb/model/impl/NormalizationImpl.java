package com.energyindex.liferay.sb.model.impl;

/**
 * The extended model implementation for the Normalization service. Represents a row in the &quot;BiBase_Normalization&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.energyindex.liferay.sb.model.Normalization} interface.
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 */
public class NormalizationImpl extends NormalizationBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a normalization model instance should use the {@link com.energyindex.liferay.sb.model.Normalization} interface instead.
     */
    public NormalizationImpl() {
    }
}
