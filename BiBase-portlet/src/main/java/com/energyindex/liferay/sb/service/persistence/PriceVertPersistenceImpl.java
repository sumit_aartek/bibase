package com.energyindex.liferay.sb.service.persistence;

import com.energyindex.liferay.sb.NoSuchPriceVertException;
import com.energyindex.liferay.sb.model.PriceVert;
import com.energyindex.liferay.sb.model.impl.PriceVertImpl;
import com.energyindex.liferay.sb.model.impl.PriceVertModelImpl;
import com.energyindex.liferay.sb.service.persistence.PriceVertPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The persistence implementation for the price vert service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author InfinityFrame : Jason Gonin
 * @see PriceVertPersistence
 * @see PriceVertUtil
 * @generated
 */
public class PriceVertPersistenceImpl extends BasePersistenceImpl<PriceVert>
    implements PriceVertPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PriceVertUtil} to access the price vert persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PriceVertImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, PriceVertImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, PriceVertImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PRODUCT_ID =
        new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, PriceVertImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByproduct_id",
            new String[] {
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCT_ID =
        new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, PriceVertImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByproduct_id",
            new String[] { Integer.class.getName() },
            PriceVertModelImpl.PRODUCT_ID_COLUMN_BITMASK |
            PriceVertModelImpl.PRICE_DATE_COLUMN_BITMASK |
            PriceVertModelImpl.PORT_ID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PRODUCT_ID = new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByproduct_id",
            new String[] { Integer.class.getName() });
    private static final String _FINDER_COLUMN_PRODUCT_ID_PRODUCT_ID_2 = "priceVert.product_id = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICE_DATE =
        new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, PriceVertImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByprice_date",
            new String[] {
                Date.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICE_DATE =
        new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByprice_date",
            new String[] { Date.class.getName() });
    private static final String _FINDER_COLUMN_PRICE_DATE_PRICE_DATE_1 = "priceVert.price_date >= NULL";
    private static final String _FINDER_COLUMN_PRICE_DATE_PRICE_DATE_2 = "priceVert.price_date >= ?";
    private static final String _FINDER_COLUMN_PRICE_DATE_PRICE_DATE_4 = "(" +
        removeConjunction(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_1) + ")";
    private static final String _FINDER_COLUMN_PRICE_DATE_PRICE_DATE_5 = "(" +
        removeConjunction(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_2) + ")";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PORT_ID = new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, PriceVertImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByport_id",
            new String[] {
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PORT_ID =
        new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, PriceVertImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByport_id",
            new String[] { Integer.class.getName() },
            PriceVertModelImpl.PORT_ID_COLUMN_BITMASK |
            PriceVertModelImpl.PRODUCT_ID_COLUMN_BITMASK |
            PriceVertModelImpl.PRICE_DATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PORT_ID = new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByport_id",
            new String[] { Integer.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_PORT_ID = new FinderPath(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByport_id",
            new String[] { Integer.class.getName() });
    private static final String _FINDER_COLUMN_PORT_ID_PORT_ID_2 = "priceVert.port_id = ?";
    private static final String _FINDER_COLUMN_PORT_ID_PORT_ID_5 = "(" +
        removeConjunction(_FINDER_COLUMN_PORT_ID_PORT_ID_2) + ")";
    private static final String _SQL_SELECT_PRICEVERT = "SELECT priceVert FROM PriceVert priceVert";
    private static final String _SQL_SELECT_PRICEVERT_WHERE = "SELECT priceVert FROM PriceVert priceVert WHERE ";
    private static final String _SQL_COUNT_PRICEVERT = "SELECT COUNT(priceVert) FROM PriceVert priceVert";
    private static final String _SQL_COUNT_PRICEVERT_WHERE = "SELECT COUNT(priceVert) FROM PriceVert priceVert WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "priceVert.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PriceVert exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PriceVert exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PriceVertPersistenceImpl.class);
    private static PriceVert _nullPriceVert = new PriceVertImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<PriceVert> toCacheModel() {
                return _nullPriceVertCacheModel;
            }
        };

    private static CacheModel<PriceVert> _nullPriceVertCacheModel = new CacheModel<PriceVert>() {
            @Override
            public PriceVert toEntityModel() {
                return _nullPriceVert;
            }
        };

    public PriceVertPersistenceImpl() {
        setModelClass(PriceVert.class);
    }

    /**
     * Returns all the price verts where product_id = &#63;.
     *
     * @param product_id the product_id
     * @return the matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByproduct_id(int product_id)
        throws SystemException {
        return findByproduct_id(product_id, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price verts where product_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param product_id the product_id
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @return the range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByproduct_id(int product_id, int start, int end)
        throws SystemException {
        return findByproduct_id(product_id, start, end, null);
    }

    /**
     * Returns an ordered range of all the price verts where product_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param product_id the product_id
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByproduct_id(int product_id, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCT_ID;
            finderArgs = new Object[] { product_id };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PRODUCT_ID;
            finderArgs = new Object[] { product_id, start, end, orderByComparator };
        }

        List<PriceVert> list = (List<PriceVert>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceVert priceVert : list) {
                if ((product_id != priceVert.getProduct_id())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PRICEVERT_WHERE);

            query.append(_FINDER_COLUMN_PRODUCT_ID_PRODUCT_ID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceVertModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(product_id);

                if (!pagination) {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceVert>(list);
                } else {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first price vert in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert findByproduct_id_First(int product_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = fetchByproduct_id_First(product_id,
                orderByComparator);

        if (priceVert != null) {
            return priceVert;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("product_id=");
        msg.append(product_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceVertException(msg.toString());
    }

    /**
     * Returns the first price vert in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price vert, or <code>null</code> if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert fetchByproduct_id_First(int product_id,
        OrderByComparator orderByComparator) throws SystemException {
        List<PriceVert> list = findByproduct_id(product_id, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last price vert in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert findByproduct_id_Last(int product_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = fetchByproduct_id_Last(product_id,
                orderByComparator);

        if (priceVert != null) {
            return priceVert;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("product_id=");
        msg.append(product_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceVertException(msg.toString());
    }

    /**
     * Returns the last price vert in the ordered set where product_id = &#63;.
     *
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price vert, or <code>null</code> if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert fetchByproduct_id_Last(int product_id,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByproduct_id(product_id);

        if (count == 0) {
            return null;
        }

        List<PriceVert> list = findByproduct_id(product_id, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the price verts before and after the current price vert in the ordered set where product_id = &#63;.
     *
     * @param price_id the primary key of the current price vert
     * @param product_id the product_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert[] findByproduct_id_PrevAndNext(long price_id,
        int product_id, OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = findByPrimaryKey(price_id);

        Session session = null;

        try {
            session = openSession();

            PriceVert[] array = new PriceVertImpl[3];

            array[0] = getByproduct_id_PrevAndNext(session, priceVert,
                    product_id, orderByComparator, true);

            array[1] = priceVert;

            array[2] = getByproduct_id_PrevAndNext(session, priceVert,
                    product_id, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected PriceVert getByproduct_id_PrevAndNext(Session session,
        PriceVert priceVert, int product_id,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PRICEVERT_WHERE);

        query.append(_FINDER_COLUMN_PRODUCT_ID_PRODUCT_ID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PriceVertModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(product_id);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(priceVert);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<PriceVert> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the price verts where product_id = &#63; from the database.
     *
     * @param product_id the product_id
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByproduct_id(int product_id) throws SystemException {
        for (PriceVert priceVert : findByproduct_id(product_id,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(priceVert);
        }
    }

    /**
     * Returns the number of price verts where product_id = &#63;.
     *
     * @param product_id the product_id
     * @return the number of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByproduct_id(int product_id) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PRODUCT_ID;

        Object[] finderArgs = new Object[] { product_id };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PRICEVERT_WHERE);

            query.append(_FINDER_COLUMN_PRODUCT_ID_PRODUCT_ID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(product_id);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the price verts where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @return the matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByprice_date(Date price_date)
        throws SystemException {
        return findByprice_date(price_date, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price verts where price_date &ge; &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_date the price_date
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @return the range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByprice_date(Date price_date, int start, int end)
        throws SystemException {
        return findByprice_date(price_date, start, end, null);
    }

    /**
     * Returns an ordered range of all the price verts where price_date &ge; &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_date the price_date
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByprice_date(Date price_date, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICE_DATE;
        finderArgs = new Object[] { price_date, start, end, orderByComparator };

        List<PriceVert> list = (List<PriceVert>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceVert priceVert : list) {
                if ((price_date.getTime() > priceVert.getPrice_date().getTime())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PRICEVERT_WHERE);

            boolean bindPrice_date = false;

            if (price_date == null) {
                query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_1);
            } else {
                bindPrice_date = true;

                query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_2);
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceVertModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindPrice_date) {
                    qPos.add(CalendarUtil.getTimestamp(price_date));
                }

                if (!pagination) {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceVert>(list);
                } else {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first price vert in the ordered set where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert findByprice_date_First(Date price_date,
        OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = fetchByprice_date_First(price_date,
                orderByComparator);

        if (priceVert != null) {
            return priceVert;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("price_date=");
        msg.append(price_date);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceVertException(msg.toString());
    }

    /**
     * Returns the first price vert in the ordered set where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price vert, or <code>null</code> if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert fetchByprice_date_First(Date price_date,
        OrderByComparator orderByComparator) throws SystemException {
        List<PriceVert> list = findByprice_date(price_date, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last price vert in the ordered set where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert findByprice_date_Last(Date price_date,
        OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = fetchByprice_date_Last(price_date,
                orderByComparator);

        if (priceVert != null) {
            return priceVert;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("price_date=");
        msg.append(price_date);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceVertException(msg.toString());
    }

    /**
     * Returns the last price vert in the ordered set where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price vert, or <code>null</code> if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert fetchByprice_date_Last(Date price_date,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByprice_date(price_date);

        if (count == 0) {
            return null;
        }

        List<PriceVert> list = findByprice_date(price_date, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the price verts before and after the current price vert in the ordered set where price_date &ge; &#63;.
     *
     * @param price_id the primary key of the current price vert
     * @param price_date the price_date
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert[] findByprice_date_PrevAndNext(long price_id,
        Date price_date, OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = findByPrimaryKey(price_id);

        Session session = null;

        try {
            session = openSession();

            PriceVert[] array = new PriceVertImpl[3];

            array[0] = getByprice_date_PrevAndNext(session, priceVert,
                    price_date, orderByComparator, true);

            array[1] = priceVert;

            array[2] = getByprice_date_PrevAndNext(session, priceVert,
                    price_date, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected PriceVert getByprice_date_PrevAndNext(Session session,
        PriceVert priceVert, Date price_date,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PRICEVERT_WHERE);

        boolean bindPrice_date = false;

        if (price_date == null) {
            query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_1);
        } else {
            bindPrice_date = true;

            query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_2);
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PriceVertModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (bindPrice_date) {
            qPos.add(CalendarUtil.getTimestamp(price_date));
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(priceVert);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<PriceVert> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the price verts where price_date &ge; all &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_dates the price_dates
     * @return the matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByprice_date(Date[] price_dates)
        throws SystemException {
        return findByprice_date(price_dates, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price verts where price_date &ge; all &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_dates the price_dates
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @return the range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByprice_date(Date[] price_dates, int start,
        int end) throws SystemException {
        return findByprice_date(price_dates, start, end, null);
    }

    /**
     * Returns an ordered range of all the price verts where price_date &ge; all &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param price_dates the price_dates
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByprice_date(Date[] price_dates, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        if ((price_dates != null) && (price_dates.length == 1)) {
            return findByprice_date(price_dates[0], start, end,
                orderByComparator);
        }

        boolean pagination = true;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderArgs = new Object[] { StringUtil.merge(price_dates) };
        } else {
            finderArgs = new Object[] {
                    StringUtil.merge(price_dates),
                    
                    start, end, orderByComparator
                };
        }

        List<PriceVert> list = (List<PriceVert>) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICE_DATE,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceVert priceVert : list) {
                if (!ArrayUtil.contains(price_dates, priceVert.getPrice_date())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_SELECT_PRICEVERT_WHERE);

            boolean conjunctionable = false;

            if ((price_dates == null) || (price_dates.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < price_dates.length; i++) {
                    Date price_date = price_dates[i];

                    if (price_date == null) {
                        query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_4);
                    } else {
                        query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_5);
                    }

                    if ((i + 1) < price_dates.length) {
                        query.append(WHERE_AND);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceVertModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (price_dates != null) {
                    qPos.add(price_dates);
                }

                if (!pagination) {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceVert>(list);
                } else {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICE_DATE,
                    finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PRICE_DATE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the price verts where price_date &ge; &#63; from the database.
     *
     * @param price_date the price_date
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByprice_date(Date price_date) throws SystemException {
        for (PriceVert priceVert : findByprice_date(price_date,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(priceVert);
        }
    }

    /**
     * Returns the number of price verts where price_date &ge; &#63;.
     *
     * @param price_date the price_date
     * @return the number of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByprice_date(Date price_date) throws SystemException {
        FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICE_DATE;

        Object[] finderArgs = new Object[] { price_date };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PRICEVERT_WHERE);

            boolean bindPrice_date = false;

            if (price_date == null) {
                query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_1);
            } else {
                bindPrice_date = true;

                query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindPrice_date) {
                    qPos.add(CalendarUtil.getTimestamp(price_date));
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of price verts where price_date &ge; all &#63;.
     *
     * @param price_dates the price_dates
     * @return the number of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByprice_date(Date[] price_dates) throws SystemException {
        Object[] finderArgs = new Object[] { StringUtil.merge(price_dates) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICE_DATE,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_COUNT_PRICEVERT_WHERE);

            boolean conjunctionable = false;

            if ((price_dates == null) || (price_dates.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < price_dates.length; i++) {
                    Date price_date = price_dates[i];

                    if (price_date == null) {
                        query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_4);
                    } else {
                        query.append(_FINDER_COLUMN_PRICE_DATE_PRICE_DATE_5);
                    }

                    if ((i + 1) < price_dates.length) {
                        query.append(WHERE_AND);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (price_dates != null) {
                    qPos.add(price_dates);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICE_DATE,
                    finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PRICE_DATE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the price verts where port_id = &#63;.
     *
     * @param port_id the port_id
     * @return the matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByport_id(int port_id) throws SystemException {
        return findByport_id(port_id, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price verts where port_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_id the port_id
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @return the range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByport_id(int port_id, int start, int end)
        throws SystemException {
        return findByport_id(port_id, start, end, null);
    }

    /**
     * Returns an ordered range of all the price verts where port_id = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_id the port_id
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByport_id(int port_id, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PORT_ID;
            finderArgs = new Object[] { port_id };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PORT_ID;
            finderArgs = new Object[] { port_id, start, end, orderByComparator };
        }

        List<PriceVert> list = (List<PriceVert>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceVert priceVert : list) {
                if ((port_id != priceVert.getPort_id())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PRICEVERT_WHERE);

            query.append(_FINDER_COLUMN_PORT_ID_PORT_ID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceVertModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(port_id);

                if (!pagination) {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceVert>(list);
                } else {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first price vert in the ordered set where port_id = &#63;.
     *
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert findByport_id_First(int port_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = fetchByport_id_First(port_id, orderByComparator);

        if (priceVert != null) {
            return priceVert;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("port_id=");
        msg.append(port_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceVertException(msg.toString());
    }

    /**
     * Returns the first price vert in the ordered set where port_id = &#63;.
     *
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching price vert, or <code>null</code> if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert fetchByport_id_First(int port_id,
        OrderByComparator orderByComparator) throws SystemException {
        List<PriceVert> list = findByport_id(port_id, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last price vert in the ordered set where port_id = &#63;.
     *
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert findByport_id_Last(int port_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = fetchByport_id_Last(port_id, orderByComparator);

        if (priceVert != null) {
            return priceVert;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("port_id=");
        msg.append(port_id);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPriceVertException(msg.toString());
    }

    /**
     * Returns the last price vert in the ordered set where port_id = &#63;.
     *
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching price vert, or <code>null</code> if a matching price vert could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert fetchByport_id_Last(int port_id,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByport_id(port_id);

        if (count == 0) {
            return null;
        }

        List<PriceVert> list = findByport_id(port_id, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the price verts before and after the current price vert in the ordered set where port_id = &#63;.
     *
     * @param price_id the primary key of the current price vert
     * @param port_id the port_id
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert[] findByport_id_PrevAndNext(long price_id, int port_id,
        OrderByComparator orderByComparator)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = findByPrimaryKey(price_id);

        Session session = null;

        try {
            session = openSession();

            PriceVert[] array = new PriceVertImpl[3];

            array[0] = getByport_id_PrevAndNext(session, priceVert, port_id,
                    orderByComparator, true);

            array[1] = priceVert;

            array[2] = getByport_id_PrevAndNext(session, priceVert, port_id,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected PriceVert getByport_id_PrevAndNext(Session session,
        PriceVert priceVert, int port_id, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PRICEVERT_WHERE);

        query.append(_FINDER_COLUMN_PORT_ID_PORT_ID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PriceVertModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(port_id);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(priceVert);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<PriceVert> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the price verts where port_id = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_ids the port_ids
     * @return the matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByport_id(int[] port_ids)
        throws SystemException {
        return findByport_id(port_ids, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the price verts where port_id = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_ids the port_ids
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @return the range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByport_id(int[] port_ids, int start, int end)
        throws SystemException {
        return findByport_id(port_ids, start, end, null);
    }

    /**
     * Returns an ordered range of all the price verts where port_id = any &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param port_ids the port_ids
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findByport_id(int[] port_ids, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        if ((port_ids != null) && (port_ids.length == 1)) {
            return findByport_id(port_ids[0], start, end, orderByComparator);
        }

        boolean pagination = true;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderArgs = new Object[] { StringUtil.merge(port_ids) };
        } else {
            finderArgs = new Object[] {
                    StringUtil.merge(port_ids),
                    
                    start, end, orderByComparator
                };
        }

        List<PriceVert> list = (List<PriceVert>) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PORT_ID,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (PriceVert priceVert : list) {
                if (!ArrayUtil.contains(port_ids, priceVert.getPort_id())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_SELECT_PRICEVERT_WHERE);

            boolean conjunctionable = false;

            if ((port_ids == null) || (port_ids.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < port_ids.length; i++) {
                    query.append(_FINDER_COLUMN_PORT_ID_PORT_ID_5);

                    if ((i + 1) < port_ids.length) {
                        query.append(WHERE_OR);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PriceVertModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (port_ids != null) {
                    qPos.add(port_ids);
                }

                if (!pagination) {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceVert>(list);
                } else {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PORT_ID,
                    finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_FIND_BY_PORT_ID,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the price verts where port_id = &#63; from the database.
     *
     * @param port_id the port_id
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByport_id(int port_id) throws SystemException {
        for (PriceVert priceVert : findByport_id(port_id, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(priceVert);
        }
    }

    /**
     * Returns the number of price verts where port_id = &#63;.
     *
     * @param port_id the port_id
     * @return the number of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByport_id(int port_id) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PORT_ID;

        Object[] finderArgs = new Object[] { port_id };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PRICEVERT_WHERE);

            query.append(_FINDER_COLUMN_PORT_ID_PORT_ID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(port_id);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of price verts where port_id = any &#63;.
     *
     * @param port_ids the port_ids
     * @return the number of matching price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByport_id(int[] port_ids) throws SystemException {
        Object[] finderArgs = new Object[] { StringUtil.merge(port_ids) };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PORT_ID,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler();

            query.append(_SQL_COUNT_PRICEVERT_WHERE);

            boolean conjunctionable = false;

            if ((port_ids == null) || (port_ids.length > 0)) {
                if (conjunctionable) {
                    query.append(WHERE_AND);
                }

                query.append(StringPool.OPEN_PARENTHESIS);

                for (int i = 0; i < port_ids.length; i++) {
                    query.append(_FINDER_COLUMN_PORT_ID_PORT_ID_5);

                    if ((i + 1) < port_ids.length) {
                        query.append(WHERE_OR);
                    }
                }

                query.append(StringPool.CLOSE_PARENTHESIS);

                conjunctionable = true;
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (port_ids != null) {
                    qPos.add(port_ids);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PORT_ID,
                    finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_WITH_PAGINATION_COUNT_BY_PORT_ID,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the price vert in the entity cache if it is enabled.
     *
     * @param priceVert the price vert
     */
    @Override
    public void cacheResult(PriceVert priceVert) {
        EntityCacheUtil.putResult(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertImpl.class, priceVert.getPrimaryKey(), priceVert);

        priceVert.resetOriginalValues();
    }

    /**
     * Caches the price verts in the entity cache if it is enabled.
     *
     * @param priceVerts the price verts
     */
    @Override
    public void cacheResult(List<PriceVert> priceVerts) {
        for (PriceVert priceVert : priceVerts) {
            if (EntityCacheUtil.getResult(
                        PriceVertModelImpl.ENTITY_CACHE_ENABLED,
                        PriceVertImpl.class, priceVert.getPrimaryKey()) == null) {
                cacheResult(priceVert);
            } else {
                priceVert.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all price verts.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PriceVertImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PriceVertImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the price vert.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(PriceVert priceVert) {
        EntityCacheUtil.removeResult(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertImpl.class, priceVert.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<PriceVert> priceVerts) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (PriceVert priceVert : priceVerts) {
            EntityCacheUtil.removeResult(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
                PriceVertImpl.class, priceVert.getPrimaryKey());
        }
    }

    /**
     * Creates a new price vert with the primary key. Does not add the price vert to the database.
     *
     * @param price_id the primary key for the new price vert
     * @return the new price vert
     */
    @Override
    public PriceVert create(long price_id) {
        PriceVert priceVert = new PriceVertImpl();

        priceVert.setNew(true);
        priceVert.setPrimaryKey(price_id);

        return priceVert;
    }

    /**
     * Removes the price vert with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param price_id the primary key of the price vert
     * @return the price vert that was removed
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert remove(long price_id)
        throws NoSuchPriceVertException, SystemException {
        return remove((Serializable) price_id);
    }

    /**
     * Removes the price vert with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the price vert
     * @return the price vert that was removed
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert remove(Serializable primaryKey)
        throws NoSuchPriceVertException, SystemException {
        Session session = null;

        try {
            session = openSession();

            PriceVert priceVert = (PriceVert) session.get(PriceVertImpl.class,
                    primaryKey);

            if (priceVert == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPriceVertException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(priceVert);
        } catch (NoSuchPriceVertException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected PriceVert removeImpl(PriceVert priceVert)
        throws SystemException {
        priceVert = toUnwrappedModel(priceVert);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(priceVert)) {
                priceVert = (PriceVert) session.get(PriceVertImpl.class,
                        priceVert.getPrimaryKeyObj());
            }

            if (priceVert != null) {
                session.delete(priceVert);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (priceVert != null) {
            clearCache(priceVert);
        }

        return priceVert;
    }

    @Override
    public PriceVert updateImpl(
        com.energyindex.liferay.sb.model.PriceVert priceVert)
        throws SystemException {
        priceVert = toUnwrappedModel(priceVert);

        boolean isNew = priceVert.isNew();

        PriceVertModelImpl priceVertModelImpl = (PriceVertModelImpl) priceVert;

        Session session = null;

        try {
            session = openSession();

            if (priceVert.isNew()) {
                session.save(priceVert);

                priceVert.setNew(false);
            } else {
                session.merge(priceVert);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !PriceVertModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((priceVertModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCT_ID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        priceVertModelImpl.getOriginalProduct_id()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRODUCT_ID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCT_ID,
                    args);

                args = new Object[] { priceVertModelImpl.getProduct_id() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRODUCT_ID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCT_ID,
                    args);
            }

            if ((priceVertModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PORT_ID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        priceVertModelImpl.getOriginalPort_id()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PORT_ID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PORT_ID,
                    args);

                args = new Object[] { priceVertModelImpl.getPort_id() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PORT_ID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PORT_ID,
                    args);
            }
        }

        EntityCacheUtil.putResult(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
            PriceVertImpl.class, priceVert.getPrimaryKey(), priceVert);

        return priceVert;
    }

    protected PriceVert toUnwrappedModel(PriceVert priceVert) {
        if (priceVert instanceof PriceVertImpl) {
            return priceVert;
        }

        PriceVertImpl priceVertImpl = new PriceVertImpl();

        priceVertImpl.setNew(priceVert.isNew());
        priceVertImpl.setPrimaryKey(priceVert.getPrimaryKey());

        priceVertImpl.setPrice_id(priceVert.getPrice_id());
        priceVertImpl.setGroupId(priceVert.getGroupId());
        priceVertImpl.setCompanyId(priceVert.getCompanyId());
        priceVertImpl.setUserId(priceVert.getUserId());
        priceVertImpl.setUserName(priceVert.getUserName());
        priceVertImpl.setCreateDate(priceVert.getCreateDate());
        priceVertImpl.setModifiedDate(priceVert.getModifiedDate());
        priceVertImpl.setPrice_date(priceVert.getPrice_date());
        priceVertImpl.setProduct_id(priceVert.getProduct_id());
        priceVertImpl.setPrice_low(priceVert.getPrice_low());
        priceVertImpl.setPrice_high(priceVert.getPrice_high());
        priceVertImpl.setPrice_close(priceVert.getPrice_close());
        priceVertImpl.setPrice_change(priceVert.getPrice_change());
        priceVertImpl.setPort_id(priceVert.getPort_id());
        priceVertImpl.setUnit_id(priceVert.getUnit_id());
        priceVertImpl.setCurrency_id(priceVert.getCurrency_id());
        priceVertImpl.setContracttype_id(priceVert.getContracttype_id());
        priceVertImpl.setPublish_id(priceVert.getPublish_id());

        return priceVertImpl;
    }

    /**
     * Returns the price vert with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the price vert
     * @return the price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert findByPrimaryKey(Serializable primaryKey)
        throws NoSuchPriceVertException, SystemException {
        PriceVert priceVert = fetchByPrimaryKey(primaryKey);

        if (priceVert == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchPriceVertException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return priceVert;
    }

    /**
     * Returns the price vert with the primary key or throws a {@link com.energyindex.liferay.sb.NoSuchPriceVertException} if it could not be found.
     *
     * @param price_id the primary key of the price vert
     * @return the price vert
     * @throws com.energyindex.liferay.sb.NoSuchPriceVertException if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert findByPrimaryKey(long price_id)
        throws NoSuchPriceVertException, SystemException {
        return findByPrimaryKey((Serializable) price_id);
    }

    /**
     * Returns the price vert with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the price vert
     * @return the price vert, or <code>null</code> if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        PriceVert priceVert = (PriceVert) EntityCacheUtil.getResult(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
                PriceVertImpl.class, primaryKey);

        if (priceVert == _nullPriceVert) {
            return null;
        }

        if (priceVert == null) {
            Session session = null;

            try {
                session = openSession();

                priceVert = (PriceVert) session.get(PriceVertImpl.class,
                        primaryKey);

                if (priceVert != null) {
                    cacheResult(priceVert);
                } else {
                    EntityCacheUtil.putResult(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
                        PriceVertImpl.class, primaryKey, _nullPriceVert);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(PriceVertModelImpl.ENTITY_CACHE_ENABLED,
                    PriceVertImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return priceVert;
    }

    /**
     * Returns the price vert with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param price_id the primary key of the price vert
     * @return the price vert, or <code>null</code> if a price vert with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PriceVert fetchByPrimaryKey(long price_id) throws SystemException {
        return fetchByPrimaryKey((Serializable) price_id);
    }

    /**
     * Returns all the price verts.
     *
     * @return the price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the price verts.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @return the range of price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the price verts.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.energyindex.liferay.sb.model.impl.PriceVertModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of price verts
     * @param end the upper bound of the range of price verts (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<PriceVert> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<PriceVert> list = (List<PriceVert>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PRICEVERT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PRICEVERT;

                if (pagination) {
                    sql = sql.concat(PriceVertModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<PriceVert>(list);
                } else {
                    list = (List<PriceVert>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the price verts from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (PriceVert priceVert : findAll()) {
            remove(priceVert);
        }
    }

    /**
     * Returns the number of price verts.
     *
     * @return the number of price verts
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PRICEVERT);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the price vert persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.energyindex.liferay.sb.model.PriceVert")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<PriceVert>> listenersList = new ArrayList<ModelListener<PriceVert>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<PriceVert>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PriceVertImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
