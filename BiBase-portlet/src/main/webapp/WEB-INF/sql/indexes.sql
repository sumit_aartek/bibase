create index IX_BCF63101 on BiBase_Internalindexes (product_id);

create index IX_47E33B26 on BiBase_Normalization (normType);

create index IX_5B67D769 on BiBase_PriceHoriz (port_id);
create index IX_AA2B8D78 on BiBase_PriceHoriz (price_date);
create index IX_6500B53F on BiBase_PriceHoriz (product_id);

create index IX_FFFD3950 on BiBase_PriceVert (port_id);
create index IX_6DF78E31 on BiBase_PriceVert (price_date);
create index IX_28CCB5F8 on BiBase_PriceVert (product_id);